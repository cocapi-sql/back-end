#! /usr/bin/perl

#############################################
#                  COC API                  #
#                                           #
#         Used to pull active war           #
#               information                 #
#                                           #
#              by Gary Douglas              #
#               April 22, 2017              #
#        http://www.the-blacklist.ca        #
#############################################


use strict;
use warnings;
use Data::Dumper;
use Time::Piece;
use JSON::Parse 'parse_json';
use DBI;

# include files
use lib '/home/thebl962/public_html/bin';
require 'API_SQL_Statements.pl';
require 'config_02.pl';
# require 'config.pl';

# SQL Variables
my $sql_platform = get_sql_platform ();
my $dbname =  get_dbname ();
my $servername = get_servername ();
my $username = get_username();
my $password = get_password();
my $dsn = "DBI:" . get_sql_platform() . ":" . get_dbname() . ":" . get_servername();

# Set up api token and url
my $api_clan_token = get_api_clan_token();
my $api_clantag_url = "https://api.clashofclans.com/v1/clans/";
my $api_member_token = get_api_member_token();
my $api_membertag_url = "https://api.clashofclans.com/v1/players/";

# load clan tags into array, Note leave the # off
my @clan_tags = get_clan_list();

# set up headers
my $json_header = "Accept: application/json";
my $api_clan_header = "authorization: Bearer " . $api_clan_token;
my $api_member_header = "authorization: Bearer " . $api_member_token;

# set up global variables
my %counter = ();
my %clan_json = ();
my %warlog_json = ();
our $clan_tag;
my $war_flag = 0; # 0=warlog closed, 1=prep, 2=inWar, 3=warDone
my @battle_hash = ();
my $battle_flag = 0; # 0=no new battles, 1=new battles

# get debug and test
my $debug = get_debug();
my $test = get_test();
my $mail_flag = get_mail_flag();

# check for cli arguments
while ( @ARGV ) {
	if ( my $Parameter eq '-debug' ) {
		$debug = 1;
	} elsif ( $Parameter eq '-test' ) {
		$test = 1;
	} elsif ( $Parameter eq '-mail' ) {
		$mail_flag = 1;
	}
} 

# display debug and test flags
if ( $debug == 1 ) { print "DEBUG: Debug (Verbose Logging) is ON\n"; }
if ( $test == 1 ) { print "DEBUG: Test (No SQL Insert) is ON\n"; }
if ( $mail_flag == 1 ) { print "DEBUG: Mail is ON\n"; }

# connect to MySQL database
my %attr = ( PrintError=>0,RaiseError=>1,PrintError=>0,ShowErrorStatement=>1 );
my $dbh = DBI->connect ( $dsn, $username, $password, \%attr );

# Get Greenwich time zone and convert to sql date-time stamp
my ($S, $M, $H, $d, $m, $Y) = gmtime(time);
$m += 1;
$Y += 1900;
my $time = sprintf("%04d-%02d-%02d %02d:%02d:%02d", $Y, $m, $d, $H, $M, $S);

# loop threw clan tag array
for $clan_tag ( @clan_tags ) {  
	war_pull();
}

# iterate threw clans
foreach $clan_tag ( @clan_tags ){
	if ( $debug ) { print "DEBUG: Start working on $clan_json{$clan_tag}{'name'} clan's data.\n"; }
	if ( $clan_json{$clan_tag}{'isWarLogPublic'} == 1 ) {
		# Check if clan is in war preperation
		if ( $warlog_json{$clan_tag}{'state'} eq 'preparation' ) {
			$counter{'Clan war status: Preparation'}++;		
			$war_flag = 1; # 0=warlog closed, 1=prep, 2=inWar, 3=warDone
			war_preparation();
		} elsif ( $warlog_json{$clan_tag}{'state'} eq 'inWar' ) {
			$counter{'Clan war status: War'}++;
			$war_flag = 2; # 0=warlog closed, 1=prep, 2=inWar, 3=warDone
			my $warlog_id = war_start();
		
			# display current war results display
			printf ( "\n%30s  ====  %-30s\n", "===============", "===============" );
			printf ( "%30s   VS   %-30s\n", $warlog_json{$clan_tag}{'clan'}{'name'}, $warlog_json{$clan_tag}{'opponent'}{'name'} );
			printf ( "%30s  ====  %-30s\n", "===============", "===============" );
			printf ( "%30d  STAR  %-30d\n", $warlog_json{$clan_tag}{'clan'}{'stars'}, $warlog_json{$clan_tag}{'opponent'}{'stars'} );
			printf ( "%30d  PECT  %-30d\n", $warlog_json{$clan_tag}{'clan'}{'destructionPercentage'}, $warlog_json{$clan_tag}{'opponent'}{'destructionPercentage'} );
			printf ( "%30d  ATCK  %-30d\n", $warlog_json{$clan_tag}{'clan'}{'attacks'}, $warlog_json{$clan_tag}{'opponent'}{'attacks'} );
			printf ( "%30s  ====  %-30s\n\n", "===============", "===============" );
			
			# check and send mail if needed
			if (( $mail_flag == 1 )&&( $battle_flag == 1 )) {
				send_mail( $warlog_id );
			}
		} elsif ( $warlog_json{$clan_tag}{'state'} eq 'warEnded' ) {
			$counter{'Clan war status: Ended'}++;
			$war_flag = 3; # 0=warlog closed, 1=prep, 2=inWar, 3=warEnded
			my $warEnd_flag = warEnd_Check();
		
			if ( $warEnd_flag == 1 ){
				my $warlog_id = war_start();
				war_end( $warlog_id );

				# check and send mail if needed
				if ( $mail_flag == 1 ) {
					send_mail( $warlog_id );
				}
			}
		}		
	} else {
		if ( $debug ) { print "DEBUG:  Skipping as warlog is private.\n"; }
	}
	if ( $debug ) { print "DEBUG: Finished working on $clan_json{$clan_tag}{'name'} clan's data.\n"; }
}

# clean up and disconnect from the MySQL database
$dbh->disconnect();
	
# display counter
print "\n";
print "Summary\n";
print "-" x 50 . " " . "-" x 5 . "\n";
foreach my $key ( sort keys %counter ) {
	printf( "%-50s %5d\n", $key, $counter{$key} );
}

#print Dumper( %warlog_json );




##############################
# war_pull
##############################
sub war_pull {
	if ( $debug ) { print "DEBUG: Getting json for clan $clan_tag.\n"; }
		
	# set up url
	my $clan_url = $api_clantag_url . '%23' . $clan_tag;
	my $warlog_url = $api_clantag_url . '%23' . $clan_tag . "/currentwar";
	
	# get the clan in json
	my $http_response = qx{curl -si --header \'$json_header\' --header \"$api_clan_header\" $clan_url};
	my ( $http_head, $http_body ) = split ( m{\r?\n\r?\n}, $http_response );
	
	# check if we got error
	if ( $http_head !~ m#HTTP/1.1 200 OK# ) {
		print "ERROR: Did not get back clan web page. Check response code.\n";
		print "================\n";
		print "= HEADER START =\n";
		print "================\n\n";
		print "$http_head\n\n";
		print "================\n";
		print "== HEADER END ==\n";
		print "================\n";
		exit ();
	}
		
	# parse json to hash
	$clan_json{$clan_tag} = parse_json ( $http_body );
	
	# check if we parsed reply correctly
	if ( !$clan_json{$clan_tag} ) {
		print "ERROR: Unable to clan parse from API data.";
		exit ();
	}

	if ( $debug ) { print "DEBUG: Successful got clan data for $clan_tag.\n"; }
	$counter{'API pull successful: clans'}++;
	
	# check if war log is public
	if ( $clan_json{$clan_tag}{'isWarLogPublic'} == 1 ) {
		$counter{'Clan war log: public'}++;	
	
		# get the clan warlog in json
		my $http_response = qx{curl -si --header \'$json_header\' --header \"$api_clan_header\" $warlog_url};
		my ( $http_head, $http_body ) = split ( m{\r?\n\r?\n}, $http_response );
	
		# check if we got error
		if ( $http_head !~ m#HTTP/1.1 200 OK# ) {
			print "ERROR: Did not get back clan web page. Check response code.\n";
			print "================\n";
			print "= HEADER START =\n";
			print "================\n\n";
			print "$http_head\n\n";
			print "================\n";
			print "== HEADER END ==\n";
			print "================\n";
 			exit ();
		}
		
		# parse json to hash
		$warlog_json{$clan_tag} = parse_json ( $http_body );
	
		# check if we parsed reply correctly
		if ( !$warlog_json{$clan_tag} ) {
			print "ERROR: Unable to parse war log from API data.";
			exit ();
		}
	
		# convert times to sql time formate
		if ( $warlog_json{$clan_tag}{'preparationStartTime'} =~ m/(\d\d\d\d)(\d\d)(\d\d)T(\d\d)(\d\d)(\d\d)/ ) {
			$warlog_json{$clan_tag}{'preparationStartTime'} = $1 . '-' . $2 . '-' . $3 . ' ' . $4 . ':' . $5 . ':' . $6;
		}
		if ( $warlog_json{$clan_tag}{'startTime'} =~ m/(\d\d\d\d)(\d\d)(\d\d)T(\d\d)(\d\d)(\d\d)/ ) {
			$warlog_json{$clan_tag}{'startTime'} = $1 . '-' . $2 . '-' . $3 . ' ' . $4 . ':' . $5 . ':' . $6;
		}
		if ( $warlog_json{$clan_tag}{'endTime'} =~ m/(\d\d\d\d)(\d\d)(\d\d)T(\d\d)(\d\d)(\d\d)/ ) {
			$warlog_json{$clan_tag}{'endTime'} = $1 . '-' . $2 . '-' . $3 . ' ' . $4 . ':' . $5 . ':' . $6;
		}

		if ( $debug ) { print "DEBUG: Successful got war log data for $clan_tag.\n"; }
		$counter{'API pull successful: clans/currentwar'}++;
	} else {
		$counter{'Clan war log: private'}++;	
	}
}

##############################
# war_preparation
##############################
sub war_preparation {
	if ( $debug ) { print "DEBUG:   War preparation mode.\n"; }

	# local variables
	my $warlog_id = 0;

	# set up sql statements
	my $stmt_warlog_opp_search = $dbh->prepare ( get_warlog_opp_search() )
							or die "Unable to prepare stmt_warlog_opp_search.  " . $dbh->errstr;
	
	# check if war is already in sql
	$stmt_warlog_opp_search->execute( $warlog_json{$clan_tag}{'opponent'}{'tag'} )
	 						or die "Unable to execute stmt_warlog_opp_search.  " . $stmt_warlog_opp_search->errstr;

	my $temp_warlog_endTime = 0;
	while ( my $row = $stmt_warlog_opp_search->fetchrow_hashref ( ) ) {
		$temp_warlog_endTime = $row->{'warlog_endTime'};
	}
	
	if ( $temp_warlog_endTime eq $warlog_json{$clan_tag}{'endTime'} ) {
		# Already in sql
		if ( $debug ) { print "DEBUG:   War preparation already in sql.\n"; }
	} else {
		# Not in sql
		if ( $debug ) { print "DEBUG:   War preparation not in sql.\n"; }
		$warlog_id = enter_warlog_data();
	}
	
	# clean up sql statements
	$stmt_warlog_opp_search->finish();
}

##############################
# warEnd_Check
##############################
sub warEnd_Check {
	# local variables
	my $warlog_endTime = 0;
	my $warlog_result = 0;
	my $warEnd_flag = 0;
	
	# set up sql statements
	my $stmt_warlog_opp_search = $dbh->prepare ( get_warlog_opp_search() )
							or die "Unable to prepare stmt_warlog_opp_search.  " . $dbh->errstr;
	
	# check if war is already in sql
	$stmt_warlog_opp_search->execute( $warlog_json{$clan_tag}{'opponent'}{'tag'} )
	 						or die "Unable to execute stmt_warlog_opp_search.  " . $stmt_warlog_opp_search->errstr;

	while ( my $row = $stmt_warlog_opp_search->fetchrow_hashref ( ) ) {
		$warlog_endTime = $row->{'warlog_endTime'};
		$warlog_result = $row->{'warlog_result'};
	}
		
	# check if war is already in sql and needs updated
	if (( $warlog_endTime eq $warlog_json{$clan_tag}{'endTime'} )&&
			(( $warlog_result eq 'preperation' )||
			( $warlog_result eq 'warEnded' ))) {
		$warEnd_flag = 1;
		if ( $debug ) { print "DEBUG:   War has ended and needs updated.\n"; }
	} else {
		if ( $debug ) { print "DEBUG:   War has ended and doesn't need updated.\n"; }
	}
	
	# clean up sql statements
	$stmt_warlog_opp_search->finish();	
	
	return $warEnd_flag;
}

##############################
# war_end
##############################
sub war_end {
	# get input variables
	my ( $warlog_id ) = @_;
	
	# set up url
	my $warlog_url = $api_clantag_url . '%23' . $clan_tag . "/warlog";

	# set up sql statements
	my $stmt_warlog_end_update = $dbh->prepare ( get_warlog_end_update() )
							or die "Unable to prepare stmt_warlog_end_update.  " . $dbh->errstr;
	
	# get the war log in json
	my $http_response = qx{curl -si --header \'$json_header\' --header \"$api_clan_header\" $warlog_url};
	my ( $http_head, $http_body ) = split ( m{\r?\n\r?\n}, $http_response );

	# check if we got error
	if ( $http_head !~ m#HTTP/1.1 200 OK# ) {
		print "ERROR: Did not get back clan web page. Check response code.\n";
		print "================\n";
		print "= HEADER START =\n";
		print "================\n\n";
		print "$http_head\n\n";
		print "================\n";
		print "== HEADER END ==\n";
		print "================\n";
		exit ();
	}
	
	# parse json to hash
	$clan_json{$clan_tag}{'warlog'} = parse_json ( $http_body );

	# check if we parsed reply correctly
	if ( !$clan_json{$clan_tag}{'warlog'} ) {
		print "ERROR: Unable to warlog parse from API data.";
		exit ();
	}

	if ( $debug ) { print "DEBUG: Successful got warlog for $clan_tag.\n"; }
	$counter{'API pull successful: clans/warlog'}++;
	
	# iterate threw warlog
	for ( my $i = 0; $i < @{$clan_json{$clan_tag}{'warlog'}{'items'}}; $i++ ) {			
			
		# check if current log is correct
		if ( $warlog_json{$clan_tag}{'opponent'}{'tag'} eq $clan_json{$clan_tag}{'warlog'}{'items'}[$i]{'opponent'}{'tag'} ) {
			# update warlog
			if ( $test == 0 ) {
				$stmt_warlog_end_update->execute ( $clan_json{$clan_tag}{'warlog'}{'items'}[$i]{'result'},
							$clan_json{$clan_tag}{'warlog'}{'items'}[$i]{'clan'}{'destructionPercentage'},
							$warlog_id )
						or die "Unable to execute stmt_warlog_end_update.  " . $stmt_warlog_end_update->errstr;
				if ( $debug ) { print "DEBUG:   WarLog for $clan_json{$clan_tag}{'name'} updated successfully.\n"; }
				$counter{'SQL update successful: API_WarLog'}++;
			}
		}		
	}		
	
	# display finished war results display
	printf ( "\n%30s  ====  %-30s\n", "===============", "===============" );
	printf ( "%30s   VS   %-30s\n", $warlog_json{$clan_tag}{'clan'}{'name'}, $warlog_json{$clan_tag}{'opponent'}{'name'} );
	printf ( "%30s  ====  %-30s\n", "===============", "===============" );
	printf ( "%30d  STAR  %-30d\n", $warlog_json{$clan_tag}{'clan'}{'stars'}, $warlog_json{$clan_tag}{'opponent'}{'stars'} );
	printf ( "%30d  PECT  %-30d\n", $warlog_json{$clan_tag}{'clan'}{'destructionPercentage'}, $warlog_json{$clan_tag}{'opponent'}{'destructionPercentage'} );
	printf ( "%30d  ATCK  %-30d\n", $warlog_json{$clan_tag}{'clan'}{'attacks'}, $warlog_json{$clan_tag}{'opponent'}{'attacks'} );
	printf ( "%30s  ====  %-30s\n\n", "===============", "===============" );
	
	# clean up sql statements
	$stmt_warlog_end_update->finish();	

}

##############################
# war_start
##############################
sub war_start {
	if ( $debug ) { print "DEBUG:   War mode.\n"; }
	
	# local variables
	my $warlog_id = 0;
	my $warlog_endTime = 0;
	my $warlog_clan_attacks = 0;
	my $warlog_opp_attacks = 0;
	my $warlog_result = 0;
	
	# set up sql statements
	my $stmt_warlog_opp_search = $dbh->prepare ( get_warlog_opp_search() )
							or die "Unable to prepare stmt_warlog_opp_search.  " . $dbh->errstr;
	my $stmt_warlog_end_update = $dbh->prepare ( get_warlog_end_update() )
							or die "Unable to prepare stmt_warlog_end_update.  " . $dbh->errstr;
	
	
	# check if war is already in sql
	$stmt_warlog_opp_search->execute( $warlog_json{$clan_tag}{'opponent'}{'tag'} )
	 						or die "Unable to execute stmt_warlog_opp_search.  " . $stmt_warlog_opp_search->errstr;

	while ( my $row = $stmt_warlog_opp_search->fetchrow_hashref ( ) ) {
		$warlog_id = $row->{'warlog_id'};
		$warlog_endTime = $row->{'warlog_endTime'};
		$warlog_clan_attacks = $row->{'warlog_clan_attacks'};
		$warlog_opp_attacks = $row->{'warlog_opp_attacks'};
		$warlog_result = $row->{'warlog_result'};
	}
	
	if ( $warlog_endTime eq $warlog_json{$clan_tag}{'endTime'} ) {
		# Already in sql
		if ( $debug ) { print "DEBUG:   War already in API_WarLog sql table.\n"; }
		
		# check if war needs updated from prep
		if ( $warlog_result eq 'preparation' ) {
			# update warlog
			if ( $test == 0 ) {
				$stmt_warlog_end_update->execute ( $warlog_result,
							0,
							$warlog_id )
						or die "Unable to execute stmt_warlog_end_update.  " . $stmt_warlog_end_update->errstr;
				if ( $debug ) { print "DEBUG:   WarLog for $clan_json{$clan_tag}{'name'} updated successfully.\n"; }
				$counter{'SQL update successful: API_WarLog'}++;
			}
		}
		
		# check if need update
		if (( $warlog_clan_attacks != $warlog_json{$clan_tag}{'clan'}{'attacks'} )|| 
				( $warlog_opp_attacks != $warlog_json{$clan_tag}{'opponent'}{'attacks'} )||
				( $war_flag == 3 )) {
			if ( $debug ) { print "DEBUG:   War in sql needs updated.\n"; }
			# update warlog
			update_warlog_data( $warlog_id );	
			
			# enter war data
			enter_battle_data( $warlog_id );
		} else {
			if ( $debug ) { print "DEBUG:   War in sql does not need updated.\n"; }
		}
	} else {
		# Not in sql
		if ( $debug ) { print "DEBUG:   War not in sql API_WarLog sql table.\n"; }
		# enter war log
		$warlog_id = enter_warlog_data();
		
		# enter war data
		enter_battle_data( $warlog_id );
	}	
	
	
	# clean up sql statements
	$stmt_warlog_opp_search->finish();	
	$stmt_warlog_end_update->finish();
	
	return $warlog_id;
}

##############################
# enter_battle_data
##############################
sub enter_battle_data {
	# get input variables
	my ( $warlog_id ) = @_;

	# local variables
	my @sql_battles = ();
	my @api_battles = ();

	# set up sql statements
	my $stmt_warlog_battle_search = $dbh->prepare ( get_warlog_battle_search() )
						or die "Unable to prepare stmt_warlog_battle_search.  " . $dbh->errstr;
	my $stmt_warlog_battle_insert = $dbh->prepare ( get_warlog_battle_insert() )
						or die "Unable to prepare stmt_warlog_battle_insert.  " . $dbh->errstr;
	my $stmt_warlog_member_update = $dbh->prepare ( get_warlog_member_update() )
						or die "Unable to prepare stmt_warlog_member_update.  " . $dbh->errstr;
	my $stmt_warlog_member_search = $dbh->prepare ( get_warlog_member_search() )
						or die "Unable to prepare stmt_warlog_member_search.  " . $dbh->errstr;
	my $stmt_memberWars_search = $dbh->prepare ( get_memberWars_search() )
						or die "Unable to prepare stmt_memberWars_search.  " . $dbh->errstr;
	my $stmt_memberWars_update = $dbh->prepare ( get_memberWars_update() )
						or die "Unable to prepare stmt_memberWars_update.  " . $dbh->errstr;
		
	
	# pull all battles for war from sql
	$stmt_warlog_battle_search->execute( $warlog_id )
							or die "Unable to execute stmt_warlog_battle_search.  " . $stmt_warlog_battle_search->errstr;

	while ( my $row = $stmt_warlog_battle_search->fetchrow_hashref ( ) ) {
		my $battle_order = $row->{'battle_order'};
		$sql_battles[$battle_order]{'battle_id'} = $row->{'battle_id'};
		$sql_battles[$battle_order]{'battle_attackerTag'} = $row->{'battle_attackerTag'};
		$sql_battles[$battle_order]{'battle_defenderTag'} = $row->{'battle_defenderTag'};
		$sql_battles[$battle_order]{'battle_stars'} = $row->{'battle_stars'};
		$sql_battles[$battle_order]{'battle_destructionPercentage'} = $row->{'battle_destructionPercentage'};
		$sql_battles[$battle_order]{'battle_clean_flag'} = $row->{'battle_clean_flag'};
		$sql_battles[$battle_order]{'battle_loot_flag'} = $row->{'battle_loot_flag'};
		$sql_battles[$battle_order]{'battle_earlyLoot_flag'} = $row->{'battle_earlyLoot_flag'};
		$sql_battles[$battle_order]{'battle_noCC_flag'} = $row->{'battle_noCC_flag'};
	}
	if ( $debug ) { print "DEBUG:   Battles pulled successfully for war.\n"; }
	my $sql_battle_length = scalar @sql_battles - 1;
	
	# get all battles from api
	# iterate threw home clans members
	for my $member_href ( @{ $warlog_json{$clan_tag}{'clan'}{'members'} } ) {
		my %member = %$member_href;
		# iterate threw member attacks
		for my $battle_href ( @{ $member{'attacks'} } ) {
			my %battle = %$battle_href;
			$api_battles[$battle{'order'}]{'attackerTag'} = $battle{'attackerTag'};
			$api_battles[$battle{'order'}]{'defenderTag'} = $battle{'defenderTag'};
			$api_battles[$battle{'order'}]{'stars'} = $battle{'stars'};
			$api_battles[$battle{'order'}]{'destructionPercentage'} = $battle{'destructionPercentage'};
			$api_battles[$battle{'order'}]{'order'} = $battle{'order'};
			$api_battles[$battle{'order'}]{'home_flag'} = 1;
		}
	}
	# iterate threw enemy clans members
	for my $member_href ( @{ $warlog_json{$clan_tag}{'opponent'}{'members'} } ) {
		my %member = %$member_href;
		# iterate threw member attacks
		for my $battle_href ( @{ $member{'attacks'} } ) {
			my %battle = %$battle_href;
			$api_battles[$battle{'order'}]{'attackerTag'} = $battle{'attackerTag'};
			$api_battles[$battle{'order'}]{'defenderTag'} = $battle{'defenderTag'};
			$api_battles[$battle{'order'}]{'stars'} = $battle{'stars'};
			$api_battles[$battle{'order'}]{'destructionPercentage'} = $battle{'destructionPercentage'};
			$api_battles[$battle{'order'}]{'order'} = $battle{'order'};
			$api_battles[$battle{'order'}]{'home_flag'} = 0;
		}
	}
	my $api_battle_length = scalar @api_battles - 1;
	
	if ( $debug ) { print "DEBUG:    Examining battles started.\n"; }
	# iterate threw api battles
	for ( my $i = 1; $i <= $api_battle_length; $i++ ) {
		if ( $debug ) { print "DEBUG:     Battle no. $i start.\n"; }
		# check if battle is not in sql
		if (( $war_flag == 3 )||( !defined ( $sql_battles[$i] ))) {
			my $battle_clean_flag = 0;
			my $battle_loot_flag = 0;
			my %sql_attacker = ();
			my %sql_defender = ();
			my %sql_member = ();

			# pull attackers info from sql
			$stmt_warlog_member_search->execute( $warlog_id, $api_battles[$i]{'attackerTag'} )
									or die "Unable to execute stmt_warlog_member_search.  " . $stmt_warlog_member_search->errstr;

			while ( my $row = $stmt_warlog_member_search->fetchrow_hashref ( ) ) {
				$sql_attacker{'member_name'} = $row->{'member_name'};
				$sql_attacker{'member_townhallLevel'} = $row->{'member_townhallLevel'};
				$sql_attacker{'member_mapPosition'} = $row->{'member_mapPosition'};
				$sql_attacker{'member_off_warWeight'} = $row->{'member_off_warWeight'};
				$sql_attacker{'member_attacks_used'} = $row->{'member_attacks_used'};
				$sql_attacker{'member_attack_stars'} = $row->{'member_attack_stars'};
				$sql_attacker{'member_attack_newStars'} = $row->{'member_attack_newStars'};
				$sql_attacker{'member_attack_destruction'} = $row->{'member_attack_destruction'};
				$sql_attacker{'member_defense_used'} = $row->{'member_defense_used'};
				$sql_attacker{'member_defense_stars'} = $row->{'member_defense_stars'};
				$sql_attacker{'member_defense_newStars'} = $row->{'member_defense_newStars'};
				$sql_attacker{'member_defense_destruction'} = $row->{'member_defense_destruction'};
				$sql_attacker{'member_clean_flag'} = $row->{'member_clean_flag'};
				$sql_attacker{'member_loot_flag'} = $row->{'member_loot_flag'};
				$sql_attacker{'member_earlyLoot_flag'} = $row->{'member_earlyLoot_flag'};
				$sql_attacker{'member_noCC_flag'} = $row->{'member_noCC_flag'};
				if ( $debug ) { print "DEBUG:      Attacker pulled successfully for $sql_attacker{'member_name'}.\n"; }
			}
			
			# pull defender info from sql
			$stmt_warlog_member_search->execute( $warlog_id, $api_battles[$i]{'defenderTag'} )
									or die "Unable to execute stmt_warlog_member_search.  " . $stmt_warlog_member_search->errstr;

			while ( my $row = $stmt_warlog_member_search->fetchrow_hashref ( ) ) {
				$sql_defender{'member_name'} = $row->{'member_name'};
				$sql_defender{'member_townhallLevel'} = $row->{'member_townhallLevel'};
				$sql_defender{'member_mapPosition'} = $row->{'member_mapPosition'};
				$sql_defender{'member_off_warWeight'} = $row->{'member_off_warWeight'};
				$sql_defender{'member_attacks_used'} = $row->{'member_attacks_used'};
				$sql_defender{'member_attack_stars'} = $row->{'member_attack_stars'};
				$sql_defender{'member_attack_newStars'} = $row->{'member_attack_newStars'};
				$sql_defender{'member_attack_destruction'} = $row->{'member_attack_destruction'};
				$sql_defender{'member_defense_used'} = $row->{'member_defense_used'};
				$sql_defender{'member_defense_stars'} = $row->{'member_defense_stars'};
				$sql_defender{'member_defense_newStars'} = $row->{'member_defense_newStars'};
				$sql_defender{'member_defense_destruction'} = $row->{'member_defense_destruction'};
				$sql_defender{'member_clean_flag'} = $row->{'member_clean_flag'};
				$sql_defender{'member_loot_flag'} = $row->{'member_loot_flag'};
				$sql_defender{'member_earlyLoot_flag'} = $row->{'member_earlyLoot_flag'};
				$sql_defender{'member_noCC_flag'} = $row->{'member_noCC_flag'};
				if ( $debug ) { print "DEBUG:      Defender pulled successfully for $sql_defender{'member_name'}.\n"; }
			}
			
			# get flags
			$battle_clean_flag = set_battle_clean_flag( \@api_battles, \%sql_attacker, \%sql_defender, $i );				
			$battle_loot_flag = set_battle_loot_flag( \@api_battles, \%sql_attacker, \%sql_defender, $i );				
			
			# check if new attack is better then last attack on defender
			if (( $api_battles[$i]{'stars'} > $sql_defender{'member_defense_stars'} )||
					(( $api_battles[$i]{'stars'} == $sql_defender{'member_defense_stars'} )&&
					( $api_battles[$i]{'destructionPercentage'} > $sql_defender{'member_defense_destruction'} ))) {
				# update newstars and destruction
				$api_battles[$i]{'newStars'} = $api_battles[$i]{'stars'} - $sql_defender{'member_defense_stars'};
				$api_battles[$i]{'destructionPercentage'} = $api_battles[$i]{'destructionPercentage'};	
			} else {
				$api_battles[$i]{'newStars'} = 0;
			}
			
			# compute star number attack
			if ( $api_battles[$i]{'stars'} == 3 ) {
				$api_battles[$i]{'3Stars'} = 1;
				$api_battles[$i]{'2Stars'} = 0;
				$api_battles[$i]{'1Stars'} = 0;
				$api_battles[$i]{'0Stars'} = 0;
			} elsif ( $api_battles[$i]{'stars'} == 2 ) {
				$api_battles[$i]{'3Stars'} = 0;
				$api_battles[$i]{'2Stars'} = 1;
				$api_battles[$i]{'1Stars'} = 0;
				$api_battles[$i]{'0Stars'} = 0;
			} elsif ( $api_battles[$i]{'stars'} == 1 ) {
				$api_battles[$i]{'3Stars'} = 0;
				$api_battles[$i]{'2Stars'} = 0;
				$api_battles[$i]{'1Stars'} = 1;
				$api_battles[$i]{'0Stars'} = 0;
			} elsif ( $api_battles[$i]{'stars'} == 0 ) {
				$api_battles[$i]{'3Stars'} = 0;
				$api_battles[$i]{'2Stars'} = 0;
				$api_battles[$i]{'1Stars'} = 0;
				$api_battles[$i]{'0Stars'} = 1;
			}

			if ( $api_battles[$i]{'newStars'} == 3 ) {
				$api_battles[$i]{'3NewStars'} = 1;
				$api_battles[$i]{'2NewStars'} = 0;
				$api_battles[$i]{'1NewStars'} = 0;
				$api_battles[$i]{'0NewStars'} = 0;
			} elsif ( $api_battles[$i]{'newStars'} == 2 ) {
				$api_battles[$i]{'3NewStars'} = 0;
				$api_battles[$i]{'2NewStars'} = 1;
				$api_battles[$i]{'1NewStars'} = 0;
				$api_battles[$i]{'0NewStars'} = 0;
			} elsif ( $api_battles[$i]{'newStars'} == 1 ) {
				$api_battles[$i]{'3NewStars'} = 0;
				$api_battles[$i]{'2NewStars'} = 0;
				$api_battles[$i]{'1NewStars'} = 1;
				$api_battles[$i]{'0NewStars'} = 0;
			} elsif ( $api_battles[$i]{'newStars'} == 0 ) {
				$api_battles[$i]{'3NewStars'} = 0;
				$api_battles[$i]{'2NewStars'} = 0;
				$api_battles[$i]{'1NewStars'} = 0;
				$api_battles[$i]{'0NewStars'} = 1;
			}

			if ( $test == 0 ) {
				# insert warlog_battle into sql
				$stmt_warlog_battle_insert->execute ( $warlog_id,
							$api_battles[$i]{'attackerTag'},
							$api_battles[$i]{'defenderTag'},
							$api_battles[$i]{'stars'},
							$api_battles[$i]{'newStars'},
							$api_battles[$i]{'destructionPercentage'},
							$api_battles[$i]{'order'},
							$battle_clean_flag,
							$battle_loot_flag,
							0,
							0 )
						or die "Unable to execute stmt_warlog_battle_insert.  " . $stmt_warlog_battle_insert->errstr;
				if ( $debug ) { print "DEBUG:      Battle inserted successfully for $sql_attacker{'member_name'}.\n"; }
				$counter{'SQL insert successful: API_WarLog_Battle'}++;
				
				# update warlog_member attacker in sql
				$stmt_warlog_member_update->execute ( $sql_attacker{'member_attacks_used'} + 1,
											 $sql_attacker{'member_attack_stars'} + $api_battles[$i]{'stars'},
											 $sql_attacker{'member_attack_newStars'} + $api_battles[$i]{'newStars'},
											 $sql_attacker{'member_attack_destruction'} + $api_battles[$i]{'destructionPercentage'},
											 $sql_attacker{'member_defense_used'},
											 $sql_attacker{'member_defense_stars'},
											 $sql_attacker{'member_defense_newStars'},
											 $sql_attacker{'member_defense_destruction'},
											 $sql_attacker{'member_clean_flag'} + $battle_clean_flag,
											 $sql_attacker{'member_loot_flag'} + $battle_loot_flag,
											 $sql_attacker{'member_earlyLoot_flag'},
											 $sql_attacker{'member_noCC_flag'},
											 $warlog_id, 
											 $api_battles[$i]{'attackerTag'} )
						or die "Unable to execute stmt_warlog_member_update.  " . $stmt_warlog_member_update->errstr;
				if ( $debug ) { print "DEBUG:      Attacker updated successfully for $sql_attacker{'member_name'}.\n"; }
				$counter{'SQL update successful: API_WarLog_Member'}++;

				# update warlog_member defender in sql
				$stmt_warlog_member_update->execute ( $sql_attacker{'member_attacks_used'},
											 $sql_defender{'member_attack_stars'},
											 $sql_defender{'member_attack_newStars'},
											 $sql_defender{'member_attack_destruction'},
											 $sql_defender{'member_defense_used'} + 1,
											 $sql_defender{'member_defense_stars'} + $api_battles[$i]{'stars'},
											 $sql_defender{'member_defense_newStars'} + $api_battles[$i]{'newStars'},
											 $sql_defender{'member_defense_destruction'} + $api_battles[$i]{'destructionPercentage'},
											 $sql_defender{'member_clean_flag'},
											 $sql_defender{'member_loot_flag'},
											 $sql_defender{'member_earlyLoot_flag'},
											 $sql_defender{'member_noCC_flag'},
											 $warlog_id, 
											 $api_battles[$i]{'defenderTag'} )
						or die "Unable to execute stmt_warlog_member_update.  " . $stmt_warlog_member_update->errstr;
				if ( $debug ) { print "DEBUG:      Defender updated successfully for $sql_defender{'member_name'}.\n"; }
				$counter{'SQL update successful: API_WarLog_Member'}++;
			}	
			
			# check if home clan is attacker
			if ( $api_battles[$i]{'home_flag'} == 1 ) {
				# attacker is home clan, pull member_wars info from sql
				$stmt_memberWars_search->execute( $api_battles[$i]{'attackerTag'} )
										or die "Unable to execute stmt_memberWars_search.  " . $stmt_memberWars_search->errstr;

				while ( my $row = $stmt_memberWars_search->fetchrow_hashref ( ) ) {
					$sql_member{'member_war'} = $row->{'member_war'};
					$sql_member{'member_attack_used'} = $row->{'member_attack_used'};
					$sql_member{'member_attack_stars'} = $row->{'member_attack_stars'};
					$sql_member{'member_attack_newStars'} = $row->{'member_attack_newStars'};
					$sql_member{'member_attack_3Stars'} = $row->{'member_attack_3Stars'};
					$sql_member{'member_attack_2Stars'} = $row->{'member_attack_2Stars'};
					$sql_member{'member_attack_1Stars'} = $row->{'member_attack_1Stars'};
					$sql_member{'member_attack_0Stars'} = $row->{'member_attack_0Stars'};
					$sql_member{'member_attack_3NewStars'} = $row->{'member_attack_3NewStars'};
					$sql_member{'member_attack_2NewStars'} = $row->{'member_attack_2NewStars'};
					$sql_member{'member_attack_1NewStars'} = $row->{'member_attack_1NewStars'};
					$sql_member{'member_attack_0NewStars'} = $row->{'member_attack_0NewStars'};
					$sql_member{'member_attack_destruction'} = $row->{'member_attack_destruction'};
					$sql_member{'member_defense_used'} = $row->{'member_defense_used'};
					$sql_member{'member_defense_stars'} = $row->{'member_defense_stars'};
					$sql_member{'member_defense_newStars'} = $row->{'member_defense_newStars'};
					$sql_member{'member_defense_3Stars'} = $row->{'member_defense_3Stars'};
					$sql_member{'member_defense_2Stars'} = $row->{'member_defense_2Stars'};
					$sql_member{'member_defense_1Stars'} = $row->{'member_defense_1Stars'};
					$sql_member{'member_defense_0Stars'} = $row->{'member_defense_0Stars'};
					$sql_member{'member_defense_3NewStars'} = $row->{'member_defense_3NewStars'};
					$sql_member{'member_defense_2NewStars'} = $row->{'member_defense_2NewStars'};
					$sql_member{'member_defense_1NewStars'} = $row->{'member_defense_1NewStars'};
					$sql_member{'member_defense_0NewStars'} = $row->{'member_defense_0NewStars'};
					$sql_member{'member_defense_destruction'} = $row->{'member_defense_destruction'};
					$sql_member{'member_clean_cnt'} = $row->{'member_clean_cnt'};
					$sql_member{'member_loot_cnt'} = $row->{'member_loot_cnt'};
					$sql_member{'member_earlyLoot_cnt'} = $row->{'member_earlyLoot_cnt'};
					$sql_member{'member_noCC_cnt'} = $row->{'member_noCC_cnt'};
					if ( $debug ) { print "DEBUG:      $sql_attacker{'member_name'} pulled successfully from member_wars.\n"; }
				}
				
				# update attack counters for member
				if ( $test == 0 ) {
					$stmt_memberWars_update->execute ( $sql_member{'member_war'},
								$sql_member{'member_attack_used'} + 1,
								$sql_member{'member_attack_stars'} + $api_battles[$i]{'stars'},
								$sql_member{'member_attack_newStars'} + $api_battles[$i]{'newStars'}, 
								$sql_member{'member_attack_destruction'} + $api_battles[$i]{'destructionPercentage'},
								$sql_member{'member_attack_3Stars'} + $api_battles[$i]{'3Stars'},
								$sql_member{'member_attack_2Stars'} + $api_battles[$i]{'2Stars'},
								$sql_member{'member_attack_1Stars'} + $api_battles[$i]{'1Stars'},
								$sql_member{'member_attack_0Stars'} + $api_battles[$i]{'0Stars'},
								$sql_member{'member_attack_3NewStars'} + $api_battles[$i]{'3NewStars'},
								$sql_member{'member_attack_2NewStars'} + $api_battles[$i]{'2NewStars'},
								$sql_member{'member_attack_1NewStars'} + $api_battles[$i]{'1NewStars'},
								$sql_member{'member_attack_0NewStars'} + $api_battles[$i]{'0NewStars'},
								$sql_member{'member_defense_used'},
								$sql_member{'member_defense_stars'},
								$sql_member{'member_defense_newStars'}, 
								$sql_member{'member_defense_destruction'},
								$sql_member{'member_defense_3Stars'}, 
								$sql_member{'member_defense_2Stars'}, 
								$sql_member{'member_defense_1Stars'}, 
								$sql_member{'member_defense_0Stars'}, 
								$sql_member{'member_defense_3NewStars'}, 
								$sql_member{'member_defense_2NewStars'}, 
								$sql_member{'member_defense_1NewStars'}, 
								$sql_member{'member_defense_0NewStars'}, 
								$sql_member{'member_clean_cnt'} + $battle_clean_flag,
								$sql_member{'member_loot_cnt'} + $battle_loot_flag,
								$sql_member{'member_earlyLoot_cnt'},
								$sql_member{'member_noCC_cnt'},
								$api_battles[$i]{'attackerTag'} )
							or die "Unable to execute stmt_memberWars_update.  " . $stmt_memberWars_update->errstr;
					if ( $debug ) { print "DEBUG:      Attacher $sql_attacker{'member_name'} updated successfully into wars.\n"; }
					$counter{'SQL update successful: API_Mem_Wars'}++;
				}
			} else {
				# defender is home clan, pull member_wars info from sql
				$stmt_memberWars_search->execute( $api_battles[$i]{'defenderTag'} )
										or die "Unable to execute stmt_memberWars_search.  " . $stmt_memberWars_search->errstr;

				while ( my $row = $stmt_memberWars_search->fetchrow_hashref ( ) ) {
					$sql_member{'member_war'} = $row->{'member_war'};
					$sql_member{'member_attack_used'} = $row->{'member_attack_used'};
					$sql_member{'member_attack_stars'} = $row->{'member_attack_stars'};
					$sql_member{'member_attack_newStars'} = $row->{'member_attack_newStars'};
					$sql_member{'member_attack_3Stars'} = $row->{'member_attack_3Stars'};
					$sql_member{'member_attack_2Stars'} = $row->{'member_attack_2Stars'};
					$sql_member{'member_attack_1Stars'} = $row->{'member_attack_1Stars'};
					$sql_member{'member_attack_0Stars'} = $row->{'member_attack_0Stars'};
					$sql_member{'member_attack_3NewStars'} = $row->{'member_attack_3NewStars'};
					$sql_member{'member_attack_2NewStars'} = $row->{'member_attack_2NewStars'};
					$sql_member{'member_attack_1NewStars'} = $row->{'member_attack_1NewStars'};
					$sql_member{'member_attack_0NewStars'} = $row->{'member_attack_0NewStars'};
					$sql_member{'member_attack_destruction'} = $row->{'member_attack_destruction'};
					$sql_member{'member_defense_used'} = $row->{'member_defense_used'};
					$sql_member{'member_defense_stars'} = $row->{'member_defense_stars'};
					$sql_member{'member_defense_newStars'} = $row->{'member_defense_newStars'};
					$sql_member{'member_defense_3Stars'} = $row->{'member_defense_3Stars'};
					$sql_member{'member_defense_2Stars'} = $row->{'member_defense_2Stars'};
					$sql_member{'member_defense_1Stars'} = $row->{'member_defense_1Stars'};
					$sql_member{'member_defense_0Stars'} = $row->{'member_defense_0Stars'};
					$sql_member{'member_defense_3NewStars'} = $row->{'member_defense_3NewStars'};
					$sql_member{'member_defense_2NewStars'} = $row->{'member_defense_2NewStars'};
					$sql_member{'member_defense_1NewStars'} = $row->{'member_defense_1NewStars'};
					$sql_member{'member_defense_0NewStars'} = $row->{'member_defense_0NewStars'};
					$sql_member{'member_defense_destruction'} = $row->{'member_defense_destruction'};
					$sql_member{'member_clean_cnt'} = $row->{'member_clean_cnt'};
					$sql_member{'member_loot_cnt'} = $row->{'member_loot_cnt'};
					$sql_member{'member_earlyLoot_cnt'} = $row->{'member_earlyLoot_cnt'};
					$sql_member{'member_noCC_cnt'} = $row->{'member_noCC_cnt'};
					if ( $debug ) { print "DEBUG:      $sql_defender{'member_name'} pulled successfully from member_wars.\n"; }
				}
				
				# update attack counters for member
				if ( $test == 0 ) {
					$stmt_memberWars_update->execute ( $sql_member{'member_war'},
								$sql_member{'member_attack_used'},
								$sql_member{'member_attack_stars'},
								$sql_member{'member_attack_newStars'}, 
								$sql_member{'member_attack_destruction'},
								$sql_member{'member_attack_3Stars'}, 
								$sql_member{'member_attack_2Stars'}, 
								$sql_member{'member_attack_1Stars'}, 
								$sql_member{'member_attack_0Stars'}, 
								$sql_member{'member_attack_3NewStars'}, 
								$sql_member{'member_attack_2NewStars'}, 
								$sql_member{'member_attack_1NewStars'}, 
								$sql_member{'member_attack_0NewStars'}, 
								$sql_member{'member_defense_used'} + 1,
								$sql_member{'member_defense_stars'} + $api_battles[$i]{'stars'},
								$sql_member{'member_defense_newStars'} + $api_battles[$i]{'newStars'}, 
								$sql_member{'member_defense_destruction'} + $api_battles[$i]{'destructionPercentage'},
								$sql_member{'member_defense_3Stars'} + $api_battles[$i]{'3Stars'}, 
								$sql_member{'member_defense_2Stars'} + $api_battles[$i]{'2Stars'}, 
								$sql_member{'member_defense_1Stars'} + $api_battles[$i]{'1Stars'}, 
								$sql_member{'member_defense_0Stars'} + $api_battles[$i]{'0Stars'}, 
								$sql_member{'member_defense_3NewStars'} + $api_battles[$i]{'3NewStars'}, 
								$sql_member{'member_defense_2NewStars'} + $api_battles[$i]{'2NewStars'}, 
								$sql_member{'member_defense_1NewStars'} + $api_battles[$i]{'1NewStars'}, 
								$sql_member{'member_defense_0NewStars'} + $api_battles[$i]{'0NewStars'}, 
								$sql_member{'member_clean_cnt'},
								$sql_member{'member_loot_cnt'},
								$sql_member{'member_earlyLoot_cnt'},
								$sql_member{'member_noCC_cnt'},
								$api_battles[$i]{'defenderTag'} )
							or die "Unable to execute stmt_memberWars_update.  " . $stmt_memberWars_update->errstr;
					if ( $debug ) { print "DEBUG:      Defender $sql_defender{'member_name'} updated successfully into wars.\n"; }
					$counter{'SQL update successful: API_Mem_Wars'}++;
				}
			}
			
			# update mail message for battle
			if ( $mail_flag == 1 ) {
				$battle_hash[$i]{'attacker_name'} = $sql_attacker{'member_name'};
				$battle_hash[$i]{'attacker_mapPosition'} = $sql_attacker{'member_mapPosition'};
				$battle_hash[$i]{'defender_name'} = $sql_defender{'member_name'};
				$battle_hash[$i]{'defender_mapPosition'} = $sql_defender{'member_mapPosition'};
				$battle_hash[$i]{'stars'} = $api_battles[$i]{'stars'};
				$battle_hash[$i]{'newStars'} = $api_battles[$i]{'newStars'};
				$battle_hash[$i]{'destructionPercentage'} = $api_battles[$i]{'destructionPercentage'};
				$battle_hash[$i]{'home_flag'} = $api_battles[$i]{'home_flag'};
				$battle_hash[$i]{'clean_flag'} = $battle_clean_flag;
				$battle_hash[$i]{'loot_flag'} = $battle_loot_flag;
				$battle_flag = 1;
			}
		} else {
			if ( $debug ) { print "DEBUG:     	Battle already in sql.\n"; }
		}
		if ( $debug ) { print "DEBUG:     Battle no. $i finished.\n"; }
	}
	if ( $debug ) { print "DEBUG:    Examining battles finished.\n"; }
			
	# clean up sql statements
	$stmt_warlog_battle_search->finish();
	$stmt_warlog_battle_insert->finish();
	$stmt_warlog_member_update->finish();
	$stmt_warlog_member_search->finish();
	$stmt_memberWars_search->finish();
	$stmt_memberWars_update->finish();
}

##############################
# set_battle_clean_flag
##############################
sub set_battle_clean_flag {
	# get data
	# \@api_battles, \%sql_attacker, \%sql_defender, $i
	my $api_battles_ref = shift;
	my $sql_attacker_ref = shift;
	my $sql_defender_ref = shift;
	my $battle = shift;

	my @api_battles = @{$api_battles_ref};
	my %sql_attacker = %{$sql_attacker_ref};
	my %sql_defender = %{$sql_defender_ref};
	
	# set flag
	my $clean_flag = 0;
	
	# check if town hall lower
	if ( $sql_attacker{'member_townhallLevel'} > $sql_defender{'member_townhallLevel'} ) {
		$clean_flag = 1;
		if ( $debug ) { print "DEBUG:      Clean Flag: Enemy Town Hall lower.\n"; }
	}
	
	# check if 5 map positions lower
	if ( $sql_attacker{'member_mapPosition'} + 5 < $sql_defender{'member_mapPosition'} ) {
		$clean_flag = 1;
		if ( $debug ) { print "DEBUG:      Clean Flag: Enemy map position 5 lower.\n"; }
	}
	
	# if defender has already been attacked
	if ( $sql_defender{'member_defense_used'} >= 0 ) {
		$clean_flag = 1;
		if ( $debug ) { print "DEBUG:      Clean Flag: Defender has already been attacked.\n"; }
	}
	return $clean_flag;
}

##############################
# set_battle_loot_flag
##############################
sub set_battle_loot_flag {
	# get data
	# \@api_battles, \%sql_attacker, \%sql_defender, $i
	my $api_battles_ref = shift;
	my $sql_attacker_ref = shift;
	my $sql_defender_ref = shift;
	my $battle = shift;

	my @api_battles = @{$api_battles_ref};
	my %sql_attacker = %{$sql_attacker_ref};
	my %sql_defender = %{$sql_defender_ref};
	
	# set flag
	my $loot_flag = 0;
	
	# check if town hall greater
	if ( $sql_attacker{'member_townhallLevel'} < $sql_defender{'member_townhallLevel'} ) {
		$loot_flag = 1;
		if ( $debug ) { print "DEBUG:      Loot Flag: Enemy Town Hall higher.\n"; }
	}
	
	# check if 5 map positions greater
	if ( $sql_attacker{'member_mapPosition'} - 5 > $sql_defender{'member_mapPosition'} ) {
		$loot_flag = 1;
		if ( $debug ) { print "DEBUG:      Loot Flag: Enemy map position 5 higher.\n"; }
	}
	
	# if defender is already three stared
	if ( $sql_defender{'member_defense_stars'} == 3 ) {
		$loot_flag = 1;
		if ( $debug ) { print "DEBUG:      Loot Flag: Defender is already three stared.\n"; }
	}
	
	return $loot_flag;
}

##############################
# enter_warlog_data
##############################
sub enter_warlog_data {
	# local variables
	my $warlog_id = 0;

	# set up sql statements
	my $stmt_warlog_insert = $dbh->prepare ( get_warlog_withtimes_insert() )
						or die "Unable to prepare stmt_warlog_insert.  " . $dbh->errstr;
	my $stmt_warlog_opp_search = $dbh->prepare ( get_warlog_opp_search() )
							or die "Unable to prepare stmt_warlog_opp_search.  " . $dbh->errstr;
	my $stmt_memWW_search = $dbh->prepare ( get_memWW_search() )
						or die "Unable to prepare stmt_memWW_time_search.  " . $dbh->errstr;
	my $stmt_warlog_member_insert = $dbh->prepare ( get_warlog_member_insert() )
						or die "Unable to prepare stmt_warlog_member_insert.  " . $dbh->errstr;
	my $stmt_memberWars_search = $dbh->prepare ( get_memberWars_search() )
						or die "Unable to prepare stmt_memberWars_search.  " . $dbh->errstr;
	my $stmt_memberWars_insert = $dbh->prepare ( get_memberWars_insert() )
						or die "Unable to prepare stmt_memberWars_insert.  " . $dbh->errstr;
	my $stmt_memberWars_update = $dbh->prepare ( get_memberWars_update() )
						or die "Unable to prepare stmt_memberWars_update.  " . $dbh->errstr;

	# add warlog
	if ( $test == 0 ) {
		$stmt_warlog_insert->execute ( $warlog_json{$clan_tag}{'state'},
					1,
					$warlog_json{$clan_tag}{'preparationStartTime'},
					$warlog_json{$clan_tag}{'startTime'},
					$warlog_json{$clan_tag}{'endTime'},
					$warlog_json{$clan_tag}{'teamSize'},
					$warlog_json{$clan_tag}{'clan'}{'tag'},
					$warlog_json{$clan_tag}{'clan'}{'clanLevel'},
					$warlog_json{$clan_tag}{'clan'}{'stars'},
					$warlog_json{$clan_tag}{'clan'}{'destructionPercentage'},
					$warlog_json{$clan_tag}{'clan'}{'attacks'},
					0,
					$warlog_json{$clan_tag}{'opponent'}{'tag'},
					$warlog_json{$clan_tag}{'opponent'}{'name'},
					$warlog_json{$clan_tag}{'opponent'}{'clanLevel'},
					$warlog_json{$clan_tag}{'opponent'}{'stars'},
					$warlog_json{$clan_tag}{'opponent'}{'destructionPercentage'},
					$warlog_json{$clan_tag}{'opponent'}{'attacks'},
					$warlog_json{$clan_tag}{'opponent'}{'badgeUrls'}{'small'},
					$warlog_json{$clan_tag}{'opponent'}{'badgeUrls'}{'medium'},
					$warlog_json{$clan_tag}{'opponent'}{'badgeUrls'}{'large'} )
				or die "Unable to execute stmt_warlog_insert.  " . $stmt_warlog_insert->errstr;
		if ( $debug ) { print "DEBUG:   WarLog for $warlog_json{$clan_tag}{'clan'}{'name'} inserted successfully.\n"; }
		$counter{'SQL insert successful: API_WarLog'}++;
	}

	# get warlog_id
	$stmt_warlog_opp_search->execute( $warlog_json{$clan_tag}{'opponent'}{'tag'} )
	 						or die "Unable to execute stmt_warlog_opp_search.  " . $stmt_warlog_opp_search->errstr;

	while ( my $row = $stmt_warlog_opp_search->fetchrow_hashref ( ) ) {
		if ( $row->{'warlog_endTime'} eq $warlog_json{$clan_tag}{'endTime'} ) {
			$warlog_id = $row->{'warlog_id'};	
		}
	}
	
	if ( $warlog_id == 0 ) {
		print "ERROR: Unable to pull warlog_id.\n";
		return 0;
	}
	
	# insert home members into sql
	if ( $debug ) { print "DEBUG:   Inserting members for $warlog_json{$clan_tag}{'clan'}{'name'} started.\n"; }
	for my $member_href ( @{ $warlog_json{$clan_tag}{'clan'}{'members'} } ) {
		my %member = %$member_href;
		my %sql_member = ();

		# set offense war weight to zero
		$member{'member_off_warWeight'} = 0;

		# get newest war weight from SQL
		$stmt_memWW_search->execute ( $member{'tag'} )
					or die "Unable to execute stmt_memWW_search.  " . $stmt_memWW_search->errstr;

		while ( my $ww_row = $stmt_memWW_search->fetchrow_hashref ( ) ) {
			my $temp_memWW_hero = $ww_row->{'memWW_hero'};
			my $temp_memWW_troop = $ww_row->{'memWW_troop'};
			my $temp_memWW_spell = $ww_row->{'memWW_spell'};
			$member{'member_off_warWeight'} = $temp_memWW_hero + $temp_memWW_troop + $temp_memWW_spell;
		}

		if ( $test == 0 ) {
			$stmt_warlog_member_insert->execute ( $warlog_id,
						$warlog_json{$clan_tag}{'clan'}{'tag'},
						$member{'tag'},
						$member{'name'},
						$member{'townhallLevel'},
						$member{'member_off_warWeight'},
						$member{'mapPosition'})
					or die "Unable to execute stmt_warlog_member_insert.  " . $stmt_warlog_member_insert->errstr;
			if ( $debug ) { print "DEBUG:     $member{'name'} inserted successfully into warlog.\n"; }
			$counter{'SQL insert successful: API_WarLog_Member'}++;
		}
		
		# pull member_wars info from sql
		$stmt_memberWars_search->execute( $member{'tag'} )
								or die "Unable to execute stmt_memberWars_search.  " . $stmt_memberWars_search->errstr;

		while ( my $row = $stmt_memberWars_search->fetchrow_hashref ( ) ) {
			$sql_member{'member_war'} = $row->{'member_war'};
			$sql_member{'member_attack_used'} = $row->{'member_attack_used'};
			$sql_member{'member_attack_stars'} = $row->{'member_attack_stars'};
			$sql_member{'member_attack_newStars'} = $row->{'member_attack_newStars'};
			$sql_member{'member_attack_destruction'} = $row->{'member_attack_destruction'};
			$sql_member{'member_attack_3Stars'} = $row->{'member_attack_3Stars'};
			$sql_member{'member_attack_2Stars'} = $row->{'member_attack_2Stars'};
			$sql_member{'member_attack_1Stars'} = $row->{'member_attack_1Stars'};
			$sql_member{'member_attack_0Stars'} = $row->{'member_attack_0Stars'};
			$sql_member{'member_attack_3NewStars'} = $row->{'member_attack_3NewStars'};
			$sql_member{'member_attack_2NewStars'} = $row->{'member_attack_2NewStars'};
			$sql_member{'member_attack_1NewStars'} = $row->{'member_attack_1NewStars'};
			$sql_member{'member_attack_0NewStars'} = $row->{'member_attack_0NewStars'};
			$sql_member{'member_defense_used'} = $row->{'member_defense_used'};
			$sql_member{'member_defense_stars'} = $row->{'member_defense_stars'};
			$sql_member{'member_defense_newStars'} = $row->{'member_defense_newStars'};
			$sql_member{'member_defense_destruction'} = $row->{'member_defense_destruction'};
			$sql_member{'member_defense_3Stars'} = $row->{'member_defense_3Stars'};
			$sql_member{'member_defense_2Stars'} = $row->{'member_defense_2Stars'};
			$sql_member{'member_defense_1Stars'} = $row->{'member_defense_1Stars'};
			$sql_member{'member_defense_0Stars'} = $row->{'member_defense_0Stars'};
			$sql_member{'member_defense_3NewStars'} = $row->{'member_defense_3NewStars'};
			$sql_member{'member_defense_2NewStars'} = $row->{'member_defense_2NewStars'};
			$sql_member{'member_defense_1NewStars'} = $row->{'member_defense_1NewStars'};
			$sql_member{'member_defense_0NewStars'} = $row->{'member_defense_0NewStars'};
			$sql_member{'member_clean_cnt'} = $row->{'member_clean_cnt'};
			$sql_member{'member_loot_cnt'} = $row->{'member_loot_cnt'};
			$sql_member{'member_earlyLoot_cnt'} = $row->{'member_earlyLoot_cnt'};
			$sql_member{'member_noCC_cnt'} = $row->{'member_noCC_cnt'};
			if ( $debug ) { print "DEBUG:     $member{'name'} pulled successfully from member_wars.\n"; }
		}

		# check if member in member_wars
		if ( defined ( $sql_member{'member_attack_used'} ) ) {
			# update war counter for member
			if ( $test == 0 ) {
				$stmt_memberWars_update->execute ( $sql_member{'member_war'} + 1,
							$sql_member{'member_attack_used'},
							$sql_member{'member_attack_stars'},
							$sql_member{'member_attack_newStars'},
							$sql_member{'member_attack_destruction'},
							$sql_member{'member_attack_3Stars'},
							$sql_member{'member_attack_2Stars'},
							$sql_member{'member_attack_1Stars'},
							$sql_member{'member_attack_0Stars'},
							$sql_member{'member_attack_3NewStars'},
							$sql_member{'member_attack_2NewStars'},
							$sql_member{'member_attack_1NewStars'},
							$sql_member{'member_attack_0NewStars'},
							$sql_member{'member_defense_used'},
							$sql_member{'member_defense_stars'},
							$sql_member{'member_defense_newStars'},
							$sql_member{'member_defense_destruction'},
							$sql_member{'member_defense_3Stars'},
							$sql_member{'member_defense_2Stars'},
							$sql_member{'member_defense_1Stars'},
							$sql_member{'member_defense_0Stars'},
							$sql_member{'member_defense_3NewStars'},
							$sql_member{'member_defense_2NewStars'},
							$sql_member{'member_defense_1NewStars'},
							$sql_member{'member_defense_0NewStars'},
							$sql_member{'member_clean_cnt'},
							$sql_member{'member_loot_cnt'},
							$sql_member{'member_earlyLoot_cnt'},
							$sql_member{'member_noCC_cnt'},
							$member{'tag'} )
						or die "Unable to execute stmt_memberWars_update.  " . $stmt_memberWars_update->errstr;
				if ( $debug ) { print "DEBUG:     $member{'name'} updated successfully into wars.\n"; }
				$counter{'SQL update successful: API_Mem_Wars'}++;
			}
		} else {
			# insert member in member_wars
			if ( $test == 0 ) {
				$stmt_memberWars_insert->execute ( $member{'tag'}, 
							1,
							0,
							0,
							0,
							0,
							0,
							0,
							0,
							0,
							0,
							0,
							0,
							0,
							0,
							0,
							0,
							0,
							0,
							0,
							0,
							0)
						or die "Unable to execute stmt_memberWars_insert.  " . $stmt_memberWars_insert->errstr;
				if ( $debug ) { print "DEBUG:     $member{'name'} inserted successfully into wars.\n"; }
				$counter{'SQL insert successful: API_Mem_Wars'}++;
			}
		}
	}
	if ( $debug ) { print "DEBUG:   Inserting members for $warlog_json{$clan_tag}{'clan'}{'name'} finished.\n"; }
	
	# insert opponent members into sql
	if ( $debug ) { print "DEBUG:   Inserting members for $warlog_json{$clan_tag}{'opponent'}{'name'} started.\n"; }
	for my $member_href ( @{ $warlog_json{$clan_tag}{'opponent'}{'members'} } ) {
		my %member = %$member_href;

		if ( $test == 0 ) {
			$stmt_warlog_member_insert->execute ( $warlog_id,
						$warlog_json{$clan_tag}{'opponent'}{'tag'},,
						$member{'tag'},
						$member{'name'},
						$member{'townhallLevel'},
						0, # War Weight for opponent = 0.
						$member{'mapPosition'})
					or die "Unable to execute stmt_warlog_member_insert.  " . $stmt_warlog_member_insert->errstr;
			if ( $debug ) { print "DEBUG:     $member{'name'} inserted successfully.\n"; }
			$counter{'SQL insert successful: API_WarLog_Member'}++;
		}
	}
	if ( $debug ) { print "DEBUG:   Inserting members for $warlog_json{$clan_tag}{'opponent'}{'name'} finished.\n"; }
	
	# clean up sql statements
	$stmt_warlog_insert->finish();
	$stmt_warlog_opp_search->finish();	
	$stmt_memWW_search->finish();
	$stmt_warlog_member_insert->finish();
	$stmt_memberWars_search->finish();
	$stmt_memberWars_insert->finish();	
	$stmt_memberWars_update->finish();
	
	return $warlog_id;
}

##############################
# update_warlog_data
##############################
sub update_warlog_data {
	# get input variables
	my ( $warlog_id ) = @_;

	# set up sql statements
	my $stmt_warlog_update = $dbh->prepare ( get_warlog_update() )
							or die "Unable to prepare stmt_warlog_update.  " . $dbh->errstr;
	if ( $test == 0 ) {
		$stmt_warlog_update->execute ( $warlog_json{$clan_tag}{'clan'}{'stars'},
									 $warlog_json{$clan_tag}{'clan'}{'destructionPercentage'},
									 $warlog_json{$clan_tag}{'clan'}{'attacks'},
									 $warlog_json{$clan_tag}{'opponent'}{'stars'},
									 $warlog_json{$clan_tag}{'opponent'}{'destructionPercentage'},
									 $warlog_json{$clan_tag}{'opponent'}{'attacks'},
									 $warlog_id )
				or die "Unable to execute stmt_warlog_update.  " . $stmt_warlog_update->errstr;
		if ( $debug ) { print "DEBUG:   WarLog for $warlog_json{$clan_tag}{'clan'}{'name'} updated successfully\n"; }
		$counter{'SQL update successful: API_WarLog'}++;
	}
		
	# clean up sql statements
	$stmt_warlog_update->finish();
}

##############################
# send_mail
##############################
sub send_mail {
	if ( $debug ) { print "DEBUG: Sending email summary of wars.\n"; }
	# get input variables
	my ( $warlog_id ) = @_;
	
	#local variables
	my $to = get_mail_to();
	my $from = get_mail_from();
	my $cc = get_cc_from();
	my $subject = '';
		
	my $message = '<!doctype html>';
	$message .= '<html>';
	$message .= '<head>';
	$message .= '<style>';
	$message .= 'table, th, td {';
	$message .= '    border: none;';
	$message .= '}';
	$message .= 'table td {';
	$message .= '    padding: 5px;';
	$message .= '    text-align: center;';
	$message .= '}';
	$message .= 'table th {';
	$message .= '    background-color: black;';
	$message .= '    color: white;';
	$message .= '}';
	$message .= 'table#t01 {';
	$message .= '    border: 1px solid black;';
	$message .= '    border-collapse: collapse;';
	$message .= '}';
	$message .= 'td#td01 {';
	$message .= '    border: 1px solid black;';
	$message .= '    padding: 5px;';
	$message .= '}';
	$message .= 'td#td02 {';
	$message .= '    padding: 0px;';
	$message .= '	background-color: #009933;';
	$message .= '}';
	$message .= '</style>';
	$message .= '</head>';
	$message .= '<body>';
	
	# get battle message table
	my $message_battle = '<table style="width:100%">';
	$message_battle .= '  <tr>';
	$message_battle .= '    <th>' . $warlog_json{$clan_tag}{'clan'}{'name'} . '</th>';
	$message_battle .= '    <th width="70">VS</th> ';
	$message_battle .= '    <th>' . $warlog_json{$clan_tag}{'opponent'}{'name'} . '</th>';
	$message_battle .= '  </tr>';
	foreach my $battle ( @battle_hash ) {
		if ( defined ( $battle )) {
			if ( $battle->{'home_flag'} == 1 ) {
				$message_battle .= '    <tr>';
				$message_battle .= '    <td id="td02" colspan="2">';
				$message_battle .= '    	<table style="width:100%" id="t01">';
				$message_battle .= '        	<tr>';
				$message_battle .= '            	<td>' . $battle->{'attacker_mapPosition'} . '. ' . $battle->{'attacker_name'} . '</td>';
				$message_battle .= '                <td width="70">' . $battle->{'stars'} . '/' . $battle->{'newStars'} . ' Stars';
				$message_battle .= '                <br>' . $battle->{'destructionPercentage'} . '%</td>';
				$message_battle .= '        	</tr>';
				$message_battle .= '        </table>';
				$message_battle .= '    </td>';
				$message_battle .= '    <td id="td01">' . $battle->{'defender_mapPosition'} . '. ' . $battle->{'defender_name'} . '</td>';
				$message_battle .= '  </tr>';
			} else {
				$message_battle .= '  <tr>';
				$message_battle .= '    <td>' . $battle->{'defender_mapPosition'} . '. ' . $battle->{'defender_name'} . '</td>';
				$message_battle .= '    <td id="td02" colspan="2">';
				$message_battle .= '    	<table style="width:100%" id="t01">';
				$message_battle .= '        	<tr>';
				$message_battle .= '                <td width="70">' . $battle->{'stars'} . '/' . $battle->{'newStars'} . ' Stars';
				$message_battle .= '                <br>' . $battle->{'destructionPercentage'} . '%</td>';
				$message_battle .= '            	<td>' . $battle->{'attacker_mapPosition'} . '. ' . $battle->{'attacker_name'} . '</td>';
				$message_battle .= '        	</tr>';
				$message_battle .= '        </table>';
				$message_battle .= '    </td>';
				$message_battle .= '  </tr>';
			}
		}
	}
	$message_battle .= '</table>';
	
	my $color_home;
	my $color_opp;
	if (( $warlog_json{$clan_tag}{'clan'}{'stars'} > $warlog_json{$clan_tag}{'opponent'}{'stars'} )||(
			$warlog_json{$clan_tag}{'clan'}{'destructionPercentage'} > $warlog_json{$clan_tag}{'opponent'}{'destructionPercentage'} )) {
		$color_home = 'bgcolor="#009933"';
		$color_opp = 'bgcolor="#ff0000"';
	} elsif (( $warlog_json{$clan_tag}{'clan'}{'stars'} < $warlog_json{$clan_tag}{'opponent'}{'stars'} )||(
			$warlog_json{$clan_tag}{'clan'}{'destructionPercentage'} < $warlog_json{$clan_tag}{'opponent'}{'destructionPercentage'} )) {
		$color_home = 'bgcolor="#ff0000"';
		$color_opp = 'bgcolor="#009933"';
	} else {
		$color_home = 'bgcolor="#ffff00"';
		$color_opp = 'bgcolor="#ffff00"';
	}
	# get the summary message table
	my $message_summary = '<table style="width:100%">';
	$message_summary .= '  <tr>';
	$message_summary .= '    <th>' . $warlog_json{$clan_tag}{'clan'}{'name'} . '</th>';
	$message_summary .= '    <th width="50">VS</th> ';
	$message_summary .= '    <th>' . $warlog_json{$clan_tag}{'opponent'}{'name'} . '</th>';
	$message_summary .= '  </tr>';
	$message_summary .= '  <tr>';
	$message_summary .= '    <td ' . $color_home . '>' . $warlog_json{$clan_tag}{'clan'}{'stars'} . '</td>';
	$message_summary .= '    <td>Stars</td>';
	$message_summary .= '    <td ' . $color_opp . '>' . $warlog_json{$clan_tag}{'opponent'}{'stars'} . '</td>';
	$message_summary .= '  </tr>';
	$message_summary .= '  <tr>';
	$message_summary .= '    <td ' . $color_home . '>' . $warlog_json{$clan_tag}{'clan'}{'destructionPercentage'} . '</td>';
	$message_summary .= '    <td>Destruction</td>';
	$message_summary .= '    <td ' . $color_opp . '>' . $warlog_json{$clan_tag}{'opponent'}{'destructionPercentage'} . '</td>';
	$message_summary .= '  </tr>';
	$message_summary .= '  <tr>';
	$message_summary .= '    <td>' . $warlog_json{$clan_tag}{'clan'}{'attacks'} . '</td>';
	$message_summary .= '    <td>Attacks Used</td>';
	$message_summary .= '    <td>' . $warlog_json{$clan_tag}{'opponent'}{'attacks'} . '</td>';
	$message_summary .= '  </tr>';
	$message_summary .= '  <tr>';
	my $attacks_left = ( int( $warlog_json{$clan_tag}{'teamSize'} ) * 2 ) - $warlog_json{$clan_tag}{'clan'}{'attacks'};
	$message_summary .= '    <td>' . $attacks_left . '</td>';
	$message_summary .= '    <td>Attacks Left</td>';
	$attacks_left = ( int( $warlog_json{$clan_tag}{'teamSize'} ) * 2 ) - $warlog_json{$clan_tag}{'opponent'}{'attacks'};
	$message_summary .= '    <td>' . $attacks_left . '</td>';
	$message_summary .= '  </tr>';
	$message_summary .= '</table>';	



	if ( $war_flag == 2 ) {
		$subject = 'War:' . $warlog_json{$clan_tag}{'clan'}{'name'} . ' VS ' . $warlog_json{$clan_tag}{'opp'}{'name'};
		$message .= '<h1 align="center">War Update For ' . $warlog_json{$clan_tag}{'clan'}{'name'} . '</h1>';
		$message .= '<br>';
		$message .= '<h2 align="center">Battle Update</h3>';
		$message .= '<br>';
		$message .= $message_battle;
		$message .= '<br>';
		$message .= '<h2 align="center">War Status</h3>';
		$message .= '<br>';
		$message .= $message_summary;

	} elsif ( $war_flag == 3 ) {
		$subject = 'War:' . $warlog_json{$clan_tag}{'clan'}{'name'} . ' VS ' . $warlog_json{$clan_tag}{'opp'}{'name'};
		$message .= '<h1 align="center">War Summary For ' . $warlog_json{$clan_tag}{'clan'}{'name'} . '</h1>';
		$message .= '<br>';
		$message .= $message_summary;
		$message .= '<br>';
		$message .= '<h2 align="center">Battle Summary</h3>';
		$message .= '<br>';
		$message .= $message_battle;
	} else {
		$subject = 'War Error';
		$message .= '<h1>There was a error getting the info for war.</h1>';
	}
	$message .= '</body>';
	$message .= '</html>';
 
	open(MAIL, "|/usr/sbin/sendmail -t");
 
	# Email Header
	print MAIL "To: $to\n";
	print MAIL "From: $from\n";
	print MAIL "Subject: $subject\n";
	print MAIL "Content-type: text/html; charset=\"utf-8\"\n\n";
	# Email Body
	print MAIL $message;

	close(MAIL);
	if ( $debug ) { print "DEBUG: Email Sent Successfully\n"; }

}

