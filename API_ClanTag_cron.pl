#! /usr/bin/perl

use strict;
use warnings;

# Standard Libraries
use Data::Dumper;
use Time::Piece;
use DBI;
use Cwd;

# Special Libraries
use JSON::Parse 'parse_json';

# include files
my $dir = getcwd() . "/public_html/bin";
require $dir . '/API_SQL_Statements.pl';
require $dir . '/config.pl';

# SQL Variables
my $sql_platform = get_sql_platform ();
my $dbname =  get_dbname ();
my $servername = get_servername ();
my $username = get_username();
my $password = get_password();
my $dsn = "DBI:" . get_sql_platform() . ":" . get_dbname() . ":" . get_servername();

# Set up api token and url
my $api_clan_token = get_api_clan_token();
my $api_clantag_url = "https://api.clashofclans.com/v1/clans/";
my $api_member_token = get_api_member_token();
my $api_membertag_url = "https://api.clashofclans.com/v1/players/";

# load clan tags into array, Note leave the # off
my @clan_tags = get_clan_list();

# set up headers
my $json_header = "Accept: application/json";
my $api_clan_header = "authorization: Bearer " . $api_clan_token;
my $api_member_header = "authorization: Bearer " . $api_member_token;

# set up global variables
my %clan_json = ();
my %warlog_json = ();
my %counter = ();
my $member_json = ();
my $member_no;
our $clan_tag;
my @achiev_sql = ();
my @achiev_id = ();
my $achiev_flag = 0;
my @troops_sql = ();
my @troops_past = ();
my @troops_id = ();
my $troops_flag = 0;
my @heros_sql = ();
my @heros_past = ();
my @heros_id = ();
my $heros_flag = 0;
my @spells_sql = ();
my @spells_past = ();
my @spells_id = ();
my $spells_flag = 0;
my %warWeight_sql = ();
my %warWeight_total = ();

# get debug and test from config
my $debug = get_debug();
my $test = get_test();
# $test = 1;
# check for cli arguments
while ( @ARGV ) {
	if ( my $Parameter eq '-debug' ) {
		$debug = 1;
	} elsif ( $Parameter eq '-test' ) {
		$test = 1;
	}
} 

# display debug and test flags
if ( $debug == 1 ) { print "DEBUG: Debug (Verbose Logging) is ON\n"; }
if ( $test == 1 ) { print "DEBUG: Test (SQL Insert) is OFF\n"; }

# connect to MySQL database
my %attr = ( PrintError=>0,RaiseError=>1,PrintError=>0,ShowErrorStatement=>1 );
my $dbh = DBI->connect ( $dsn, $username, $password, \%attr );

# Get Greenwich time zone and convert to sql date-time stamp
my ($S, $M, $H, $d, $m, $Y) = gmtime(time);
$m += 1;
$Y += 1900;
my $time = sprintf("%04d-%02d-%02d %02d:%02d:%02d", $Y, $m, $d, $H, $M, $S);

# loop threw clan tag array
for $clan_tag ( @clan_tags ) {  
	clan_pull_sub();
}

# iterate threw clans
foreach $clan_tag ( @clan_tags ){
	if ( $debug ) { print "DEBUG: Start working on $clan_json{$clan_tag}{'name'} clan's data.\n"; }

	# enter clan data into sql
	clan_entry_sub();
	
	# check if war log is public
	if ( $clan_json{$clan_tag}{'isWarLogPublic'} == 1 ) {
		clan_warlog_sub();
	}

	# pull past member data from sql
	member_past_sub();
	
	# pull war weight for all items
	warWeight_pull_sub();

	# iterate threw members of clans
	for ( $member_no = 0; $member_no < $clan_json{$clan_tag}{'members'}; $member_no++ ) {
		if ( $debug ) { print "DEBUG:   Start working on $clan_json{$clan_tag}{'memberList'}[$member_no]{'name'} member's data.\n"; }
		
		# get the member general data from sql
		member_general_sub();
		
		# get the member achievement data into sql
		member_achiev_sub();
		
		# get the member troop data into sql
		member_troop_sub();
		
		# get the member hero data into sql
		member_hero_sub();
		
		# get the member spell data into sql
		member_spell_sub();

		# get the member war weight data into sql
		member_warWeight_sub();

		# check if the member is banned
		member_banned_sub();

		if ( $debug ) { print "DEBUG:   Finished working on $clan_json{$clan_tag}{'memberList'}[$member_no]{'name'} member's data.\n"; }
	}
	
	if ( $debug ) { print "DEBUG: Finished working on $clan_json{$clan_tag}{'name'} clan's data.\n"; }
}

# disconnect from the MySQL database
$dbh->disconnect();

# display counter
print "\n";
print "Summary\n";
print "-" x 50 . " " . "-" x 5 . "\n";
foreach my $key ( sort keys %counter ) {
	printf( "%-50s %5d\n", $key, $counter{$key} );
}



# print Dumper( %warWeight_total );

##############################
#
#  Subroutines
#
##############################

##############################
# member_banned_sub
##############################
sub member_banned_sub{
	if ( $debug ) { print "DEBUG:    Checking if $clan_json{$clan_tag}{'memberList'}[$member_no]{'name'} is banned.\n"; }
	$counter{'Banned member: Searched'}++;	
	# local variables
	my $banned_flag = 0;

	# set up sql statements
	my $stmt_banned_search = $dbh->prepare ( get_banned_search() )
							or die "Unable to prepare stmt_banned_search.  " . $dbh->errstr;
	my $stmt_banned_update = $dbh->prepare ( get_banned_update() )
							or die "Unable to prepare stmt_banned_update.  " . $dbh->errstr;
	
	# search sql for banned member
	$stmt_banned_search->execute ( $clan_json{$clan_tag}{'memberList'}[$member_no]{'tag'} )
				or die "Unable to execute stmt_banned_search.  " . $stmt_banned_search->errstr;
	
	while ( my $row = $stmt_banned_search->fetchrow_hashref ( ) ) {
		$banned_flag = 1;
		my $banned_notify = $row->{'banned_notify'};
		my $banned_notified = $row->{'banned_notified'};
		my $banned_notes = $row->{'banned_notes'};

		# check if banned
		if ( $banned_notify == 1 ) {
			$counter{'Banned member: Found'}++;	
			if ( $debug ) { print "DEBUG:     Banned member found.\n"; }
			# check if notified
			if ( $banned_notified == 0 ) {
				# prepare email
				my $to = get_mail_ban_to();
				my $from = get_mail_from();
				my $cc = get_cc_ban_from();
				my $subject = 'Banned Member Joined';
				
				my $message = '<!doctype html>';
				$message .= '<html>';
				$message .= ' <body>';
				$message .= '  <h2>' . $clan_json{$clan_tag}{'memberList'}[$member_no]{'name'} . '</h2>';
				$message .= '  <h3>' . $clan_json{$clan_tag}{'memberList'}[$member_no]{'tag'} . '</h3>';
				$message .= '  Notes: ' . $banned_notes . '<br>';
				$message .= ' </body>';
				$message .= '</html>';

				open(MAIL, "|/usr/sbin/sendmail -t");
			
				# Email Header
				print MAIL "To: $to\n";
				print MAIL "From: $from\n";
				print MAIL "Subject: $subject\n";
				print MAIL "Content-type: text/html; charset=\"utf-8\"\n\n";
				# Email Body
				print MAIL $message;

				close(MAIL);
				if ( $debug ) { print "DEBUG:     Banned member notificiation sent.\n"; }
				$counter{'Banned member: Notified'}++;

				# update notification
				$banned_notes .= " - Notification Sent " . $time;
				$banned_notified = 1;

				# update banned member in sql
				if ( $test == 0 ) {
					$stmt_banned_update->execute ( $banned_notify, 
								$banned_notified, 
								$time, 
								$banned_notes,
								$clan_json{$clan_tag}{'memberList'}[$member_no]{'tag'} )
						or die "Unable to execute stmt_banned_update.  " . $stmt_banned_update->errstr;
					$counter{'SQL update successful: API_Mem_Banned'}++;
					$counter{'Banned member: Updated'}++;
				}
			}
		}
		$counter{'SQL search successful: API_Mem_Banned'}++;	
		if ( $debug ) { print "DEBUG:     SQL searched for banned member.\n"; }
	}

	$stmt_banned_search->finish();
	$stmt_banned_update->finish();
}
##############################
# member_warWeight_sub
##############################
sub member_warWeight_sub{
	# local variables
	my %warWeight_past = ();
	my %warWeight_current = ();
	my $member_tag_temp = $clan_json{$clan_tag}{'memberList'}[$member_no]{'tag'};
	
	# set up sql statements
	my $stmt_memWW_search = $dbh->prepare ( get_memWW_search() )
							or die "Unable to prepare stmt_memWW_search.  " . $dbh->errstr;
	my $stmt_wwTH_search = $dbh->prepare ( get_wwTH_search() )
							or die "Unable to prepare stmt_wwTH_search.  " . $dbh->errstr;
	my $stmt_memWW_insert = $dbh->prepare ( get_memWW_insert() )
							or die "Unable to prepare stmt_memWW_insert.  " . $dbh->errstr;
	my $stmt_memWW_woDef_insert = $dbh->prepare ( get_memWW_woDef_insert() )
							or die "Unable to prepare stmt_memWW_woDef_insert.  " . $dbh->errstr;
	my $stmt_th_defBldgs_search = $dbh->prepare ( get_th_defBldgs_search() )
							or die "Unable to prepare stmt_th_defBldgs_search.  " . $dbh->errstr;
	my $stmt_defense_search = $dbh->prepare ( get_defense_search() )
							or die "Unable to prepare stmt_defense_search.  " . $dbh->errstr;
	my $stmt_defToMem_defBldg_search = $dbh->prepare ( get_defToMem_defBldg_search() )
							or die "Unable to prepare stmt_defToMem_defBldg_search.  " . $dbh->errstr;
	my $stmt_th_traps_search = $dbh->prepare ( get_th_traps_search() )
							or die "Unable to prepare stmt_th_traps_search.  " . $dbh->errstr;
	my $stmt_trap_search = $dbh->prepare ( get_trap_search() )
							or die "Unable to prepare stmt_trap_search.  " . $dbh->errstr;
	my $stmt_trapToMem_trap_search = $dbh->prepare ( get_trapToMem_trap_search() )
							or die "Unable to prepare stmt_trapToMem_trap_search.  " . $dbh->errstr;
	my $stmt_th_miscs_search = $dbh->prepare ( get_th_miscs_search() )
							or die "Unable to prepare stmt_th_miscs_search.  " . $dbh->errstr;
	my $stmt_misc_search = $dbh->prepare ( get_misc_search() )
							or die "Unable to prepare stmt_misc_search.  " . $dbh->errstr;
	my $stmt_miscToMem_misc_search = $dbh->prepare ( get_miscToMem_misc_search() )
							or die "Unable to prepare stmt_miscToMem_misc_search.  " . $dbh->errstr;
	my $stmt_th_nonWt_search = $dbh->prepare ( get_th_nonWt_search() )
							or die "Unable to prepare stmt_th_nonWt_search.  " . $dbh->errstr;
	my $stmt_nonWtToMem_nonWt_search = $dbh->prepare ( get_nonWtToMem_nonWt_search() )
							or die "Unable to prepare stmt_nonWtToMem_nonWt_search.  " . $dbh->errstr;
	my $stmt_th_troops_search = $dbh->prepare ( get_th_troops_search() )
							or die "Unable to prepare stmt_th_troops_search.  " . $dbh->errstr;
	my $stmt_th_heros_search = $dbh->prepare ( get_th_heros_search() )
							or die "Unable to prepare stmt_th_heros_search.  " . $dbh->errstr;
	my $stmt_th_spells_search = $dbh->prepare ( get_th_spells_search() )
							or die "Unable to prepare stmt_th_spells_search.  " . $dbh->errstr;
	my $stmt_memWW_Def_insert = $dbh->prepare ( get_memWW_Def_insert() )
							or die "Unable to prepare stmt_memWW_Def_insert.  " . $dbh->errstr;

	# pull war weight from sql for member
	if ( $debug ) { print "DEBUG:    Getting war weight data from sql for member $clan_json{$clan_tag}{'memberList'}[$member_no]{'name'}.\n"; }
	$stmt_memWW_search->execute ( $member_tag_temp )
							or die "Unable to execute stmt_memWW_search.  " . $stmt_memWW_search->errstr;

	# get results
	my $temp_warWeight_flag = 0;
	while ( my $row = $stmt_memWW_search->fetchrow_hashref ( ) ) {
		$warWeight_past{'memWW_defBldg'} = $row->{'memWW_defBldg'};
		$warWeight_past{'memWW_wall'} = $row->{'memWW_wall'};
		$warWeight_past{'memWW_trap'} = $row->{'memWW_trap'};
		$warWeight_past{'memWW_miscBldg'} = $row->{'memWW_miscBldg'};
		$warWeight_past{'memWW_hero_off'} = $row->{'memWW_hero_off'};
		$warWeight_past{'memWW_hero_def'} = $row->{'memWW_hero_def'};
		$warWeight_past{'memWW_troop'} = $row->{'memWW_troop'};
		$warWeight_past{'memWW_spell'} = $row->{'memWW_spell'};
		$warWeight_past{'memWW_defense'} = $row->{'memWW_defense'};
		$warWeight_past{'memWW_offense'} = $row->{'memWW_offense'};
		$warWeight_past{'memWW_total'} = $row->{'memWW_total'};
		$warWeight_past{'memWW_rounded'} = $row->{'memWW_rounded'};
		$warWeight_past{'memWW_Step1'} = $row->{'memWW_Step1'};
		$warWeight_past{'memWW_Step2'} = $row->{'memWW_Step2'};
		$warWeight_past{'memWW_Step3_OffRatio'} = $row->{'memWW_Step3_OffRatio'};
		$warWeight_past{'memWW_Step3_DefRatio'} = $row->{'memWW_Step3_DefRatio'};
		$warWeight_past{'memWW_Step3_DiffRatio'} = $row->{'memWW_Step3_DiffRatio'};
		$warWeight_past{'memWW_Step3'} = $row->{'memWW_Step3'};
		$warWeight_past{'memWW_Step4_Diff'} = $row->{'memWW_Step4_Diff'};
		$warWeight_past{'memWW_Step4'} = $row->{'memWW_Step4'};
		$temp_warWeight_flag = 1;
		$counter{'SQL search successful: API_Mem_WarWeight'}++;	
	}

	# check if we got previous war weight from sql
	if ( $temp_warWeight_flag == 0 ) {
		$warWeight_past{'memWW_defBldg'} = 0;
		$warWeight_past{'memWW_wall'} = 0;
		$warWeight_past{'memWW_trap'} = 0;
		$warWeight_past{'memWW_miscBldg'} = 0;
		$warWeight_past{'memWW_hero_off'} = 0;
		$warWeight_past{'memWW_hero_def'} = 0;
		$warWeight_past{'memWW_troop'} = 0;
		$warWeight_past{'memWW_spell'} = 0;
		$warWeight_past{'memWW_defense'} = 0;
		$warWeight_past{'memWW_offense'} = 0;
		$warWeight_past{'memWW_total'} = 0;
		$warWeight_past{'memWW_rounded'} = 0;
		$warWeight_past{'memWW_Step1'} = 0;
		$warWeight_past{'memWW_Step2'} = 0;
		$warWeight_past{'memWW_Step3_OffRatio'} = 0;
		$warWeight_past{'memWW_Step3_DefRatio'} = 0;
		$warWeight_past{'memWW_Step3_DiffRatio'} = 0;
		$warWeight_past{'memWW_Step3'} = 0;
		$warWeight_past{'memWW_Step4_Diff'} = 0;
		$warWeight_past{'memWW_Step4'} = 0;
	}

	# Get current war weights
	$warWeight_current{'memWW_defBldg'} = $warWeight_past{'memWW_defBldg'};
	$warWeight_current{'memWW_wall'} = $warWeight_past{'memWW_wall'};
	$warWeight_current{'memWW_trap'} = $warWeight_past{'memWW_trap'};
	$warWeight_current{'memWW_miscBldg'} = $warWeight_past{'memWW_miscBldg'};
	$warWeight_current{'memWW_defense'} = $warWeight_past{'memWW_defense'};
	$warWeight_current{'memWW_troop'} = $warWeight_total{$member_tag_temp}{'memWW_troop'};
	$warWeight_current{'memWW_spell'} = $warWeight_total{$member_tag_temp}{'memWW_spell'};
	$warWeight_current{'memWW_hero_off'} = $warWeight_total{$member_tag_temp}{'memWW_hero_off'};
	$warWeight_current{'memWW_hero_def'} = $warWeight_total{$member_tag_temp}{'memWW_hero_def'};

	# check if war weights are defined
	if ( !defined ( $warWeight_current{'memWW_defBldg'} )) {
		$warWeight_current{'memWW_defBldg'} = 0;
	}
	if ( !defined ( $warWeight_current{'memWW_wall'} )) {
		$warWeight_current{'memWW_wall'} = 0;
	}
	if ( !defined ( $warWeight_current{'memWW_trap'} )) {
		$warWeight_current{'memWW_trap'} = 0;
	}
	if ( !defined ( $warWeight_current{'memWW_miscBldg'} )) {
		$warWeight_current{'memWW_miscBldg'} = 0;
	}
	if ( !defined ( $warWeight_current{'memWW_troop'} )) {
		$warWeight_current{'memWW_troop'} = 0;
	}
	if ( !defined ( $warWeight_current{'memWW_spell'} )) {
		$warWeight_current{'memWW_spell'} = 0;
	}
	if ( !defined ( $warWeight_current{'memWW_hero_off'} )) {
		$warWeight_current{'memWW_hero_off'} = 0;
	}
	if ( !defined ( $warWeight_current{'memWW_hero_def'} )) {
		$warWeight_current{'memWW_hero_def'} = 0;
	}

	if ( $debug ) { print "DEBUG:    Computing current war weight for member $clan_json{$clan_tag}{'memberList'}[$member_no]{'name'}.\n"; }
	# computer current war weight's
	if ( $warWeight_current{'memWW_defBldg'} == 0 ) {
		$warWeight_current{'memWW_defense'} = $warWeight_current{'memWW_hero_def'};
	} else {
		$warWeight_current{'memWW_defense'} = $warWeight_current{'memWW_defBldg'} + $warWeight_current{'memWW_wall'} +
				$warWeight_current{'memWW_trap'} + $warWeight_current{'memWW_miscBldg'} + $warWeight_current{'memWW_hero_def'};
	}
	$warWeight_current{'memWW_offense'} = $warWeight_current{'memWW_hero_off'} + $warWeight_current{'memWW_troop'} +
			$warWeight_current{'memWW_spell'};
	$warWeight_current{'memWW_total'} = $warWeight_current{'memWW_offense'} + $warWeight_current{'memWW_defense'};

	# check if we have a total war weight
	if ( !defined ( $warWeight_current{'memWW_total'} )) {
		print ( "WARNING: No total war weight for $clan_json{$clan_tag}{'memberList'}[$member_no]{'name'}\n");
		$warWeight_current{'memWW_total'} = 0;
	}
	
	my $temp_current = sprintf( "%.1f", $warWeight_current{'memWW_total'} );
	my $temp_past = sprintf( "%.1f", $warWeight_past{'memWW_total'} );

	# check if war weight has changed
	if ( $temp_current ne $temp_past ) {
		# check if misc buildings are not entered for member
		if ( $warWeight_current{'memWW_defBldg'} == 0 ) {
			if ( $debug ) { print "DEBUG:    CMember $clan_json{$clan_tag}{'memberList'}[$member_no]{'name'} does not have defensive's entered.\n"; }
			# enter war weight into sql
			if ( $test == 0 ) {
				$stmt_memWW_woDef_insert->execute ( $member_tag_temp,
							$time, 
							$warWeight_current{'memWW_hero_off'},
							$warWeight_current{'memWW_hero_def'},
							$warWeight_current{'memWW_troop'},
							$warWeight_current{'memWW_spell'},
							$warWeight_current{'memWW_defense'},
							$warWeight_current{'memWW_offense'},
							$warWeight_current{'memWW_total'} )
						or die "Unable to execute stmt_memWW_woDef_insert.  " . $stmt_memWW_woDef_insert->errstr;
				if ( $debug ) { print "DEBUG:		War weight for $member_tag_temp inserted successfully.\n"; }
				$counter{'SQL insert successful: API_Mem_WarWeight'}++;	
			}
		} else {
			# $debug = 1;
			if ( $debug ) { print "DEBUG:    Check if member Member $clan_json{$clan_tag}{'memberList'}[$member_no]{'name'} is engineered.\n"; }
			$counter{'Engineered Check: Players'}++;
			# building entered, need to see if engineered
			my $step1_flag = 0;
			my $step2_flag = 0;
			my $step3_flag = 0;
			my $step3_off_level = 0;
			my $step3_def_level = 0;
			my $step3_off_total = 0;
			my $step3_def_total = 0;
			my $step4_flag = 0;
			my %temp_bldgDefs = ();
			my %temp_traps = ();
			my %temp_miscs = ();
			my %temp_nonWts = ();
			my %temp_troops = ();
			my %temp_heros = ();
			my %temp_spells = ();

			my $th_current = $member_json->{'townHallLevel'};
			my $th_past = $member_json->{'townHallLevel'} - 1;
			# get unadjusted war weight
			$warWeight_current{'memWW_rounded'} = 
						( int ( $warWeight_current{'memWW_total'} / 2000)) * 2000;

			#####################
			# Defense Buildings
			#####################
			# get current th def bldgs
			if ( $debug ) { print "DEBUG:     Getting max def bldg level for current th from sql.\n"; }
			$stmt_th_defBldgs_search->execute ( $th_current ) 
							or die "Unable to execute stmt_th_defBldgs_search.  " . $stmt_th_defBldgs_search->errstr;
			while ( my $row = $stmt_th_defBldgs_search->fetchrow_hashref ( ) ) {
				my $temp_id = $row->{'ww_build_id'};
				$temp_bldgDefs{$temp_id}{'currMaxLvl'} = $row->{'ww_level'};
				$temp_bldgDefs{$temp_id}{'name'} = $row->{'ww_name'};
				$counter{'SQL search successful: API_WarWeight'}++;
			}

			# get previous th def bldgs
			if ( $debug ) { print "DEBUG:     Getting max def bldg level for previous th from sql.\n"; }
			$stmt_th_defBldgs_search->execute ( $th_past ) 
							or die "Unable to execute stmt_th_defBldgs_search.  " . $stmt_th_defBldgs_search->errstr;
			while ( my $row = $stmt_th_defBldgs_search->fetchrow_hashref ( ) ) {
				 my $temp_id = $row->{'ww_build_id'};
				$temp_bldgDefs{$temp_id}{'prevMaxLvl'} = $row->{'ww_level'};
				$temp_bldgDefs{$temp_id}{'name'} = $row->{'ww_name'};
				$counter{'SQL search successful: API_WarWeight'}++;
			}

			# get count for def blgds
			if ( $debug ) { print "DEBUG:     Getting max def bldg count for current th from sql.\n"; }
			$stmt_defense_search->execute () 
							or die "Unable to execute stmt_defense_search.  " . $stmt_defense_search->errstr;
			while ( my $row = $stmt_defense_search->fetchrow_hashref ( ) ) {
				my $temp_id = $row->{'def_id'};
				my $temp_cnt = $row->{'def_cnt_th' . $th_current};
				if ( $temp_cnt != 0 ) {
					$temp_bldgDefs{$temp_id}{'currMaxCnt'} = $temp_cnt;
					$temp_bldgDefs{$temp_id}{'name'} = $row->{'def_name'};
				}
			}
			$counter{'SQL search successful: API_Mem_Defense'}++;

			# Get current def bldg from sql
			if ( $debug ) { print "DEBUG:     Getting current def bldg from sqls.\n"; }
			foreach my $temp_id ( keys %temp_bldgDefs ) {
				$stmt_defToMem_defBldg_search->execute ( $member_tag_temp, $temp_id ) 
								or die "Unable to execute stmt_defToMem_defBldg_search.  " . $stmt_defToMem_defBldg_search->errstr;
				while ( my $row = $stmt_defToMem_defBldg_search->fetchrow_hashref ( ) ) {
					$temp_bldgDefs{$temp_id}{'name'} = $row->{'defToMem_defense_name'};	
					for ( my $i = 1; $i <= 12; $i++ ) {
						my $temp_pos = $row->{'defToMem_pos' . $i};
						if ( defined ( $temp_pos )) {
							$temp_bldgDefs{$temp_id}{'pos'}{$i} = $temp_pos;
						}
					}				
				$counter{'SQL search successful: API_Mem_DefToMem'}++;
				}
			}

			# Check current def bldg for step 1 and 2
			if ( $debug ) { print "DEBUG:     Checking current def bldg for step 1 and 2.\n"; }
			foreach my $temp_id ( keys %temp_bldgDefs ) {
				# check walls seperatly
				if ( $temp_bldgDefs{$temp_id}{'name'} eq 'Wall' ) {
					my $temp_step2_flag = 0;
					my $temp_cnt = 0;
					for ( my $i = 1; $i <= $temp_bldgDefs{$temp_id}{'currMaxLvl'}; $i++ ) {
						# set wall pos to 0 if not defined
						if ( !defined ( $temp_bldgDefs{$temp_id}{'pos'}{$i} )) {
							$temp_bldgDefs{$temp_id}{'pos'}{$i} = 0;
						}
						# total all walls up
						$temp_cnt += $temp_bldgDefs{$temp_id}{'pos'}{$i};
						# check if level is below previous th
						if (( $temp_bldgDefs{$temp_id}{'pos'}{$i} > 0 )&&
								( $i < $temp_bldgDefs{$temp_id}{'prevMaxLvl'} )) {
							$temp_step2_flag = 1;
						}
						# compute defensive levels
						$step3_def_level += $temp_bldgDefs{$temp_id}{'pos'}{$i} * $i;
					}
					# check walls are built to level
					if ( $temp_bldgDefs{$temp_id}{'currMaxCnt'} > $temp_cnt ) {
						if ( $debug ) { print "DEBUG:       Step 1 positive for $temp_bldgDefs{$temp_id}{'name'}.\n"; }
						$step1_flag = 1;
						$counter{'Engineered Check: Step 1: Def Bldg: Positive'}++;
					}
					if ( $temp_step2_flag == 1 ) {
						if ( $debug ) { print "DEBUG:       Step 2 positive for $temp_bldgDefs{$temp_id}{'name'}.\n"; }
						$step2_flag = 1;
						$counter{'Engineered Check: Step 2: Def Bldg: Positive'}++;
					}
				} else {
					my $temp_step1_flag = 0;
					my $temp_step2_flag = 0;
					for ( my $i = 1; $i <= $temp_bldgDefs{$temp_id}{'currMaxCnt'}; $i++ ) {
						# check if level is not set
						if (( !defined ( $temp_bldgDefs{$temp_id}{'pos'}{$i} ))||
									( $temp_bldgDefs{$temp_id}{'pos'}{$i} == 0 )) {
							$temp_step1_flag = 1;
							$temp_bldgDefs{$temp_id}{'pos'}{$i} = 0;
						}						
						# check if level is below previous th
						if (( defined ( $temp_bldgDefs{$temp_id}{'prevMaxLvl'} ))&&
									( $temp_bldgDefs{$temp_id}{'prevMaxLvl'} > $temp_bldgDefs{$temp_id}{'pos'}{$i} )) {
							$temp_step2_flag = 1;
						}
						# compute defensive levels
						$step3_def_level += $temp_bldgDefs{$temp_id}{'pos'}{$i};
					}
					# check flag and message
					if ( $temp_step1_flag == 1 ) {
						if ( $debug ) { print "DEBUG:       Step 1 positive for $temp_bldgDefs{$temp_id}{'name'}.\n"; }
						$step1_flag = 1;
						$counter{'Engineered Check: Step 1: Def Bldg: Positive'}++;
					}
					if ( $temp_step2_flag == 1 ) {
						if ( $debug ) { print "DEBUG:       Step 2 positive for $temp_bldgDefs{$temp_id}{'name'}.\n"; }
						$step2_flag = 1;
						$counter{'Engineered Check: Step 2: Def Bldg: Positive'}++;
					}
				}
			}

			#####################
			# Traps
			#####################
			# get current th traps
			if ( $debug ) { print "DEBUG:     Getting max traps level for current th from sql.\n"; }
			$stmt_th_traps_search->execute ( $th_current ) 
							or die "Unable to execute stmt_th_traps_search.  " . $stmt_th_traps_search->errstr;
			while ( my $row = $stmt_th_traps_search->fetchrow_hashref ( ) ) {
				my $temp_id = $row->{'ww_trap_id'};
				$temp_traps{$temp_id}{'currMaxLvl'} = $row->{'ww_level'};
				$temp_traps{$temp_id}{'name'} = $row->{'ww_name'};
				$counter{'SQL search successful: API_WarWeight'}++;
			}

			# get previous th traps
			if ( $debug ) { print "DEBUG:     Getting max traps level for previous th from sql.\n"; }
			$stmt_th_traps_search->execute ( $th_past ) 
							or die "Unable to execute stmt_th_traps_search.  " . $stmt_th_traps_search->errstr;
			while ( my $row = $stmt_th_traps_search->fetchrow_hashref ( ) ) {
				 my $temp_id = $row->{'ww_trap_id'};
				$temp_traps{$temp_id}{'prevMaxLvl'} = $row->{'ww_level'};
				$temp_traps{$temp_id}{'name'} = $row->{'ww_name'};
				$counter{'SQL search successful: API_WarWeight'}++;
			}

			# get count for traps
			if ( $debug ) { print "DEBUG:     Getting max traps count for current th from sql.\n"; }
			$stmt_trap_search->execute () 
							or die "Unable to execute stmt_trap_search.  " . $stmt_trap_search->errstr;
			while ( my $row = $stmt_trap_search->fetchrow_hashref ( ) ) {
				my $temp_id = $row->{'trap_id'};
				my $temp_cnt = $row->{'trap_cnt_th' . $th_current};
				if ( $temp_cnt != 0 ) {
					$temp_traps{$temp_id}{'currMaxCnt'} = $temp_cnt;
					$temp_traps{$temp_id}{'name'} = $row->{'trap_name'};
				}
			}
			$counter{'SQL search successful: API_Mem_Traps'}++;

			# Get current traps from sql
			if ( $debug ) { print "DEBUG:     Getting current traps from sqls.\n"; }
			foreach my $temp_id ( keys %temp_traps ) {
				$stmt_trapToMem_trap_search->execute ( $member_tag_temp, $temp_id ) 
								or die "Unable to execute stmt_trapToMem_trap_search.  " . $stmt_trapToMem_trap_search->errstr;
				while ( my $row = $stmt_trapToMem_trap_search->fetchrow_hashref ( ) ) {
					$temp_traps{$temp_id}{'name'} = $row->{'trapToMem_trap_name'};	
					for ( my $i = 1; $i <= $temp_traps{$temp_id}{'currMaxCnt'}; $i++ ) {
						my $temp_pos = $row->{'trapToMem_pos' . $i};
						if ( defined ( $temp_pos )) {
							$temp_traps{$temp_id}{'pos'}{$i} = $temp_pos;
						}
					}				
				$counter{'SQL search successful: API_Mem_TrapToMem'}++;
				}
			}

			# Check current traps for step 1 and 2
			if ( $debug ) { print "DEBUG:     Checking current traps for step 1 and 2.\n"; }
			foreach my $temp_id ( keys %temp_traps ) {
				my $temp_step1_flag = 0;
				my $temp_step2_flag = 0;
				for ( my $i = 1; $i <= $temp_traps{$temp_id}{'currMaxCnt'}; $i++ ) {
					# check if level is not set
					if (( !defined ( $temp_traps{$temp_id}{'pos'}{$i} ))||
								( $temp_traps{$temp_id}{'pos'}{$i} == 0 )) {
						$temp_step1_flag = 1;
						$temp_traps{$temp_id}{'pos'}{$i} = 0;
					}						
					# check if level is below previous th
					if (( defined ( $temp_traps{$temp_id}{'prevMaxLvl'} ))&&
								( $temp_traps{$temp_id}{'prevMaxLvl'} > $temp_traps{$temp_id}{'pos'}{$i} )) {
						$temp_step2_flag = 1;
					}
					# compute defensive levels
					$step3_def_level += $temp_traps{$temp_id}{'pos'}{$i};
				}
				# check flag and message
				if ( $temp_step1_flag == 1 ) {
					if ( $debug ) { print "DEBUG:       Step 1 positive for $temp_traps{$temp_id}{'name'}.\n"; }
					$step1_flag = 1;
					$counter{'Engineered Check: Step 1: Traps: Positive'}++;
				}
				if ( $temp_step2_flag == 1 ) {
					if ( $debug ) { print "DEBUG:       Step 2 positive for $temp_traps{$temp_id}{'name'}.\n"; }
					$step2_flag = 1;
					$counter{'Engineered Check: Step 2: Traps: Positive'}++;
				}
			}

			#####################
			# Misc. Buildings
			#####################
			# get current th misc bldg
			if ( $debug ) { print "DEBUG:     Getting max misc bldg level for current th from sql.\n"; }
			$stmt_th_miscs_search->execute ( $th_current ) 
							or die "Unable to execute stmt_th_miscs_search.  " . $stmt_th_miscs_search->errstr;
			while ( my $row = $stmt_th_miscs_search->fetchrow_hashref ( ) ) {
				my $temp_id = $row->{'ww_misc_id'};
				$temp_miscs{$temp_id}{'currMaxLvl'} = $row->{'ww_level'};
				$temp_miscs{$temp_id}{'name'} = $row->{'ww_name'};
				$counter{'SQL search successful: API_WarWeight'}++;
			}

			# get previous th misc bldg
			if ( $debug ) { print "DEBUG:     Getting max misc bldg level for previous th from sql.\n"; }
			$stmt_th_miscs_search->execute ( $th_past ) 
							or die "Unable to execute stmt_th_miscs_search.  " . $stmt_th_miscs_search->errstr;
			while ( my $row = $stmt_th_miscs_search->fetchrow_hashref ( ) ) {
				 my $temp_id = $row->{'ww_misc_id'};
				$temp_miscs{$temp_id}{'prevMaxLvl'} = $row->{'ww_level'};
				$temp_miscs{$temp_id}{'name'} = $row->{'ww_name'};
				$counter{'SQL search successful: API_WarWeight'}++;
			}

			# get count for misc bldg
			if ( $debug ) { print "DEBUG:     Getting max misc bldg count for current th from sql.\n"; }
			$stmt_misc_search->execute () 
							or die "Unable to execute stmt_misc_search.  " . $stmt_misc_search->errstr;
			while ( my $row = $stmt_misc_search->fetchrow_hashref ( ) ) {
				my $temp_id = $row->{'misc_id'};
				my $temp_cnt = $row->{'misc_cnt_th' . $th_current};
				if ( $temp_cnt != 0 ) {
					$temp_miscs{$temp_id}{'currMaxCnt'} = $temp_cnt;
					$temp_miscs{$temp_id}{'name'} = $row->{'misc_name'};
				}
			}
			$counter{'SQL search successful: API_Mem_Misc'}++;

			# Get current misc bldg from sql
			if ( $debug ) { print "DEBUG:     Getting current misc bldg from sqls.\n"; }
			foreach my $temp_id ( keys %temp_miscs ) {
				$stmt_miscToMem_misc_search->execute ( $member_tag_temp, $temp_id ) 
								or die "Unable to execute stmt_miscToMem_misc_search.  " . $stmt_miscToMem_misc_search->errstr;
				while ( my $row = $stmt_miscToMem_misc_search->fetchrow_hashref ( ) ) {
					$temp_miscs{$temp_id}{'name'} = $row->{'miscToMem_misc_name'};	
					for ( my $i = 1; $i <= $temp_miscs{$temp_id}{'currMaxCnt'}; $i++ ) {
						my $temp_pos = $row->{'miscToMem_pos' . $i};
						if ( defined ( $temp_pos )) {
							$temp_miscs{$temp_id}{'pos'}{$i} = $temp_pos;
						}
					}				
				$counter{'SQL search successful: API_Mem_MiscToMem'}++;
				}
			}

			# Check current misc bldg for step 1 and 2
			if ( $debug ) { print "DEBUG:     Checking current misc bldg for step 1 and 2.\n"; }
			foreach my $temp_id ( keys %temp_miscs ) {
				my $temp_step1_flag = 0;
				my $temp_step2_flag = 0;
				for ( my $i = 1; $i <= $temp_miscs{$temp_id}{'currMaxCnt'}; $i++ ) {
					# check if level is not set
					if (( !defined ( $temp_miscs{$temp_id}{'pos'}{$i} ))||
								( $temp_miscs{$temp_id}{'pos'}{$i} == 0 )) {
						$temp_step1_flag = 1;
						$temp_miscs{$temp_id}{'pos'}{$i} = 0;
					}						
					# check if level is below previous th
					if (( defined ( $temp_miscs{$temp_id}{'prevMaxLvl'} ))&&
								( $temp_miscs{$temp_id}{'prevMaxLvl'} > $temp_miscs{$temp_id}{'pos'}{$i} )) {
						$temp_step2_flag = 1;
					}
					# compute defensive levels
					$step3_def_level += $temp_miscs{$temp_id}{'pos'}{$i};
				}
				# check flag and message
				if ( $temp_step1_flag == 1 ) {
					if ( $debug ) { print "DEBUG:       Step 1 positive for $temp_miscs{$temp_id}{'name'}.\n"; }
					$step1_flag = 1;
					$counter{'Engineered Check: Step 1: Misc Bldgs: Positive'}++;
				}
				if ( $temp_step2_flag == 1 ) {
					if ( $debug ) { print "DEBUG:       Step 2 positive for $temp_miscs{$temp_id}{'name'}.\n"; }
					$step2_flag = 1;
					$counter{'Engineered Check: Step 2: Misc Bldgs: Positive'}++;
				}
			}

			#####################
			# Non Weight Buildings
			#####################
			# get current th NonWt bldg
			if ( $debug ) { print "DEBUG:     Getting max non wt bldg level for current th from sql.\n"; }
			$stmt_th_nonWt_search->execute ( $th_current ) 
							or die "Unable to execute stmt_th_nonWt_search.  " . $stmt_th_nonWt_search->errstr;
			while ( my $row = $stmt_th_nonWt_search->fetchrow_hashref ( ) ) {
				my $temp_id = $row->{'ww_nonWt_id'};
				$temp_nonWts{$temp_id}{'currMaxLvl'} = $row->{'ww_level'};
				$temp_nonWts{$temp_id}{'name'} = $row->{'ww_name'};
				$counter{'SQL search successful: API_WarWeight'}++;
			}

			# Get current NonWt bldg from sql
			if ( $debug ) { print "DEBUG:     Getting current NonWt bldg from sqls.\n"; }
			foreach my $temp_id ( keys %temp_nonWts ) {
				$stmt_nonWtToMem_nonWt_search->execute ( $member_tag_temp, $temp_id ) 
								or die "Unable to execute stmt_nonWtToMem_nonWt_search.  " . $stmt_nonWtToMem_nonWt_search->errstr;
				while ( my $row = $stmt_nonWtToMem_nonWt_search->fetchrow_hashref ( ) ) {
					$temp_nonWts{$temp_id}{'name'} = $row->{'nonWtToMem_nonWt_name'};	
					for ( my $i = 1; $i <= 5; $i++ ) {
						my $temp_pos = $row->{'nonWtToMem_pos' . $i};
						if ( defined ( $temp_pos )) {
							$temp_nonWts{$temp_id}{'pos'}{$i} = $temp_pos;
						}
						if ((( $temp_nonWts{$temp_id}{'name'} eq 'Army Camp' )||
									( $temp_nonWts{$temp_id}{'name'} eq 'Clan Castle' ))&&
									( defined ( $temp_pos ))) {
							# compute defensive levels
							$step3_def_level += $temp_nonWts{$temp_id}{'pos'}{$i};
						}
					}				
				$counter{'SQL search successful: API_Mem_NonWtToMem'}++;
				}
			}

			#####################
			# Troops
			#####################
			# get current th troops
			if ( $debug ) { print "DEBUG:     Getting max troop level for current th from sql.\n"; }
			$stmt_th_troops_search->execute ( $th_current ) 
							or die "Unable to execute stmt_th_troops_search.  " . $stmt_th_troops_search->errstr;
			while ( my $row = $stmt_th_troops_search->fetchrow_hashref ( ) ) {
				 my $temp_id = $row->{'ww_troops_id'};
				$temp_troops{$temp_id}{'currMaxLvl'} = $row->{'ww_level'};
				$temp_troops{$temp_id}{'name'} = $row->{'ww_name'};
				$counter{'SQL search successful: API_WarWeight'}++;
			}

			# get previous th troops
			if ( $debug ) { print "DEBUG:     Getting max troop level for previous th from sql.\n"; }
			$stmt_th_troops_search->execute ( $th_past ) 
							or die "Unable to execute stmt_th_troops_search.  " . $stmt_th_troops_search->errstr;
			while ( my $row = $stmt_th_troops_search->fetchrow_hashref ( ) ) {
				 my $temp_id = $row->{'ww_troops_id'};
				$temp_troops{$temp_id}{'prevMaxLvl'} = $row->{'ww_level'};
				$temp_troops{$temp_id}{'name'} = $row->{'ww_name'};
				$counter{'SQL search successful: API_WarWeight'}++;
			}

			# get current troop levels
			if ( $debug ) { print "DEBUG:     Getting current troop's from data.\n"; }
			foreach my $troop_info ( @{ $member_json->{'troops'} } ) {
				my $temp_village = $troop_info->{'village'};
				if ( $temp_village eq 'home') {
					$temp_troops{$troop_info->{'id'}}{'currLvl'} = $troop_info->{'level'};
					$temp_troops{$troop_info->{'id'}}{'village'} = $temp_village;
				}
			}

			# Check current troops for step 1 and 2
			if ( $debug ) { print "DEBUG:     Checking current troops for step 1 and 2.\n"; }
			foreach my $temp_id ( keys %temp_troops ) {
				# check if level is not set
				if (( !defined ( $temp_troops{$temp_id}{'currLvl'}))||
							( $temp_troops{$temp_id}{'currLvl'} == 0 )) {
					if ( $debug ) { print "DEBUG:       Step 1 positive for $temp_troops{$temp_id}{'name'}.\n"; }
					$step1_flag = 1;
					$counter{'Engineered Check: Step 1: Troops: Positive'}++;
					$temp_troops{$temp_id}{'currLvl'} = 0;
				}
				# check if level is below previous th
				if (( defined ( $temp_troops{$temp_id}{'prevMaxLvl'}))&&
							( $temp_troops{$temp_id}{'prevMaxLvl'} > $temp_troops{$temp_id}{'currLvl'} )) {
					if ( $debug ) { print "DEBUG:       Step 2 positive for $temp_troops{$temp_id}{'name'}.\n"; }
					$step2_flag = 1;
					$counter{'Engineered Check: Step 2: Troops: Positive'}++;
				}
				# compute offensive levels
				$step3_off_level += $temp_troops{$temp_id}{'currLvl'};
			}

			#####################
			# Heros
			#####################
			# get current th heros
			if ( $debug ) { print "DEBUG:     Getting max hero level for current th from sql.\n"; }
			$stmt_th_heros_search->execute ( $th_current ) 
							or die "Unable to execute stmt_th_heros_search.  " . $stmt_th_heros_search->errstr;
			while ( my $row = $stmt_th_heros_search->fetchrow_hashref ( ) ) {
				my $temp_id = $row->{'ww_heros_id'};
				$temp_heros{$temp_id}{'currMaxLvl'} = $row->{'ww_level'};
				$temp_heros{$temp_id}{'name'} = $row->{'ww_name'};
				$counter{'SQL search successful: API_WarWeight'}++;
			}

			# get previous th heros
			if ( $debug ) { print "DEBUG:     Getting max hero level for previous th from sql.\n"; }
			$stmt_th_heros_search->execute ( $th_past ) 
							or die "Unable to execute stmt_th_heros_search.  " . $stmt_th_heros_search->errstr;
			while ( my $row = $stmt_th_heros_search->fetchrow_hashref ( ) ) {
				 my $temp_id = $row->{'ww_heros_id'};
				$temp_heros{$temp_id}{'prevMaxLvl'} = $row->{'ww_level'};
				$temp_heros{$temp_id}{'name'} = $row->{'ww_name'};
				$counter{'SQL search successful: API_WarWeight'}++;
			}

			# get current hero levels
			if ( $debug ) { print "DEBUG:     Getting current hero's from data.\n"; }
			foreach my $hero_info ( @{ $member_json->{'heroes'} } ) {
				my $temp_village = $hero_info->{'village'};
				if ( $temp_village eq 'home') {
					$temp_heros{$hero_info->{'id'}}{'currLvl'} = $hero_info->{'level'};
					$temp_heros{$hero_info->{'id'}}{'village'} = $temp_village;
				}
			}

			# Check current heros for step 1 and 2
			if ( $debug ) { print "DEBUG:     Checking current heros for step 1 and 2.\n"; }
			foreach my $temp_id ( keys %temp_heros ) {
				# check if level is not set
				if (( !defined ( $temp_heros{$temp_id}{'currLvl'}))||
							( $temp_heros{$temp_id}{'currLvl'} == 0 )) {
					if ( $debug ) { print "DEBUG:       Step 1 positive for $temp_heros{$temp_id}{'name'}.\n"; }
					$step1_flag = 1;
					$counter{'Engineered Check: Step 1: Heros: Positive'}++;
					$temp_heros{$temp_id}{'currLvl'} = 0;
				}
				# check if level is below previous th
				if (( defined ( $temp_heros{$temp_id}{'prevMaxLvl'}))&&
							( $temp_heros{$temp_id}{'prevMaxLvl'} > $temp_heros{$temp_id}{'currLvl'} )) {
					if ( $debug ) { print "DEBUG:       Step 2 positive for $temp_heros{$temp_id}{'name'}.\n"; }
					$step2_flag = 1;
					$counter{'Engineered Check: Step 2: Heros: Positive'}++;
				}
				# compute offensive levels
				$step3_off_level += $temp_heros{$temp_id}{'currLvl'};
			}

			#####################
			# Spells
			#####################
			# get current th spells
			if ( $debug ) { print "DEBUG:     Getting max spell level for current th from sql.\n"; }
			$stmt_th_spells_search->execute ( $th_current ) 
							or die "Unable to execute stmt_th_spells_search.  " . $stmt_th_spells_search->errstr;
			while ( my $row = $stmt_th_spells_search->fetchrow_hashref ( ) ) {
				 my $temp_id = $row->{'ww_spells_id'};
				$temp_spells{$temp_id}{'currMaxLvl'} = $row->{'ww_level'};
				$temp_spells{$temp_id}{'name'} = $row->{'ww_name'};
				$counter{'SQL search successful: API_WarWeight'}++;
			}

			# get previous th spells
			if ( $debug ) { print "DEBUG:     Getting max spell level for previous th from sql.\n"; }
			$stmt_th_spells_search->execute ( $th_past ) 
							or die "Unable to execute stmt_th_spells_search.  " . $stmt_th_spells_search->errstr;
			while ( my $row = $stmt_th_spells_search->fetchrow_hashref ( ) ) {
				 my $temp_id = $row->{'ww_spells_id'};
				$temp_spells{$temp_id}{'prevMaxLvl'} = $row->{'ww_level'};
				$temp_spells{$temp_id}{'name'} = $row->{'ww_name'};
				$counter{'SQL search successful: API_WarWeight'}++;
			}

			# get current spell levels
			if ( $debug ) { print "DEBUG:     Getting current spell's from data.\n"; }
			foreach my $spell_info ( @{ $member_json->{'spells'} } ) {
				my $temp_village = $spell_info->{'village'};
				if ( $temp_village eq 'home') {
					$temp_spells{$spell_info->{'id'}}{'currLvl'} = $spell_info->{'level'};
					$temp_spells{$spell_info->{'id'}}{'village'} = $temp_village;
				}
			}

			# Check current spells for step 1 and 2
			if ( $debug ) { print "DEBUG:     Checking current spells for step 1 and 2.\n"; }
			foreach my $temp_id ( keys %temp_spells ) {
				# check if level is not set
				if (( !defined ( $temp_spells{$temp_id}{'currLvl'}))||
							( $temp_spells{$temp_id}{'currLvl'} == 0 )) {
					if ( $debug ) { print "DEBUG:       Step 1 positive for $temp_spells{$temp_id}{'name'}.\n"; }
					$step1_flag = 1;
					$counter{'Engineered Check: Step 1: Spells: Positive'}++;
					$temp_spells{$temp_id}{'currLvl'} = 0;
				}
				# check if level is below previous th
				if (( defined ( $temp_spells{$temp_id}{'prevMaxLvl'}))&&
							( $temp_spells{$temp_id}{'prevMaxLvl'} > $temp_spells{$temp_id}{'currLvl'} )) {
					if ( $debug ) { print "DEBUG:       Step 2 positive for $temp_spells{$temp_id}{'name'}.\n"; }
					$step2_flag = 1;
					$counter{'Engineered Check: Step 2: Spells: Positive'}++;
				}
				# compute offensive levels
				$step3_off_level += $temp_spells{$temp_id}{'currLvl'};
			}

			# get total level for max th from sql
			$stmt_wwTH_search->execute ( 11 ) 
							or die "Unable to execute stmt_wwTH_search.  " . $stmt_wwTH_search->errstr;
			while ( my $row = $stmt_wwTH_search->fetchrow_hashref ( ) ) {
				 $step3_off_total = $row->{'WarWeight_OffLvlperTH'};
				 $step3_def_total = $row->{'WarWeight_DefLvlperTh'};
				$counter{'SQL search successful: API_WarWeight_TH'}++;
			}

			# compute step 3
			if ( $debug ) { print "DEBUG:     Checking for step 3.\n"; }
			my $step3_off_ratio = $step3_off_level / $step3_off_total * 100;
			my $step3_def_ratio = $step3_def_level / $step3_def_total * 100;
			my $step3_diff_ratio = abs ( $step3_off_ratio - $step3_def_ratio );

			if ( $step3_diff_ratio > 30 ) {
				if ( $debug ) { print "DEBUG:       Step 3 positive.\n"; }
				$step3_flag = 1;
				$counter{'Engineered Check: Step 3: Positive'}++;
			}

			# compute step 4
			if ( $debug ) { print "DEBUG:     Checking for step 4.\n"; }
			my $step4_diff = abs ( $warWeight_current{'memWW_offense'} - $warWeight_current{'memWW_defense'} );
			if ((( $step1_flag  == 1 )||( $step2_flag == 1 ))&&( $step3_flag == 1 )) {
				if ( $debug ) { print "DEBUG:       Step 4 positive.\n"; }
				$counter{'Engineered Check: Step 4: Positive'}++;
				if ( $step4_diff < 5000 ) {
					$step4_flag = 1;
				} elsif ( $step4_diff < 15000 ) {
					$step4_flag = 2;
				} else {
					$step4_flag = 3;
				}
			}

			# enter war weight into sql
			if ( $test == 0 ) {
				$stmt_memWW_Def_insert->execute ( $member_tag_temp,
							$time, 
							$warWeight_current{'memWW_defBldg'},
							$warWeight_current{'memWW_wall'},
							$warWeight_current{'memWW_trap'},
							$warWeight_current{'memWW_miscBldg'},
							$warWeight_current{'memWW_hero_off'},
							$warWeight_current{'memWW_hero_def'},
							$warWeight_current{'memWW_troop'},
							$warWeight_current{'memWW_spell'},
							$warWeight_current{'memWW_defense'},
							$warWeight_current{'memWW_offense'},
							$warWeight_current{'memWW_total'},
							$warWeight_current{'memWW_rounded'},
							$step1_flag, 
							$step2_flag, 
							$step3_off_ratio, 
							$step3_def_ratio, 
							$step3_diff_ratio, 
							$step3_flag, 
							$step4_diff, 
							$step4_flag )
						or die "Unable to execute stmt_memWW_Def_insert.  " . $stmt_memWW_Def_insert->errstr;
				if ( $debug ) { print "DEBUG:		War weight for $member_tag_temp inserted successfully.\n"; }
				$counter{'SQL insert successful: API_Mem_WarWeight'}++;	
			}

			if ( $debug ) {
				print "=================== \n";
				print "member_tag = ".$member_tag_temp."\n";
				print "th_current = ".$th_current."\n";
				print "step1_flag = ".$step1_flag."\n";
				print "step2_flag = ".$step2_flag."\n";
				print "step3_off_level = ".$step3_off_level."\n";
				print "step3_off_total = ".$step3_off_total."\n";
				print "step3_off_ratio = ".$step3_off_ratio."\n";
				print "step3_def_level = ".$step3_def_level."\n";
				print "step3_def_total = ".$step3_def_total."\n";
				print "step3_def_ratio = ".$step3_def_ratio."\n";
				print "step3_diff_ratio = ".$step3_diff_ratio."\n";
				print "step3_flag = ".$step3_flag."\n";
				print "step4_diff = ".$step4_diff."\n";
				print "step4_flag = ".$step4_flag."\n";
				# print "temp_nonWts \n=================== \n";
				# print Dumper(%temp_nonWts);
				print "=================== \n";
				$debug = 0;
			}
		}
	}

	# clear sql statements
	$stmt_memWW_search->finish();
	$stmt_wwTH_search->finish();
	$stmt_memWW_insert->finish();
	$stmt_memWW_woDef_insert->finish();
	$stmt_th_defBldgs_search->finish();
	$stmt_defense_search->finish();
	$stmt_defToMem_defBldg_search->finish();
	$stmt_th_traps_search->finish();
	$stmt_trap_search->finish();
	$stmt_trapToMem_trap_search->finish();
	$stmt_th_miscs_search->finish();
	$stmt_misc_search->finish();
	$stmt_miscToMem_misc_search->finish();
	$stmt_th_nonWt_search->finish();
	$stmt_nonWtToMem_nonWt_search->finish();
	$stmt_th_troops_search->finish();
	$stmt_th_heros_search->finish();
	$stmt_th_spells_search->finish();
	$stmt_memWW_Def_insert->finish();
}

##############################
# warWeight_pull_sub
##############################
sub warWeight_pull_sub {
	
	# set up sql statements
	my $stmt_ww_troops_search = $dbh->prepare ( get_ww_troops_search() )
							or die "Unable to prepare stmt_ww_troops_search.  " . $dbh->errstr;
	my $stmt_ww_heros_search = $dbh->prepare ( get_ww_heros_search() )
							or die "Unable to prepare stmt_ww_heros_search.  " . $dbh->errstr;
	my $stmt_ww_spells_search = $dbh->prepare ( get_ww_spells_search() )
							or die "Unable to prepare stmt_ww_spells_search.  " . $dbh->errstr;
	
	# pull war weight from sql for troops
	$stmt_ww_troops_search->execute ()
							or die "Unable to execute stmt_ww_troops_search.  " . $stmt_ww_troops_search->errstr;
	
	# check if there was results
	while ( my $row = $stmt_ww_troops_search->fetchrow_hashref ( ) ) {
		my $temp_id = $row->{'ww_troops_id'};
		my $temp_level = $row->{'ww_level'};
		$warWeight_sql{'troops'}{$temp_id}{'ww'}{$temp_level} = $row->{'ww_weight'};
		$warWeight_sql{'troops'}{$temp_id}{'th'}{$temp_level} = $row->{'ww_thLevel'};
		$counter{'SQL search successful: API_WarWeight'}++;	
	}

	# pull war weight from sql for heros
	$stmt_ww_heros_search->execute ()
							or die "Unable to execute stmt_ww_heros_search.  " . $stmt_ww_heros_search->errstr;
	
	# check if there was results
	while ( my $row = $stmt_ww_heros_search->fetchrow_hashref ( ) ) {
		my $temp_id = $row->{'ww_heros_id'};
		my $temp_level = $row->{'ww_level'};
		$warWeight_sql{'heros'}{$temp_id}{'ww_off'}{$temp_level} = $row->{'ww_weight'};
		$warWeight_sql{'heros'}{$temp_id}{'ww_def'}{$temp_level} = $row->{'ww_weight_def'};
		$warWeight_sql{'heros'}{$temp_id}{'th'}{$temp_level} = $row->{'ww_thLevel'};
		$counter{'SQL search successful: API_WarWeight'}++;	
	}

	# pull war weight from sql for spells
	$stmt_ww_spells_search->execute ()
							or die "Unable to execute stmt_ww_spells_search.  " . $stmt_ww_spells_search->errstr;
	
	# check if there was results
	while ( my $row = $stmt_ww_spells_search->fetchrow_hashref ( ) ) {
		my $temp_id = $row->{'ww_spells_id'};
		my $temp_level = $row->{'ww_level'};
		$warWeight_sql{'spells'}{$temp_id}{'ww'}{$temp_level} = $row->{'ww_weight'};
		$counter{'SQL search successful: API_WarWeight'}++;	
	}

	# clear sql statements
	$stmt_ww_troops_search->finish();
	$stmt_ww_heros_search->finish();
	$stmt_ww_spells_search->finish();
}

##############################
# clan_pull_sub
##############################
sub clan_pull_sub {
	if ( $debug ) { print "DEBUG: Getting json for clan $clan_tag.\n"; }
	
	# set up url
	my $clan_url = $api_clantag_url . '%23' . $clan_tag;
	my $warlog_url = $api_clantag_url . '%23' . $clan_tag . "/warlog";
	# get the clan in json
	my $http_response = qx{curl -si --header \'$json_header\' --header \"$api_clan_header\" $clan_url};
	my ( $http_head, $http_body ) = split ( m{\r?\n\r?\n}, $http_response );
	
	# check if we got error
	if ( $http_head !~ m#HTTP/1.1 200 OK# ) {
		print "ERROR: Did not get back clan web page. Check response code.\n";
		print "================\n";
		print "= HEADER START =\n";
		print "================\n\n";
		print "$http_head\n\n";
		print "================\n";
		print "== HEADER END ==\n";
		print "================\n";
		exit ();
	}
		
	# parse json to hash
	$clan_json{$clan_tag} = parse_json ( $http_body );
	
	# check if we parsed reply correctly
	if ( !$clan_json{$clan_tag} ) {
		print "ERROR: Unable to clan parse from API data.";
		exit ();
	}

	if ( $debug ) { print "DEBUG: Successful got clan data for $clan_tag.\n"; }
	$counter{'API pull successful: clans'}++;
	
	# check if war log is public
	if ( $clan_json{$clan_tag}{'isWarLogPublic'} == 1 ) {
		# get the war log in json
		my $http_response = qx{curl -si --header \'$json_header\' --header \"$api_clan_header\" $warlog_url};
		my ( $http_head, $http_body ) = split ( m{\r?\n\r?\n}, $http_response );
	
		# check if we got error
		if ( $http_head !~ m#HTTP/1.1 200 OK# ) {
			print "ERROR: Did not get back clan web page. Check response code.\n";
			print "================\n";
			print "= HEADER START =\n";
			print "================\n\n";
			print "$http_head\n\n";
			print "================\n";
			print "== HEADER END ==\n";
			print "================\n";
			exit ();
		}
		
		# parse json to hash
		$clan_json{$clan_tag}{'warlog'} = parse_json ( $http_body );
	
		# check if we parsed reply correctly
		if ( !$clan_json{$clan_tag}{'warlog'} ) {
			print "ERROR: Unable to warlog parse from API data.";
			exit ();
		}

		if ( $debug ) { print "DEBUG: Successful got warlog for $clan_tag.\n"; }
		$counter{'API pull successful: clans/warlog'}++;
	}
}

##############################
# clan_entry_sub
##############################
sub clan_entry_sub {
	# set up sql statements
	my $stmt_clan_warLosses_search = $dbh->prepare ( get_clan_warLosses_search() )
							or die "Unable to prepare stmt_clan_warLosses_search.  " . $dbh->errstr;
	my $stmt_clan_search = $dbh->prepare ( get_clan_search() )
							or die "Unable to prepare stmt_clan_search.  " . $dbh->errstr;
	my $stmt_clan_update = $dbh->prepare ( get_clan_update() )
							or die "Unable to prepare stmt_clan_update.  " . $dbh->errstr;
	my $stmt_clan_insert = $dbh->prepare ( get_clan_insert() )
							or die "Unable to prepare stmt_clan_insert.  " . $dbh->errstr;
	my $stmt_clan_info_insert = $dbh->prepare ( get_clan_info_insert() )
							or die "Unable to prepare stmt_clan_info_insert.  " . $dbh->errstr;
	
	# check if war log is private
	if ( $clan_json{$clan_tag}{'isWarLogPublic'} != 1 ) {
		if ( $debug ) { print "DEBUG:   War Log is private.\n"; }
		
		# set war flag to false
		delete $clan_json{$clan_tag}{'isWarLogPublic'};
		$clan_json{$clan_tag}{'isWarLogPublic'} = 0;
		
		# get most recent warTies and warLosses from db
		if ( $test == 0 ) {
			$stmt_clan_warLosses_search->execute( '#' . $clan_tag )
	 					or die "Unable to execute stmt_clan_warLosses_search.  " . $stmt_clan_warLosses_search->errstr;
			while (my @row = $stmt_clan_warLosses_search->fetchrow_array) { 
				# update warLosses and warTies
				$clan_json{$clan_tag}{'warLosses'} = pop @row;
				$clan_json{$clan_tag}{'warTies'} = pop @row;
				if ( $debug ) { print "DEBUG:   Updated war ties and losses from last entry.\n"; }
			
				# get time of last event and see if it is over a week old
				my $time_update = pop @row;
				my $time_formate = '%Y-%m-%d %H:%M:%S';
				my $time_diff = Time::Piece->strptime ( $time, $time_formate )
						 - Time::Piece->strptime ( $time_update, $time_formate );
				if ( $time_diff > 604800 ) {
					print "WARNING: The clans war log has not been updated in over a week\n";
				}
				$counter{'SQL search successful: API_Clan_Info'}++;	
			}
		}
		
		# Check if war losses are empty, set to 0
		if ( ! $clan_json{$clan_tag}{'warLosses'} ) {
			$clan_json{$clan_tag}{'warLosses'} = 0;
			if ( $debug ) { print "DEBUG:   War losses set to 0.\n"; }
		}
		
		# Check if war ties are empty, set to 0
		if ( ! $clan_json{$clan_tag}{'warTies'} ) {
			$clan_json{$clan_tag}{'warTies'} = 0;
			if ( $debug ) { print "DEBUG:   War ties set to 0.\n"; }
		}
		$counter{'Clan war log: private'}++;
	} 
	
	# check if clan is in clan table
	$stmt_clan_search->execute ( '#' . $clan_tag )
	 		or die "Unable to execute stmt_clan_search.  " . $stmt_clan_search->errstr;
	if ( $debug ) { print "DEBUG:   Clan $clan_json{$clan_tag}{'name'} select successfully.\n"; }
		
	# check if there was results
	my $clan_count = 0;
	while ( my $row = $stmt_clan_search->fetchrow_arrayref ( ) ) {
		# Update clan table
		$stmt_clan_update->execute ( $clan_json{$clan_tag}{'description'},
									 $clan_json{$clan_tag}{'badgeUrls'}{'small'},
									 $clan_json{$clan_tag}{'badgeUrls'}{'medium'},
									 $clan_json{$clan_tag}{'badgeUrls'}{'large'},
									 $clan_json{$clan_tag}{'location'}{'id'},
									 $clan_json{$clan_tag}{'location'}{'name'},
									 $clan_json{$clan_tag}{'location'}{'countryCode'},
									 $clan_json{$clan_tag}{'warFrequency'},
									 '#' . $clan_tag )
				or die "Unable to execute stmt_clan_update.  " . $stmt_clan_update->errstr;
				$counter{'SQL update successful: API_Clan'}++;	
		if ( $debug ) { print "DEBUG:   Clan $clan_json{$clan_tag}{'name'} update successfully.\n"; }
		$counter{'SQL search successful: API_Clan'}++;
		$clan_count++;
	}

	if ( $clan_count == 0 && $test == 0 ) {
		# Insert clan table
		$stmt_clan_insert->execute ( '#' . $clan_tag,
					$clan_json{$clan_tag}{'name'},
					$clan_json{$clan_tag}{'description'},
					$clan_json{$clan_tag}{'badgeUrls'}{'small'},
					$clan_json{$clan_tag}{'badgeUrls'}{'medium'},
					$clan_json{$clan_tag}{'badgeUrls'}{'large'},
					$clan_json{$clan_tag}{'location'}{'id'},
					$clan_json{$clan_tag}{'location'}{'name'},
					$clan_json{$clan_tag}{'location'}{'countryCode'},
					$clan_json{$clan_tag}{'warFrequency'} )
	 		or die "Unable to execute stmt_clan_insert.  " . $stmt_clan_insert->errstr;
		if ( $debug ) { print "DEBUG:   Clan $clan_json{$clan_tag}{'name'} inserted successfully.\n"; }
		$counter{'SQL insert successful: API_Clan'}++;
	}

	# insert clan info into clan_info table
	if ( $test == 0 ) {
		$stmt_clan_info_insert->execute ( $time,
					'#' . $clan_tag,
					$clan_json{$clan_tag}{'clanLevel'},
					$clan_json{$clan_tag}{'clanPoints'},
					$clan_json{$clan_tag}{'warWins'},
					$clan_json{$clan_tag}{'warWinStreak'},
					$clan_json{$clan_tag}{'warTies'},
					$clan_json{$clan_tag}{'warLosses'},
					$clan_json{$clan_tag}{'members'},
					$clan_json{$clan_tag}{'type'},
					$clan_json{$clan_tag}{'requiredTrophies'},
					$clan_json{$clan_tag}{'isWarLogPublic'} )
	 			or die "Unable to execute stmt_clan_info_insert.  " . $stmt_clan_info_insert->errstr;
		if ( $debug ) { print "DEBUG:   Clan info for $clan_json{$clan_tag}{'name'} inserted successfully.\n"; }
		$counter{'SQL insert successful: API_Clan_Info'}++;
	}
	# clean up sql statements
	$stmt_clan_warLosses_search->finish();
	$stmt_clan_search->finish();
	$stmt_clan_insert->finish();
	$stmt_clan_update->finish();
	$stmt_clan_info_insert->finish();

}

##############################
# clan_warlog_sub
##############################
sub clan_warlog_sub {
	if ( $debug ) { print "DEBUG:   War Log is public.\n"; }
	
	# setup sql statements
	my $stmt_warlog_search = $dbh->prepare ( get_warlog_search() )
							or die "Unable to prepare stmt_warlog_search.  " . $dbh->errstr;
	my $stmt_warlog_insert = $dbh->prepare ( get_warlog_insert() )
							or die "Unable to prepare stmt_warlog_insert.  " . $dbh->errstr;
	
	# get newest war from sql
	$stmt_warlog_search->execute ( '#' . $clan_tag )	
		or die "Unable to execute stmt_warlog_search.  " . $stmt_warlog_search->errstr;
	if ( $debug ) { print "DEBUG:   Warlog for $clan_json{$clan_tag}{'name'} select successfully.\n"; }
	$counter{'SQL search successful: API_WarLog'}++;	
	
	# check if there was results
	my $temp_opp_tag;
	while ( my $row = $stmt_warlog_search->fetchrow_hashref ( ) ) {
		$temp_opp_tag = $row->{'warlog_opp_tag'};
	}

	# iterate threw warlog
	for ( my $i = 0; $i < @{$clan_json{$clan_tag}{'warlog'}{'items'}}; $i++ ) {
		# check if this is different then the newest war
		my $test_opp_tag = $clan_json{$clan_tag}{'warlog'}{'items'}[$i]{'opponent'}{'tag'};
		
		if ( $temp_opp_tag !~ m/$test_opp_tag/ ) {
			if ( $debug ) { print "DEBUG:   Warlog for is not in sql.\n"; }

			# convert date
			if ( $clan_json{$clan_tag}{'warlog'}{'items'}[$i]{'endTime'} =~ 
					m/(\d\d\d\d)(\d\d)(\d\d)T(\d\d)(\d\d)(\d\d)/ ) {
				my $warlog_time = $1 . '-' . $2 . '-' . $3 . ' ' . $4 . ':' . $5 . ':' . $6;
				
				# add warlog
				if ( $test == 0 ) {
					$stmt_warlog_insert->execute ( $clan_json{$clan_tag}{'warlog'}{'items'}[$i]{'result'},
								0,
								$warlog_time,
								$clan_json{$clan_tag}{'warlog'}{'items'}[$i]{'teamSize'},
								$clan_json{$clan_tag}{'warlog'}{'items'}[$i]{'clan'}{'tag'},
								$clan_json{$clan_tag}{'warlog'}{'items'}[$i]{'clan'}{'clanLevel'},
								$clan_json{$clan_tag}{'warlog'}{'items'}[$i]{'clan'}{'stars'},
								$clan_json{$clan_tag}{'warlog'}{'items'}[$i]{'clan'}{'destructionPercentage'},
								$clan_json{$clan_tag}{'warlog'}{'items'}[$i]{'clan'}{'attacks'},
								$clan_json{$clan_tag}{'warlog'}{'items'}[$i]{'clan'}{'expEarned'},
								$clan_json{$clan_tag}{'warlog'}{'items'}[$i]{'opponent'}{'tag'},
								$clan_json{$clan_tag}{'warlog'}{'items'}[$i]{'opponent'}{'name'},
								$clan_json{$clan_tag}{'warlog'}{'items'}[$i]{'opponent'}{'clanLevel'},
								$clan_json{$clan_tag}{'warlog'}{'items'}[$i]{'opponent'}{'stars'},
								$clan_json{$clan_tag}{'warlog'}{'items'}[$i]{'opponent'}{'destructionPercentage'},
								$clan_json{$clan_tag}{'warlog'}{'items'}[$i]{'opponent'}{'badgeUrls'}{'small'},
								$clan_json{$clan_tag}{'warlog'}{'items'}[$i]{'opponent'}{'badgeUrls'}{'medium'},
								$clan_json{$clan_tag}{'warlog'}{'items'}[$i]{'opponent'}{'badgeUrls'}{'large'} )
							or die "Unable to execute stmt_warlog_insert.  " . $stmt_warlog_insert->errstr;
					if ( $debug ) { print "DEBUG:   WarLog for $clan_json{$clan_tag}{'name'} inserted successfully.\n"; }
					$counter{'SQL insert successful: API_WarLog'}++;
				}
			} else {
				print "ERROR: Unable to match warlog time\n";
			}
		} else {
			#skip the rest of the warlog
			last;
		}
	}
	$counter{'Clan war log: public'}++;
	
	# clean up sql
	$stmt_warlog_search->finish();
	$stmt_warlog_insert->finish();
}

##############################
# member_past_sub
##############################
sub member_past_sub {
	# set up sql statements
	my $stmt_achiev_search = $dbh->prepare ( get_achiev_search() )
							or die "Unable to prepare stmt_achiev_search.  " . $dbh->errstr;
	my $stmt_troops_search = $dbh->prepare ( get_troops_search() )
							or die "Unable to prepare stmt_troops_search.  " . $dbh->errstr;
	my $stmt_hero_search = $dbh->prepare ( get_hero_search() )
							or die "Unable to prepare stmt_hero_search.  " . $dbh->errstr;
	my $stmt_spell_search = $dbh->prepare ( get_spell_search() )
							or die "Unable to prepare stmt_spell_search.  " . $dbh->errstr;
	
	# Pull past achiev from sql
	if ( $debug ) { print "DEBUG:   Getting all achievements from sql.\n"; }
	$stmt_achiev_search->execute ()
	 		or die "Unable to execute stmt_achiev_search.  " . $stmt_achiev_search->errstr;

	# get the results
	while ( my $row = $stmt_achiev_search->fetchrow_hashref ( ) ) {
		my $temp_id = $row->{'achiev_id'};
		my $temp_name = $row->{'achiev_name'};
		my $temp_base = $row->{'achiev_base'};
		$achiev_id[$temp_base]{$temp_name} = $temp_id;
		$counter{'SQL search successful: API_Mem_Achiev'}++;	
	}

	# Pull past troops from sql
	if ( $debug ) { print "DEBUG:   Getting all troops from sql.\n"; }
	$stmt_troops_search->execute ()
	 		or die "Unable to execute stmt_troops_search.  " . $stmt_troops_search->errstr;

	# get the results
	my $j = 0;
	while ( my $row = $stmt_troops_search->fetchrow_hashref ( ) ) {
		my $temp_id = $row->{'troops_id'};
		my $temp_name = $row->{'troops_name'};
		my $temp_base = $row->{'troops_base'};
		$troops_past[$j]{'id'} = $temp_id;
		$troops_past[$j]{'name'} = $temp_name;
		$troops_past[$j]{'base'} = $temp_base;
		$troops_past[$j]{'order'} = $row->{'troops_order'};
		$troops_past[$j]{'maxLevel'} = $row->{'troops_maxLevel'};
		$troops_id[$temp_base]{$temp_name} = $temp_id;
		$j++;
		$counter{'SQL search successful: API_Mem_Troops'}++;	
	}

	# Pull past heros from sql
	if ( $debug ) { print "DEBUG:   Getting all heros from sql.\n"; }
	$stmt_hero_search->execute ()
	 		or die "Unable to execute stmt_hero_search.  " . $stmt_hero_search->errstr;

	# get the results
	$j = 0;
	while ( my $row = $stmt_hero_search->fetchrow_hashref ( ) ) {
		my $temp_id = $row->{'hero_id'};
		my $temp_name = $row->{'hero_name'};
		my $temp_base = $row->{'hero_base'};
		$heros_past[$j]{'id'} = $temp_id;
		$heros_past[$j]{'name'} = $temp_name;
		$heros_past[$j]{'base'} = $temp_base;
		$heros_past[$j]{'order'} = $row->{'hero_order'};
		$heros_past[$j]{'maxLevel'} = $row->{'hero_maxLevel'};
		$heros_id[$temp_base]{$temp_name} = $temp_id;
		$j++;
		$counter{'SQL search successful: API_Mem_Heros'}++;	
	}

	# Pull past spells from sql
	if ( $debug ) { print "DEBUG:   Getting all spells from sql.\n"; }
	$stmt_spell_search->execute ()
	 		or die "Unable to execute stmt_hero_search.  " . $stmt_hero_search->errstr;

	# get the results
	$j = 0;
	while ( my $row = $stmt_spell_search->fetchrow_hashref ( ) ) {
		my $temp_id = $row->{'spells_id'};
		my $temp_name = $row->{'spells_name'};
		my $temp_base = $row->{'spells_base'};		
		$spells_past[$j]{'id'} = $temp_id;
		$spells_past[$j]{'name'} = $temp_name;
		$spells_past[$j]{'maxLevel'} = $row->{'spells_maxLevel'};
		$spells_id[$temp_base]{$temp_name} = $temp_id;
		$j++;
		$counter{'SQL search successful: API_Mem_Spells'}++;	
	}

	# release sql statements
	$stmt_troops_search->finish();
	$stmt_hero_search->finish();
	$stmt_spell_search->finish();
}

##############################
# member_general_sub
##############################
sub member_general_sub {
	# set up sql statements
	my $stmt_league_search = $dbh->prepare ( get_league_search() )
							or die "Unable to prepare stmt_league_search.  " . $dbh->errstr;
	my $stmt_league_insert = $dbh->prepare ( get_league_insert() )
							or die "Unable to prepare stmt_league_insert.  " . $dbh->errstr;
	my $stmt_member_search = $dbh->prepare ( get_member_search() )
							or die "Unable to prepare stmt_member_search.  " . $dbh->errstr;
	my $stmt_member_insert = $dbh->prepare ( get_member_insert() )
							or die "Unable to prepare stmt_member_insert.  " . $dbh->errstr;
	my $stmt_member_time_update = $dbh->prepare ( get_member_time_update() )
							or die "Unable to prepare stmt_member_time_update.  " . $dbh->errstr;

	# Update league info if needed?
	$stmt_league_search->execute ( $clan_json{$clan_tag}{'memberList'}[$member_no]{'league'}{'id'} )
							or die "Unable to execute stmt_league_search.  " . $stmt_league_search->errstr;
	
	# check if there was results
	my $temp_league_id = 0;
	while ( my $row = $stmt_league_search->fetchrow_hashref ( ) ) {
		$temp_league_id = $row->{'league_id'};
		$counter{'SQL search successful: API_League'}++;	
	}

	# if no results insert league info
	if ( $temp_league_id == 0 && $test == 0 ) {
		# check that iconUrls-medium does not exists
		if ( ! $clan_json{$clan_tag}{'memberList'}[$member_no]{'league'}{'iconUrls'}{'medium'} ) {
			# set to nothing
			$clan_json{$clan_tag}{'memberList'}[$member_no]{'league'}{'iconUrls'}{'medium'} = ' ';
		}
		
		$stmt_league_insert->execute ( $clan_json{$clan_tag}{'memberList'}[$member_no]{'league'}{'id'},
					$clan_json{$clan_tag}{'memberList'}[$member_no]{'league'}{'name'},
					$clan_json{$clan_tag}{'memberList'}[$member_no]{'league'}{'iconUrls'}{'tiny'},
					$clan_json{$clan_tag}{'memberList'}[$member_no]{'league'}{'iconUrls'}{'small'},
					$clan_json{$clan_tag}{'memberList'}[$member_no]{'league'}{'iconUrls'}{'medium'} )
				or die "Unable to execute stmt_league_insert.  " . $stmt_league_insert->errstr;
		if ( $debug ) { print "DEBUG:   League $clan_json{$clan_tag}{'memberList'}[$member_no]{'league'}{'name'} inserted successfully.\n"; }
		$counter{'SQL insert successful: API_League'}++;
	}
	
	# grab last donations and donationsReceived entries from sql
	$stmt_member_search->execute ( $clan_json{$clan_tag}{'memberList'}[$member_no]{'tag'} )
				or die "Unable to execute stmt_member_search.  " . $stmt_member_search->errstr;
	
	while ( my $row = $stmt_member_search->fetchrow_hashref ( ) ) {
		$clan_json{$clan_tag}{'memberList'}[$member_no]{'member_id'} = $row->{'member_id'};
		$clan_json{$clan_tag}{'memberList'}[$member_no]{'member_timestamp'} = $row->{'member_timestamp'};
		$clan_json{$clan_tag}{'memberList'}[$member_no]{'member_clanRank'} = $row->{'member_clanRank'};
		$clan_json{$clan_tag}{'memberList'}[$member_no]{'member_previousClanRank'} = $row->{'member_previousClanRank'};
		$clan_json{$clan_tag}{'memberList'}[$member_no]{'member_expLevel'} = $row->{'member_expLevel'};
		$clan_json{$clan_tag}{'memberList'}[$member_no]{'member_role'} = $row->{'member_role'};
		$clan_json{$clan_tag}{'memberList'}[$member_no]{'member_townHallLevel'} = $row->{'member_townHallLevel'};
		$clan_json{$clan_tag}{'memberList'}[$member_no]{'member_trophies'} = $row->{'member_trophies'};
		$clan_json{$clan_tag}{'memberList'}[$member_no]{'member_bestTrophies'} = $row->{'member_bestTrophies'};
		$clan_json{$clan_tag}{'memberList'}[$member_no]{'member_warStars'} = $row->{'member_warStars'};
		$clan_json{$clan_tag}{'memberList'}[$member_no]{'member_attackWins'} = $row->{'member_attackWins'};
		$clan_json{$clan_tag}{'memberList'}[$member_no]{'member_defenseWins'} = $row->{'member_defenseWins'};
		$clan_json{$clan_tag}{'memberList'}[$member_no]{'member_builderHallLevel'} = $row->{'member_builderHallLevel'};
		$clan_json{$clan_tag}{'memberList'}[$member_no]{'member_versusTrophies'} = $row->{'member_versusTrophies'};
		$clan_json{$clan_tag}{'memberList'}[$member_no]{'member_bestVersusTrophies'} = $row->{'member_bestVersusTrophies'};
		$clan_json{$clan_tag}{'memberList'}[$member_no]{'member_versusBattleWinCount'} = $row->{'member_versusBattleWinCount'};
		$clan_json{$clan_tag}{'memberList'}[$member_no]{'member_versusBattleWins'} = $row->{'member_versusBattleWins'};
		$clan_json{$clan_tag}{'memberList'}[$member_no]{'last_donations'} = $row->{'member_donations'};
		$clan_json{$clan_tag}{'memberList'}[$member_no]{'last_total_donations'} = $row->{'member_total_donations'};
		$clan_json{$clan_tag}{'memberList'}[$member_no]{'last_donationsReceived'} = $row->{'member_donationsReceived'};
		$clan_json{$clan_tag}{'memberList'}[$member_no]{'last_total_donationsReceived'} = $row->{'member_total_donationsReceived'};
		$counter{'SQL search successful: API_WarLog_Member'}++;	
	}

	# check if member was in table
	if ( defined  ( $clan_json{$clan_tag}{'memberList'}[$member_no]{'last_donations'} ) ) {
		# check if donations are lower, higher, or equal and update accordingly
		if ( $clan_json{$clan_tag}{'memberList'}[$member_no]{'donations'} < 
				$clan_json{$clan_tag}{'memberList'}[$member_no]{'last_donations'} ) {
			$clan_json{$clan_tag}{'memberList'}[$member_no]{'total_donations'} = 
					$clan_json{$clan_tag}{'memberList'}[$member_no]{'last_total_donations'} + 
					$clan_json{$clan_tag}{'memberList'}[$member_no]{'donations'};
		} elsif ( $clan_json{$clan_tag}{'memberList'}[$member_no]{'donations'} > 
				$clan_json{$clan_tag}{'memberList'}[$member_no]{'last_donations'} ) {
			$clan_json{$clan_tag}{'memberList'}[$member_no]{'total_donations'} = 
					$clan_json{$clan_tag}{'memberList'}[$member_no]{'last_total_donations'} + 
					$clan_json{$clan_tag}{'memberList'}[$member_no]{'donations'} - 
					$clan_json{$clan_tag}{'memberList'}[$member_no]{'last_donations'};
		} else {
			$clan_json{$clan_tag}{'memberList'}[$member_no]{'total_donations'} = 
					$clan_json{$clan_tag}{'memberList'}[$member_no]{'last_total_donations'};
		}
	
		# check if donationsReceived are lower, higher, or equal and update accordingly
		if ( $clan_json{$clan_tag}{'memberList'}[$member_no]{'donationsReceived'} < 
				$clan_json{$clan_tag}{'memberList'}[$member_no]{'last_donationsReceived'} ) {
			$clan_json{$clan_tag}{'memberList'}[$member_no]{'total_donationsReceived'} = 
					$clan_json{$clan_tag}{'memberList'}[$member_no]{'last_total_donationsReceived'} + 
					$clan_json{$clan_tag}{'memberList'}[$member_no]{'donationsReceived'};
		} elsif ( $clan_json{$clan_tag}{'memberList'}[$member_no]{'donationsReceived'} > 
				$clan_json{$clan_tag}{'memberList'}[$member_no]{'last_donationsReceived'} ) {
			$clan_json{$clan_tag}{'memberList'}[$member_no]{'total_donationsReceived'} = 
					$clan_json{$clan_tag}{'memberList'}[$member_no]{'last_total_donationsReceived'} + 
					$clan_json{$clan_tag}{'memberList'}[$member_no]{'donationsReceived'} - 
					$clan_json{$clan_tag}{'memberList'}[$member_no]{'last_donationsReceived'};
		} else {
			$clan_json{$clan_tag}{'memberList'}[$member_no]{'total_donationsReceived'} = 
					$clan_json{$clan_tag}{'memberList'}[$member_no]{'last_total_donationsReceived'};
		}
	} else {
		$clan_json{$clan_tag}{'memberList'}[$member_no]{'total_donations'} = 
				$clan_json{$clan_tag}{'memberList'}[$member_no]{'donations'};
		$clan_json{$clan_tag}{'memberList'}[$member_no]{'total_donationsReceived'} = 
				$clan_json{$clan_tag}{'memberList'}[$member_no]{'donationsReceived'};			
	} 

	# pull member info from api
	if ( $debug ) { print "DEBUG:    Getting json for member $clan_json{$clan_tag}{'memberList'}[$member_no]{'name'}.\n"; }

	# set up url
	my $member_tag_temp = $clan_json{$clan_tag}{'memberList'}[$member_no]{'tag'};
	$member_tag_temp =~ s/#/%23/;
	my $member_url = $api_membertag_url . $member_tag_temp;
	# get the member in json
	my $http_response = qx{curl -si --header \'$json_header\' --header \"$api_member_header\" $member_url};
	my ( $http_head, $http_body ) = split ( m{\r?\n\r?\n}, $http_response );

	# check if we got error
	if ( $http_head !~ m#HTTP/1.1 200 OK# ) {
		print "ERROR: Did not get back member web page. Check response code.\n";
		print "================\n";
		print "= HEADER START =\n";
		print "================\n\n";
		print "$http_head\n\n";
		print "================\n";
		print "== HEADER END ==\n";
		print "================\n";
		exit ();
	}
	
	# parse json to hash
	$member_json = parse_json ( $http_body );
	
	# check if we parsed reply correctly
	if ( !$member_json ) {
		print "ERROR: Unable to member parse from API data.";
		exit ();
	}

	$counter{'API pull successful: players'}++;
	
	# check if builder base has been started
	if ( !defined ( $member_json->{'builderHallLevel'} )) {
		$member_json->{'builderHallLevel'} = 0;
	}
	
	# update role
	if ( $clan_json{$clan_tag}{'memberList'}[$member_no]{'role'} eq 'leader' ) {
		$clan_json{$clan_tag}{'memberList'}[$member_no]{'role'} = 'Leader';
	} elsif ( $clan_json{$clan_tag}{'memberList'}[$member_no]{'role'} eq 'coLeader' ) {
		$clan_json{$clan_tag}{'memberList'}[$member_no]{'role'} = 'Coleader';
	} elsif ( $clan_json{$clan_tag}{'memberList'}[$member_no]{'role'} eq 'admin' ) {
		$clan_json{$clan_tag}{'memberList'}[$member_no]{'role'} = 'Elder';
	} else {
		$clan_json{$clan_tag}{'memberList'}[$member_no]{'role'} = 'Member';
	}
	
	# check if anything has changed for member
	if (( $clan_json{$clan_tag}{'memberList'}[$member_no]{'member_clanRank'} ne $clan_json{$clan_tag}{'memberList'}[$member_no]{'clanRank'})||
				( $clan_json{$clan_tag}{'memberList'}[$member_no]{'member_previousClanRank'} ne $clan_json{$clan_tag}{'memberList'}[$member_no]{'previousClanRank'} )||
				( $clan_json{$clan_tag}{'memberList'}[$member_no]{'member_expLevel'} ne $clan_json{$clan_tag}{'memberList'}[$member_no]{'expLevel'} )||
				( $clan_json{$clan_tag}{'memberList'}[$member_no]{'member_role'} ne $clan_json{$clan_tag}{'memberList'}[$member_no]{'role'} )||
				( $clan_json{$clan_tag}{'memberList'}[$member_no]{'member_townHallLevel'} ne $member_json->{'townHallLevel'} )||
				( $clan_json{$clan_tag}{'memberList'}[$member_no]{'member_trophies'} ne $clan_json{$clan_tag}{'memberList'}[$member_no]{'trophies'} )||
				( $clan_json{$clan_tag}{'memberList'}[$member_no]{'member_bestTrophies'} ne $member_json->{'bestTrophies'} )||
				( $clan_json{$clan_tag}{'memberList'}[$member_no]{'member_warStars'} ne $member_json->{'warStars'} )||
				( $clan_json{$clan_tag}{'memberList'}[$member_no]{'member_attackWins'} ne $member_json->{'attackWins'} )||
				( $clan_json{$clan_tag}{'memberList'}[$member_no]{'member_defenseWins'} ne $member_json->{'defenseWins'} )||
				( $clan_json{$clan_tag}{'memberList'}[$member_no]{'member_builderHallLevel'} ne $member_json->{'builderHallLevel'} )||
				( $clan_json{$clan_tag}{'memberList'}[$member_no]{'member_versusTrophies'} ne $member_json->{'versusTrophies'} )||
				( $clan_json{$clan_tag}{'memberList'}[$member_no]{'member_bestVersusTrophies'} ne $member_json->{'bestVersusTrophies'} )||
				( $clan_json{$clan_tag}{'memberList'}[$member_no]{'member_versusBattleWinCount'} ne $member_json->{'versusBattleWinCount'} )||
				( $clan_json{$clan_tag}{'memberList'}[$member_no]{'member_versusBattleWins'} ne $member_json->{'versusBattleWins'} )||
				( $clan_json{$clan_tag}{'memberList'}[$member_no]{'last_donations'} ne $clan_json{$clan_tag}{'memberList'}[$member_no]{'donations'} )||
				( $clan_json{$clan_tag}{'memberList'}[$member_no]{'last_total_donations'} ne $clan_json{$clan_tag}{'memberList'}[$member_no]{'total_donations'} )||
				( $clan_json{$clan_tag}{'memberList'}[$member_no]{'last_donationsReceived'} ne $clan_json{$clan_tag}{'memberList'}[$member_no]{'donationsReceived'} )||
				( $clan_json{$clan_tag}{'memberList'}[$member_no]{'last_total_donationsReceived'} ne $clan_json{$clan_tag}{'memberList'}[$member_no]{'total_donationsReceived'} )) {
		# insert into members table
		if ( $test == 0 ) {
			$stmt_member_insert->execute ( $clan_json{$clan_tag}{'memberList'}[$member_no]{'tag'},
						'#' . $clan_tag,
						$clan_json{$clan_tag}{'memberList'}[$member_no]{'league'}{'id'},
						$clan_json{$clan_tag}{'memberList'}[$member_no]{'name'},
						$time,
						$time,
						$clan_json{$clan_tag}{'memberList'}[$member_no]{'clanRank'},
						$clan_json{$clan_tag}{'memberList'}[$member_no]{'previousClanRank'},
						$clan_json{$clan_tag}{'memberList'}[$member_no]{'expLevel'},
						$clan_json{$clan_tag}{'memberList'}[$member_no]{'role'},
						$member_json->{'townHallLevel'},
						$clan_json{$clan_tag}{'memberList'}[$member_no]{'trophies'}, 
						$member_json->{'bestTrophies'},
						$member_json->{'warStars'},
						$member_json->{'attackWins'},
						$member_json->{'defenseWins'},
						$member_json->{'builderHallLevel'}, 
						$member_json->{'versusTrophies'}, 
						$member_json->{'bestVersusTrophies'}, 
						$member_json->{'versusBattleWinCount'}, 
						$member_json->{'versusBattleWins'}, 
						$clan_json{$clan_tag}{'memberList'}[$member_no]{'donations'}, 
						$clan_json{$clan_tag}{'memberList'}[$member_no]{'total_donations'}, 
						$clan_json{$clan_tag}{'memberList'}[$member_no]{'donationsReceived'}, 
						$clan_json{$clan_tag}{'memberList'}[$member_no]{'total_donationsReceived'} )
					or die "Unable to execute stmt_member_insert.  " . $stmt_member_insert->errstr;
			if ( $debug ) { print "DEBUG:    Member $clan_json{$clan_tag}{'memberList'}[$member_no]{'name'} inserted successfully.\n"; }
			$counter{'SQL insert successful: API_Member'}++;
		}
	} else {
	# 		sub get_member_time_update {
	# return "UPDATE `API_Member` 
	# 	SET `member_current_timestamp` = ? 
	# 	WHERE `API_Member`.`member_id` = ?";
		# update time in members table
		if ( $test == 0 ) {
			$stmt_member_time_update->execute ( $time,
						$clan_json{$clan_tag}{'memberList'}[$member_no]{'member_id'} )
					or die "Unable to execute stmt_member_time_update.  " . $stmt_member_time_update->errstr;
			if ( $debug ) { print "DEBUG:    Member $clan_json{$clan_tag}{'memberList'}[$member_no]{'name'} updated successfully.\n"; }
			$counter{'SQL update successful: API_Member'}++;
		}
	}
	# clear sql statements
	$stmt_league_search->finish();
	$stmt_league_insert->finish();
	$stmt_member_search->finish();
	$stmt_member_insert->finish();
	$stmt_member_time_update->finish();
}

##############################
# member_achiev_sub
##############################
sub member_achiev_sub {
	# set up sql statements
	my $stmt_achievToMem_search = $dbh->prepare ( get_achievToMem_group_search() )
							or die "Unable to prepare stmt_achievToMem_search.  " . $dbh->errstr;
	my $stmt_achievToMem_insert = $dbh->prepare ( get_achievToMem_insert() )
							or die "Unable to prepare stmt_achievToMem_insert.  " . $dbh->errstr;
	my $stmt_achiev_insert = $dbh->prepare ( get_achiev_insert() )
							or die "Unable to prepare stmt_achiev_insert.  " . $dbh->errstr;
	
	# Pull current member achievements from sql
	if ( $debug ) { print "DEBUG:    Getting achievement from sql for member $clan_json{$clan_tag}{'memberList'}[$member_no]{'name'}.\n"; }
	$stmt_achievToMem_search->execute ( $clan_json{$clan_tag}{'memberList'}[$member_no]{'tag'} )
				or die "Unable to execute stmt_achievToMem_search.  " . $stmt_achievToMem_search->errstr;
	
	# get the results
	my $j = 0;
	while ( my $row = $stmt_achievToMem_search->fetchrow_hashref ( ) ) {
		$achiev_sql[$j]{'name'} = $row->{'achievToMem_achiev_name'};
		$achiev_sql[$j]{'id'} = $row->{'achievToMem_achiev_id'};
		$achiev_sql[$j]{'base'} = $row->{'achiev_base'};
		$achiev_sql[$j]{'order'} = $row->{'achiev_order'};
		$achiev_sql[$j]{'value'} = $row->{'achievToMem_value'};
		$j++;
		$counter{'SQL search successful: API_Mem_AchievToMem'}++;	
	}
			
	# iterate threw members current achievements
	foreach my $achiev_current ( @{ $member_json->{'achievements'} } ) {
		# check if completionInfo is blank and fill if so
		if ( ! $achiev_current->{'completionInfo'} ) {
			$achiev_current->{'completionInfo'} = '';
		}
		
		if ( $achiev_current->{'village'} eq 'home' ) {
			$achiev_current->{'base'} = 1;
		} else {
			$achiev_current->{'base'} = 2;
		}
		
		# get troop id
		$achiev_current->{'id'} = $achiev_id[$achiev_current->{'base'}]{$achiev_current->{'name'}};
		
		# check for past achiev
		if ( !@achiev_sql ) {
			if ( $test == 0 ) {
					# insert member achiev into sql
					$stmt_achievToMem_insert->execute ( $clan_json{$clan_tag}{'memberList'}[$member_no]{'tag'},
									 $achiev_current->{'id'},
									 $achiev_current->{'name'},
									 $time,
									 $achiev_current->{'stars'},
									 $achiev_current->{'value'},
									 $achiev_current->{'target'},
									 $achiev_current->{'completionInfo'} )
							or die "Unable to execute stmt_achievToMem_insert.  " . $stmt_achievToMem_insert->errstr;
					if ( $debug ) { print "DEBUG:     Achiev $achiev_current->{'name'} for member $clan_json{$clan_tag}{'memberList'}[$member_no]{'name'} inserted successfully.\n"; }
					$counter{'SQL insert successful: API_Mem_AchievToMem'}++;
			}				
			next;
		}
		
		# iterate threw members past achievements
		my $achiev_new_flag = 0;
		foreach my $achiev_past ( @achiev_sql ) {

			# check if current achiev is in achiev sql
			if ( $achiev_current->{'name'} eq $achiev_past->{'name'} ) { 
				# check if current achiev is different then past achiev
				if ( $achiev_current->{'value'} != $achiev_past->{'value'} && $test == 0 ) {
					# insert member achiev into sql
					$stmt_achievToMem_insert->execute ( $clan_json{$clan_tag}{'memberList'}[$member_no]{'tag'},
							 $achiev_current->{'id'},
							 $achiev_current->{'name'},
							 $time,
							 $achiev_current->{'stars'},
							 $achiev_current->{'value'},
							 $achiev_current->{'target'},
							 $achiev_current->{'completionInfo'} )
						or die "Unable to execute stmt_achievToMem_insert.  " . $stmt_achievToMem_insert->errstr;
					if ( $debug ) { print "DEBUG:     Achiev $achiev_current->{'name'} for member $clan_json{$clan_tag}{'memberList'}[$member_no]{'name'} inserted successfully.\n"; }
					$counter{'SQL insert successful: API_Mem_AchievToMem'}++;
				}
				$achiev_new_flag = 1;
				last;
			}
		}
		if ( $achiev_new_flag == 0 && $test == 0 ) {
			# check if first user
			if ( $achiev_flag == 0 ) {
				# insert achiev into sql
				$stmt_achiev_insert->execute ( $achiev_current->{'name'},
											   $achiev_current->{'info'},
											   $achiev_current->{'base'} )
							or die "Unable to execute stmt_achiev_insert.  " . $stmt_achiev_insert->errstr;
				if ( $debug ) { print "DEBUG:     Achiev $achiev_current->{'name'} inserted successfully.\n"; }
				$counter{'SQL insert successful: API_Mem_Achiev'}++;
				print "WARNING: Achievement Order needs to be updated for $achiev_current->{'name'}\n";
				$counter{'WARNING: Achievement Order needs updated'}++;
			}
			
			# insert member achiev into sql
			$stmt_achievToMem_insert->execute ( $clan_json{$clan_tag}{'memberList'}[$member_no]{'tag'},
							 $achiev_current->{'id'},
							 $achiev_current->{'name'},
							 $time,
							 $achiev_current->{'stars'},
							 $achiev_current->{'value'},
							 $achiev_current->{'target'},
							 $achiev_current->{'completionInfo'} )
						or die "Unable to execute stmt_achievToMem_insert.  " . $stmt_achievToMem_insert->errstr;
			if ( $debug ) { print "DEBUG:     Achiev $achiev_current->{'name'} for member $clan_json{$clan_tag}{'memberList'}[$member_no]{'name'} inserted successfully.\n"; }
			$counter{'SQL insert successful: API_Mem_AchievToMem'}++;
		}
	}
	# set achiev flag so new members don't cause problem
	$achiev_flag = 1;
	
	# release sql statements
	$stmt_achievToMem_search->finish();
	$stmt_achievToMem_insert->finish();
	$stmt_achiev_insert->finish();
}

##############################
# member_troop_sub
##############################
sub member_troop_sub {
	# set up sql statements
	my $stmt_troopsToMem_group_search = $dbh->prepare ( get_troopsToMem_group_search() )
							or die "Unable to prepare stmt_troopsToMem_group_search.  " . $dbh->errstr;
	my $stmt_troopsToMem_insert = $dbh->prepare ( get_troopsToMem_insert() )
							or die "Unable to prepare stmt_troopsToMem_insert.  " . $dbh->errstr;
	my $stmt_troops_insert = $dbh->prepare ( get_troops_insert() )
							or die "Unable to prepare stmt_troops_insert.  " . $dbh->errstr;
	my $stmt_troops_update = $dbh->prepare ( get_troops_update() )
							or die "Unable to prepare stmt_troops_update.  " . $dbh->errstr;

	# Pull current member troops from sql
	if ( $debug ) { print "DEBUG:    Getting troops from sql for member $clan_json{$clan_tag}{'memberList'}[$member_no]{'name'}.\n"; }
	$stmt_troopsToMem_group_search->execute ( $clan_json{$clan_tag}{'memberList'}[$member_no]{'tag'} )
							or die "Unable to execute stmt_troopsToMem_group_search.  " . $stmt_troopsToMem_group_search->errstr;
	
	# get the results
	my $j = 0;
	while ( my $row = $stmt_troopsToMem_group_search->fetchrow_hashref ( ) ) {
		$troops_sql[$j]{'name'} = $row->{'troopsToMem_troops_name'};
		$troops_sql[$j]{'id'} = $row->{'troopsToMem_troop_id'};
		$troops_sql[$j]{'base'} = $row->{'troops_base'};
		$troops_sql[$j]{'order'} = $row->{'troops_order'};
		$troops_sql[$j]{'level'} = $row->{'troopsToMem_level'};
		$troops_sql[$j]{'warWeight'} = $row->{'troopsToMem_warWeight'};
		$j++;
		$counter{'SQL search successful: API_Mem_TroopsToMem'}++;	
	}
	
	# iterate threw members current troops
	foreach my $troop_current ( @{ $member_json->{'troops'} } ) {
		my $troop_max_flag = 0;
		
		# get if home or builders base
		if ( $troop_current->{'village'} eq 'home' ) {
			$troop_current->{'base'} = 1;
		} else {
			$troop_current->{'base'} = 2;
		}
		
		# get troop id
		$troop_current->{'id'} = $troops_id[$troop_current->{'base'}]{$troop_current->{'name'}};
		
		# get war weight
		$troop_current->{'warWeight'} = $warWeight_sql{'troops'}{$troop_current->{'id'}}{'ww'}{$troop_current->{'level'}};
		if ( !defined ( $troop_current->{'warWeight'} )) {
			$troop_current->{'warWeight'} = 0;
		}

		# add troop war weight
		$warWeight_total{$clan_json{$clan_tag}{'memberList'}[$member_no]{'tag'}}{'memWW_troop'} += $troop_current->{'warWeight'};

		# check troops flag, so we only go threw this once
		if ( $troops_flag == 0 ) {
			# iterate threw past troops
			foreach my $troop_past ( @troops_past ) {
				# find matching troop
				if (( $troop_current->{'name'} eq $troop_past->{'name'} )&&
						( $troop_current->{'base'} == $troop_past->{'base'}  )) {
					#See if current troop max level equals past troop max level
					if ( $troop_current->{'maxLevel'} != $troop_past->{'maxLevel'} && $test == 0 ) { 
						$stmt_troops_update->execute ( $troop_current->{'maxLevel'},
														$troop_current->{'name'},
														$troop_current->{'base'} )
							or die "Unable to execute stmt_troops_update.  " . $stmt_troops_update->errstr;
						if ( $debug ) { print "DEBUG:     Troops $troop_current->{'name'} update successfully.\n"; }
						$counter{'SQL update successful: API_Mem_Troops'}++;
					}
					$troop_max_flag = 1;
					last;
				}
			}
			# check if current troop was found in sql
			if ( $troop_max_flag == 0 && $test == 0  ) {
				# insert troop into sql, new toop
				$stmt_troops_insert->execute ( $troop_current->{'name'},
													$troop_current->{'base'},
												   	$troop_current->{'maxLevel'} )
							or die "Unable to execute stmt_troops_insert.  " . $stmt_troops_insert->errstr;
				if ( $debug ) { print "DEBUG:     Troop $troop_current->{'name'} inserted successfully.\n"; }
				$counter{'SQL insert successful: API_Mem_Troops'}++;
				print "WARNING: Troop Order needs to be updated for $troop_current->{'name'}\n";
				$counter{'WARNING: Troop Order needs updated'}++;
			}
		}
		
		# check for past member troops, new member
		if ( !@troops_sql ) {
			if ( $test == 0 ) {
				# insert member troop into sql
				$stmt_troopsToMem_insert->execute ( $clan_json{$clan_tag}{'memberList'}[$member_no]{'tag'},
								$troop_current->{'id'},
							 	$troop_current->{'name'},
							 	$time,
							 	$troop_current->{'level'},
							 	$troop_current->{'warWeight'} ) 
						or die "Unable to execute stmt_troopsToMem_insert.  " . $stmt_troopsToMem_insert->errstr;
				if ( $debug ) { print "DEBUG:     Troop $troop_current->{'name'} for member $clan_json{$clan_tag}{'memberList'}[$member_no]{'name'} inserted successfully.\n"; }
				$counter{'SQL insert successful: API_Mem_TroopsToMem'}++;
			}				
			next;
		}

		# iterate threw members past troops
		my $troop_flag = 0;
		foreach my $troop_past ( @troops_sql ) {
			# check if current troop is in troop sql
			if (( $troop_current->{'name'} eq $troop_past->{'name'} )&&
					( $troop_current->{'base'} == $troop_past->{'base'}  )) {
				
				# check if current troop is different then past troop, troops changed
				if ( $troop_current->{'level'} != $troop_past->{'level'} && $test == 0 ) {
					# insert member troop into sql
					$stmt_troopsToMem_insert->execute ( $clan_json{$clan_tag}{'memberList'}[$member_no]{'tag'},
									$troop_current->{'id'},
									$troop_current->{'name'},
									$time,
									$troop_current->{'level'},
									$troop_current->{'warWeight'} ) 
							or die "Unable to execute stmt_troopsToMem_insert.  " . $stmt_troopsToMem_insert->errstr;
					if ( $debug ) { print "DEBUG:     Troop $troop_current->{'name'} for member $clan_json{$clan_tag}{'memberList'}[$member_no]{'name'} inserted successfully.\n"; }
					$counter{'SQL insert successful: API_Mem_TroopsToMem'}++;
				}
				$troop_flag = 1;
				last;
			}
		}

		if ( $troop_flag == 0 && $test == 0 ) {
			# insert member troop into sql, no current troop (DE barracks)
			$stmt_troopsToMem_insert->execute ( $clan_json{$clan_tag}{'memberList'}[$member_no]{'tag'},
							$troop_current->{'id'},
							$troop_current->{'name'},
							$time,
							$troop_current->{'level'},
							$troop_current->{'warWeight'} ) 
						or die "Unable to execute stmt_troopsToMem_insert.  " . $stmt_troopsToMem_insert->errstr;
			if ( $debug ) { print "DEBUG:     Troop $troop_current->{'name'} for member $clan_json{$clan_tag}{'memberList'}[$member_no]{'name'} inserted successfully.\n"; }
			$counter{'SQL insert successful: API_Mem_TroopsToMem'}++;
		}
	}
	# set troops flag so troops do not get updated again
	$troops_flag = 1;

	# add troop war weight to total war weight
	$warWeight_total{$clan_json{$clan_tag}{'memberList'}[$member_no]{'tag'}}{'memWW_total'} += 
			$warWeight_total{$clan_json{$clan_tag}{'memberList'}[$member_no]{'tag'}}{'memWW_troop'};
	
	# release sql statements
	$stmt_troopsToMem_group_search->finish();
	$stmt_troopsToMem_insert->finish();
	$stmt_troops_insert->finish();
	$stmt_troops_update->finish();	
}

##############################
# member_hero_sub
##############################
sub member_hero_sub {

	# set up sql statements
	my $stmt_heroToMem_group_search = $dbh->prepare ( get_heroToMem_group_search() )
							or die "Unable to prepare stmt_heroToMem_group_search.  " . $dbh->errstr;
	my $stmt_heroToMem_insert = $dbh->prepare ( get_heroToMem_insert() )
							or die "Unable to prepare stmt_heroToMem_insert.  " . $dbh->errstr;
	my $stmt_hero_insert = $dbh->prepare ( get_hero_insert() )
							or die "Unable to prepare stmt_hero_insert.  " . $dbh->errstr;
	my $stmt_hero_update = $dbh->prepare ( get_hero_update() )
							or die "Unable to prepare stmt_hero_update.  " . $dbh->errstr;

	# Pull current member heros from sql
	if ( $debug ) { print "DEBUG:    Getting heros from sql for member $clan_json{$clan_tag}{'memberList'}[$member_no]{'name'}.\n"; }
	$stmt_heroToMem_group_search->execute ( $clan_json{$clan_tag}{'memberList'}[$member_no]{'tag'} )
							or die "Unable to execute stmt_heroToMem_group_search.  " . $stmt_heroToMem_group_search->errstr;

	# get the results
	my $j = 0;
	while ( my $row = $stmt_heroToMem_group_search->fetchrow_hashref ( ) ) {
		$heros_sql[$j]{'name'} = $row->{'heroToMem_heros_name'};
		$heros_sql[$j]{'id'} = $row->{'heroToMem_heros_id'};
		$heros_sql[$j]{'base'} = $row->{'hero_base'};
		$heros_sql[$j]{'order'} = $row->{'hero_order'};
		$heros_sql[$j]{'level'} = $row->{'heroToMem_level'};
		$heros_sql[$j]{'warWeight_off'} = $row->{'heroToMem_warWeight_off'};
		$heros_sql[$j]{'warWeight_def'} = $row->{'heroToMem_warWeight_def'};
		$j++;
		$counter{'SQL search successful: API_Mem_HerosToMem'}++;
	}

	# iterate threw members current heros
	foreach my $hero_current ( @{ $member_json->{'heroes'} } ) {
		my $hero_max_flag = 0;
		
		# set home or builders base
		if ( $hero_current->{'village'} eq 'home' ) {
			$hero_current->{'base'} = 1;
		} else {
			$hero_current->{'base'} = 2;
		}
		
		# set hero id
		$hero_current->{'id'} = $heros_id[$hero_current->{'base'}]{$hero_current->{'name'}};
		
		# set war weight
		$hero_current->{'warWeight_off'} = $warWeight_sql{'heros'}{$hero_current->{'id'}}{'ww_off'}{$hero_current->{'level'}};
		$hero_current->{'warWeight_def'} = $warWeight_sql{'heros'}{$hero_current->{'id'}}{'ww_def'}{$hero_current->{'level'}};
		if ( $hero_current->{'base'} == 2 ) {
			$hero_current->{'warWeight_def'} = 0;
			$hero_current->{'warWeight_off'} = 0;
		}

		# add hero war weight
		$warWeight_total{$clan_json{$clan_tag}{'memberList'}[$member_no]{'tag'}}{'memWW_hero_off'} += $hero_current->{'warWeight_off'};
		$warWeight_total{$clan_json{$clan_tag}{'memberList'}[$member_no]{'tag'}}{'memWW_hero_def'} += $hero_current->{'warWeight_def'};

		# check heros flag, so we only go threw this once
		if ( $heros_flag == 0 ) {
			# iterate threw past heros
			foreach my $hero_past ( @heros_past ) {
				# find matching hero
				if ( $hero_current->{'name'} eq $hero_past->{'name'} ) {
					#See if current hero max level equals past hero max level
					if ( $hero_current->{'maxLevel'} != $hero_past->{'maxLevel'} && $test == 0 ) { 
						$stmt_hero_update->execute ( $hero_current->{'maxLevel'},
														$hero_current->{'name'} )
							or die "Unable to execute stmt_hero_update.  " . $stmt_hero_update->errstr;
						if ( $debug ) { print "DEBUG:     Heros $hero_current->{'name'} update successfully.\n"; }
						$counter{'SQL update sucessful: API_Mem_Heros'}++;
					}
					$hero_max_flag = 1;
					last;
				}
			}
			# check if current hero was found in sql
			if ( $hero_max_flag == 0 && $test == 0  ) {
				# insert hero into sql, new toop
				$stmt_hero_insert->execute ( $hero_current->{'name'},
												   $hero_current->{'maxLevel'} )
							or die "Unable to execute stmt_hero_insert.  " . $stmt_hero_insert->errstr;
				if ( $debug ) { print "DEBUG:     Hero $hero_current->{'name'} inserted successfully.\n"; }
				$counter{'SQL insert sucessful: API_Mem_Heros'}++;
			}
		}
		
		# check for past member heros, new member
		if ( !@heros_sql ) {
			if ( $test == 0 ) {
				# insert member hero into sql
				$stmt_heroToMem_insert->execute ( $clan_json{$clan_tag}{'memberList'}[$member_no]{'tag'},
							 $hero_current->{'id'},
							 $hero_current->{'name'},
							 $time,
							 $hero_current->{'level'},
							 $hero_current->{'warWeight_off'}, 
							 $hero_current->{'warWeight_def'} ) 
						or die "Unable to execute stmt_heroToMem_insert.  " . $stmt_heroToMem_insert->errstr;
				if ( $debug ) { print "DEBUG:     Hero $hero_current->{'name'} for member $clan_json{$clan_tag}{'memberList'}[$member_no]{'name'} inserted successfully.\n"; }
				$counter{'SQL insert sucessful: API_Mem_HerosToMem'}++;
			}				
			next;
		}

		# iterate threw members past heros
		my $hero_flag = 0;
		foreach my $hero_past ( @heros_sql ) {
			# check if current hero is in hero sql
			if ( $hero_current->{'name'} eq $hero_past->{'name'} ) { 
				# check if current hero is different then past hero, heros changed
				if ( $hero_current->{'level'} != $hero_past->{'level'} && $test == 0 ) {
					# insert member hero into sql
					$stmt_heroToMem_insert->execute ( $clan_json{$clan_tag}{'memberList'}[$member_no]{'tag'},
								$hero_current->{'id'},
								$hero_current->{'name'},
								$time,
								$hero_current->{'level'},
								$hero_current->{'warWeight_off'}, 
								$hero_current->{'warWeight_def'} ) 
							or die "Unable to execute stmt_heroToMem_insert.  " . $stmt_heroToMem_insert->errstr;
					if ( $debug ) { print "DEBUG:     Hero $hero_current->{'name'} for member $clan_json{$clan_tag}{'memberList'}[$member_no]{'name'} inserted successfully.\n"; }
					$counter{'SQL insert sucessful: API_Mem_HerosToMem'}++;
				}
				$hero_flag = 1;
				last;
			}
		}
		if ( $hero_flag == 0 && $test == 0 ) {
			# insert member hero into sql, no current hero (DE barracks)
			$stmt_heroToMem_insert->execute ( $clan_json{$clan_tag}{'memberList'}[$member_no]{'tag'},
							 $hero_current->{'id'},
							 $hero_current->{'name'},
							 $time,
							 $hero_current->{'level'},
							 $hero_current->{'warWeight_off'}, 
							 $hero_current->{'warWeight_def'} ) 
						or die "Unable to execute stmt_heroToMem_insert.  " . $stmt_heroToMem_insert->errstr;
			if ( $debug ) { print "DEBUG:     Hero $hero_current->{'name'} for member $clan_json{$clan_tag}{'memberList'}[$member_no]{'name'} inserted successfully.\n"; }
			$counter{'SQL insert sucessful: API_Mem_HerosToMem'}++;
		}
		# add hero war weight to total war weight
		if ( defined ( $warWeight_total{$clan_json{$clan_tag}{'memberList'}[$member_no]{'tag'}}{'memWW_hero'} )) {
			$warWeight_total{$clan_json{$clan_tag}{'memberList'}[$member_no]{'tag'}}{'memWW_total'} += 
					$hero_current->{'warWeight_def'} + $hero_current->{'warWeight_off'};
		} else {
			$warWeight_total{$clan_json{$clan_tag}{'memberList'}[$member_no]{'tag'}}{'memWW_hero'} = 0;
		}
	}
	# set heros flag so heros do not get updated again
	$heros_flag = 1;
	

	# release sql statements
	$stmt_heroToMem_group_search->finish();
	$stmt_heroToMem_insert->finish();
	$stmt_hero_insert->finish();
	$stmt_hero_update->finish();	
}

##############################
# member_spell_sub
##############################
sub member_spell_sub {

 	# set up sql statements
	my $stmt_spellToMem_group_search = $dbh->prepare ( get_spellToMem_group_search() )
							or die "Unable to prepare stmt_spellToMem_group_search.  " . $dbh->errstr;
	my $stmt_spellToMem_insert = $dbh->prepare ( get_spellToMem_insert() )
							or die "Unable to prepare stmt_spellToMem_insert.  " . $dbh->errstr;
	my $stmt_spell_insert = $dbh->prepare ( get_spell_insert() )
							or die "Unable to prepare stmt_spell_insert.  " . $dbh->errstr;
	my $stmt_spell_update = $dbh->prepare ( get_spell_update() )
							or die "Unable to prepare stmt_spell_update.  " . $dbh->errstr;

	# Pull current member spells from sql
	if ( $debug ) { print "DEBUG:    Getting spells from sql for member $clan_json{$clan_tag}{'memberList'}[$member_no]{'name'}.\n"; }
	$stmt_spellToMem_group_search->execute ( $clan_json{$clan_tag}{'memberList'}[$member_no]{'tag'} )
							or die "Unable to execute stmt_spellToMem_group_search.  " . $stmt_spellToMem_group_search->errstr;

	# get the results
	my $j = 0;
	while ( my $row = $stmt_spellToMem_group_search->fetchrow_hashref ( ) ) {
		$spells_sql[$j]{'name'} = $row->{'spellsToMem_spells_name'};
		$spells_sql[$j]{'level'} = $row->{'spellsToMem_level'};
		$j++;
		$counter{'SQL search successful: API_Mem_SpellsToMem'}++;
	}

	# iterate threw members current spells
	foreach my $spell_current ( @{ $member_json->{'spells'} } ) {
		my $spell_max_flag = 0;

		# set spell id
		$spell_current->{'id'} = $spells_id[1]{$spell_current->{'name'}};
		
		# set war weight
		$spell_current->{'warWeight'} = $warWeight_sql{'spells'}{$spell_current->{'id'}}{'ww'}{$spell_current->{'level'}};
		if ( !defined ( $spell_current->{'warWeight'} )) {
			$spell_current->{'warWeight'} = 0;
		}

		# add spells war weight
		$warWeight_total{$clan_json{$clan_tag}{'memberList'}[$member_no]{'tag'}}{'memWW_spell'} += 
				$spell_current->{'warWeight'};


		#check spells flag, so we only go threw this once
		if ( $spells_flag == 0 ) {
			# iterate threw past spells
			foreach my $spell_past ( @spells_past ) {
				#find matching spell
				if ( $spell_current->{'name'} eq $spell_past->{'name'} ) {
					# See if current spell max level equals past spell max level
					if ( $spell_current->{'maxLevel'} != $spell_past->{'maxLevel'} && $test == 0 ) { 
						$stmt_spell_update->execute ( $spell_current->{'maxLevel'},
														$spell_current->{'name'} )
							or die "Unable to execute stmt_spell_update.  " . $stmt_spell_update->errstr;
						if ( $debug ) { print "DEBUG:     Spells $spell_current->{'name'} update successfully.\n"; }
						$counter{'SQL update successful: API_Mem_Spells'}++;
					}
					$spell_max_flag = 1;
					last;
				}
			}
			# check if current spell was found in sql
			if ( $spell_max_flag == 0 && $test == 0  ) {
				# insert spell into sql, new toop
				$stmt_spell_insert->execute ( $spell_current->{'name'},
												   $spell_current->{'maxLevel'} )
							or die "Unable to execute stmt_spell_insert.  " . $stmt_spell_insert->errstr;
				if ( $debug ) { print "DEBUG:     Spell $spell_current->{'name'} inserted successfully.\n"; }
				$counter{'SQL insert successful: API_Mem_Spells'}++;
			}
		}
		
		# check for past member spells, new member
		if ( !@spells_sql ) {
			if ( $test == 0 ) {
				# insert member spell into sql
				$stmt_spellToMem_insert->execute ( $clan_json{$clan_tag}{'memberList'}[$member_no]{'tag'},
							 $spell_current->{'id'},
							 $spell_current->{'name'},
							 $time,
							 $spell_current->{'level'},
							 $spell_current->{'warWeight'} ) 
						or die "Unable to execute stmt_spellToMem_insert.  " . $stmt_spellToMem_insert->errstr;
				if ( $debug ) { print "DEBUG:     Spell $spell_current->{'name'} for member $clan_json{$clan_tag}{'memberList'}[$member_no]{'name'} inserted successfully.\n"; }
				$counter{'SQL insert successful: API_Mem_SpellsToMem'}++;
			}				
			next;
		}

		# iterate threw members past spells
		my $spell_flag = 0;
		foreach my $spell_past ( @spells_sql ) {
			# check if current spell is in spell sql
			if ( $spell_current->{'name'} eq $spell_past->{'name'} ) { 
				# check if current spell is different then past spell, spells changed
				if ( $spell_current->{'level'} != $spell_past->{'level'} && $test == 0 ) {
					# insert member spell into sql
					$stmt_spellToMem_insert->execute ( $clan_json{$clan_tag}{'memberList'}[$member_no]{'tag'},
								$spell_current->{'id'},
								$spell_current->{'name'},
								$time,
								$spell_current->{'level'},
								$spell_current->{'warWeight'} ) 
							or die "Unable to execute stmt_spellToMem_insert.  " . $stmt_spellToMem_insert->errstr;
					if ( $debug ) { print "DEBUG:     Spell $spell_current->{'name'} for member $clan_json{$clan_tag}{'memberList'}[$member_no]{'name'} inserted successfully.\n"; }
					$counter{'SQL insert successful: API_Mem_SpellsToMem'}++;
				}
				$spell_flag = 1;
				last;
			}
		}
		if ( $spell_flag == 0 && $test == 0 ) {
			# insert member spell into sql, no current spell (Spell Factory Upgrade)
			$stmt_spellToMem_insert->execute ( $clan_json{$clan_tag}{'memberList'}[$member_no]{'tag'},
							$spell_current->{'id'},
							$spell_current->{'name'},
							$time,
							$spell_current->{'level'},
							$spell_current->{'warWeight'} ) 							
						or die "Unable to execute stmt_spellToMem_insert.  " . $stmt_spellToMem_insert->errstr;
			if ( $debug ) { print "DEBUG:     Spell $spell_current->{'name'} for member $clan_json{$clan_tag}{'memberList'}[$member_no]{'name'} inserted successfully.\n"; }
			$counter{'SQL insert successful: API_Mem_SpellsToMem'}++;
		}

		# set spells flag so spells do not get updated again
		$spells_flag = 1;
		
		# add spell war weight to total war weight
		if ( defined ( $warWeight_total{$clan_json{$clan_tag}{'memberList'}[$member_no]{'tag'}}{'memWW_spell'} )) {
			$warWeight_total{$clan_json{$clan_tag}{'memberList'}[$member_no]{'tag'}}{'memWW_total'} += 
					$warWeight_total{$clan_json{$clan_tag}{'memberList'}[$member_no]{'tag'}}{'memWW_spell'};
		} else {
			$warWeight_total{$clan_json{$clan_tag}{'memberList'}[$member_no]{'tag'}}{'memWW_spell'} = 0;
		}
	}

	# release sql statements
	$stmt_spellToMem_group_search->finish();
	$stmt_spellToMem_insert->finish();
	$stmt_spell_insert->finish();
	$stmt_spell_update->finish();	
}
