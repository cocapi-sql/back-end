#! /usr/bin/perl

use strict;
use warnings;
use Data::Dumper;
use Time::Piece;
use JSON::Parse 'parse_json';
use DBI;

# include files
use lib '/home/thebl962/public_html/bin';
require 'API_SQL_Statements.pl';
require 'config.pl';

# SQL Variables
my $sql_platform = get_sql_platform ();
my $dbname =  get_dbname ();
my $servername = get_servername ();
my $username = get_username();
my $password = get_password();
my $dsn = "DBI:" . get_sql_platform() . ":" . get_dbname() . ":" . get_servername();

# Set up api token and url
my $api_clan_token = get_api_clan_token();
my $api_clantag_url = "https://api.clashofclans.com/v1/clans/";
my $api_member_token = get_api_member_token();
my $api_membertag_url = "https://api.clashofclans.com/v1/players/";

# load clan tags into array, Note leave the # off
my @clan_tags = get_clan_list();

# set up headers
my $json_header = "Accept: application/json";
my $api_clan_header = "authorization: Bearer " . $api_clan_token;
my $api_member_header = "authorization: Bearer " . $api_member_token;

# set up global variables
my %clan_json = ();
my %warlog_json = ();
my %counter = ();
my $member_json = ();
my $member_no;
our $clan_tag;
my @achiev_sql = ();
my @troops_sql = ();
my @troops_past = ();
my $troops_flag = 0;
my @heros_sql = ();
my @heros_past = ();
my $heros_flag = 0;
my @spells_sql = ();
my @spells_past = ();
my $spells_flag = 0;

# get debug and test from config
my $debug = get_debug();
my $test = get_test();

# check for cli arguments
while ( @ARGV ) {
	if ( my $Parameter eq '-debug' ) {
		$debug = 1;
	} elsif ( $Parameter eq '-test' ) {
		$test = 1;
	}
} 

# display debug and test flags
if ( $debug == 1 ) { print "DEBUG: Debug (Verbose Logging) is ON\n"; }
if ( $test == 1 ) { print "DEBUG: Test (SQL Insert) is ON\n"; }

# connect to MySQL database
my %attr = ( PrintError=>0,RaiseError=>1,PrintError=>0,ShowErrorStatement=>1 );
my $dbh = DBI->connect ( $dsn, $username, $password, \%attr );

# Get Greenwich time zone and convert to sql date-time stamp
my ($S, $M, $H, $d, $m, $Y) = gmtime(time);
$m += 1;
$Y += 1900;
my $time = sprintf("%04d-%02d-%02d %02d:%02d:%02d", $Y, $m, $d, $H, $M, $S);

# loop threw clan tag array
for $clan_tag ( @clan_tags ) {  
	clan_pull_sub();
}

# iterate threw clans
foreach $clan_tag ( @clan_tags ){
	if ( $debug ) { print "DEBUG: Start working on $clan_json{$clan_tag}{'name'} clan's data.\n"; }

	# enter clan data into sql
	clan_entry_sub();
	
	# check if war log is public
	if ( $clan_json{$clan_tag}{'isWarLogPublic'} == 1 ) {
		clan_warlog_sub();
	}

	# pull past member data from sql
	member_past_sub();

	# iterate threw members of clans
	for ( $member_no = 0; $member_no < $clan_json{$clan_tag}{'members'}; $member_no++ ) {
		if ( $debug ) { print "DEBUG:   Start working on $clan_json{$clan_tag}{'memberList'}[$member_no]{'name'} member's data.\n"; }
		
		# get the member general data into sql
		member_general_sub();
		
		# get the member achievement data into sql
		member_achiev_sub();
		
		# get the member troop data into sql
		member_troop_sub();
		
		# get the member hero data into sql
		member_hero_sub();
		
		# get the member spell data into sql
		member_spell_sub();

		if ( $debug ) { print "DEBUG:   Finished working on $clan_json{$clan_tag}{'memberList'}[$member_no]{'name'} member's data.\n"; }
	}
	
	if ( $debug ) { print "DEBUG: Finished working on $clan_json{$clan_tag}{'name'} clan's data.\n"; }
}

# disconnect from the MySQL database
$dbh->disconnect();

# display counter
print "\n";
print "Summary\n";
print "-" x 50 . " " . "-" x 5 . "\n";
foreach my $key ( sort keys %counter ) {
	printf( "%-50s %5d\n", $key, $counter{$key} );
}



#print Dumper( $clan_json{QLP22JYG}{'warlog'}{'items'}[0] );

sub clan_pull_sub {
	if ( $debug ) { print "DEBUG: Getting json for clan $clan_tag.\n"; }
	
	# set up url
	my $clan_url = $api_clantag_url . '%23' . $clan_tag;
	my $warlog_url = $api_clantag_url . '%23' . $clan_tag . "/warlog";
	# get the clan in json
	my $http_response = qx{curl -si --header \'$json_header\' --header \"$api_clan_header\" $clan_url};
	my ( $http_head, $http_body ) = split ( m{\r?\n\r?\n}, $http_response );
	
	# check if we got error
	if ( $http_head !~ m#HTTP/1.1 200 OK# ) {
		print "ERROR: Did not get back clan web page. Check response code.\n";
		print "================\n";
		print "= HEADER START =\n";
		print "================\n\n";
		print "$http_head\n\n";
		print "================\n";
		print "== HEADER END ==\n";
		print "================\n";
		exit ();
	}
		
	# parse json to hash
	$clan_json{$clan_tag} = parse_json ( $http_body );
	
	# check if we parsed reply correctly
	if ( !$clan_json{$clan_tag} ) {
		print "ERROR: Unable to clan parse from API data.";
		exit ();
	}

	if ( $debug ) { print "DEBUG: Successful got clan data for $clan_tag.\n"; }
	$counter{'Clan data pulled from API'}++;
	
	# check if war log is public
	if ( $clan_json{$clan_tag}{'isWarLogPublic'} == 1 ) {
		# get the war log in json
		my $http_response = qx{curl -si --header \'$json_header\' --header \"$api_clan_header\" $warlog_url};
		my ( $http_head, $http_body ) = split ( m{\r?\n\r?\n}, $http_response );
	
		# check if we got error
		if ( $http_head !~ m#HTTP/1.1 200 OK# ) {
			print "ERROR: Did not get back clan web page. Check response code.\n";
			print "================\n";
			print "= HEADER START =\n";
			print "================\n\n";
			print "$http_head\n\n";
			print "================\n";
			print "== HEADER END ==\n";
			print "================\n";
			exit ();
		}
		
		# parse json to hash
		$clan_json{$clan_tag}{'warlog'} = parse_json ( $http_body );
	
		# check if we parsed reply correctly
		if ( !$clan_json{$clan_tag}{'warlog'} ) {
			print "ERROR: Unable to warlog parse from API data.";
			exit ();
		}

		if ( $debug ) { print "DEBUG: Successful got warlog for $clan_tag.\n"; }
		$counter{'Clan warlog pulled from API'}++;
	}
}

sub clan_entry_sub {
	# set up sql statements
	my $stmt_clan_warLosses_search = $dbh->prepare ( get_clan_warLosses_search() )
							or die "Unable to prepare stmt_clan_warLosses_search.  " . $dbh->errstr;
	my $stmt_clan_search = $dbh->prepare ( get_clan_search() )
							or die "Unable to prepare stmt_clan_search.  " . $dbh->errstr;
	my $stmt_clan_update = $dbh->prepare ( get_clan_update() )
							or die "Unable to prepare stmt_clan_update.  " . $dbh->errstr;
	my $stmt_clan_insert = $dbh->prepare ( get_clan_insert() )
							or die "Unable to prepare stmt_clan_insert.  " . $dbh->errstr;
	my $stmt_clan_info_insert = $dbh->prepare ( get_clan_info_insert() )
							or die "Unable to prepare stmt_clan_info_insert.  " . $dbh->errstr;
	
	# check if war log is private
	if ( $clan_json{$clan_tag}{'isWarLogPublic'} != 1 ) {
		if ( $debug ) { print "DEBUG:   War Log is private.\n"; }
		
		# set war flag to false
		delete $clan_json{$clan_tag}{'isWarLogPublic'};
		$clan_json{$clan_tag}{'isWarLogPublic'} = 0;
		
		# get most recent warTies and warLosses from db
		if ( $test == 0 ) {
			$stmt_clan_warLosses_search->execute( '#' . $clan_tag )
	 					or die "Unable to execute stmt_clan_warLosses_search.  " . $stmt_clan_warLosses_search->errstr;
			while (my @row = $stmt_clan_warLosses_search->fetchrow_array) { 
				# update warLosses and warTies
				$clan_json{$clan_tag}{'warLosses'} = pop @row;
				$clan_json{$clan_tag}{'warTies'} = pop @row;
				if ( $debug ) { print "DEBUG:   Updated war ties and losses from last entry.\n"; }
			
				# get time of last event and see if it is over a week old
				my $time_update = pop @row;
				my $time_formate = '%Y-%m-%d %H:%M:%S';
				my $time_diff = Time::Piece->strptime ( $time, $time_formate )
						 - Time::Piece->strptime ( $time_update, $time_formate );
				if ( $time_diff > 604800 ) {
					print "WARNING: The clans war log has not been updated in over a week\n";
				}
			}
		}
		
		# Check if war losses are empty, set to 0
		if ( ! $clan_json{$clan_tag}{'warLosses'} ) {
			$clan_json{$clan_tag}{'warLosses'} = 0;
			if ( $debug ) { print "DEBUG:   War losses set to 0.\n"; }
		}
		
		# Check if war ties are empty, set to 0
		if ( ! $clan_json{$clan_tag}{'warTies'} ) {
			$clan_json{$clan_tag}{'warTies'} = 0;
			if ( $debug ) { print "DEBUG:   War ties set to 0.\n"; }
		}
		$counter{'Clan war log is private'}++;
	} 
	
	# check if clan is in clan table
	$stmt_clan_search->execute ( '#' . $clan_tag )
	 		or die "Unable to execute stmt_clan_search.  " . $stmt_clan_search->errstr;
	if ( $debug ) { print "DEBUG:   Clan $clan_json{$clan_tag}{'name'} select successfully.\n"; }
		
	# check if there was results
	my $clan_count = 0;
	while ( my $row = $stmt_clan_search->fetchrow_arrayref ( ) ) {
		# Update clan table
		$stmt_clan_update->execute ( $clan_json{$clan_tag}{'description'},
									 $clan_json{$clan_tag}{'badgeUrls'}{'small'},
									 $clan_json{$clan_tag}{'badgeUrls'}{'medium'},
									 $clan_json{$clan_tag}{'badgeUrls'}{'large'},
									 $clan_json{$clan_tag}{'location'}{'id'},
									 $clan_json{$clan_tag}{'location'}{'name'},
									 $clan_json{$clan_tag}{'location'}{'countryCode'},
									 $clan_json{$clan_tag}{'warFrequency'},
									 '#' . $clan_tag )
				or die "Unable to execute stmt_clan_update.  " . $stmt_clan_update->errstr;
		if ( $debug ) { print "DEBUG:   Clan $clan_json{$clan_tag}{'name'} update successfully.\n"; }
		$counter{'Clan data updated'}++;
		$clan_count++;
	}

	if ( $clan_count == 0 && $test == 0 ) {
		# Insert clan table
		$stmt_clan_insert->execute ( '#' . $clan_tag,
					$clan_json{$clan_tag}{'name'},
					$clan_json{$clan_tag}{'description'},
					$clan_json{$clan_tag}{'badgeUrls'}{'small'},
					$clan_json{$clan_tag}{'badgeUrls'}{'medium'},
					$clan_json{$clan_tag}{'badgeUrls'}{'large'},
					$clan_json{$clan_tag}{'location'}{'id'},
					$clan_json{$clan_tag}{'location'}{'name'},
					$clan_json{$clan_tag}{'location'}{'countryCode'},
					$clan_json{$clan_tag}{'warFrequency'} )
	 		or die "Unable to execute stmt_clan_insert.  " . $stmt_clan_insert->errstr;
		if ( $debug ) { print "DEBUG:   Clan $clan_json{$clan_tag}{'name'} inserted successfully.\n"; }
		$counter{'Clan data inserted'}++;
	}

	# insert clan info into clan_info table
	if ( $test == 0 ) {
		$stmt_clan_info_insert->execute ( $time,
					'#' . $clan_tag,
					$clan_json{$clan_tag}{'clanLevel'},
					$clan_json{$clan_tag}{'clanPoints'},
					$clan_json{$clan_tag}{'warWins'},
					$clan_json{$clan_tag}{'warWinStreak'},
					$clan_json{$clan_tag}{'warTies'},
					$clan_json{$clan_tag}{'warLosses'},
					$clan_json{$clan_tag}{'members'},
					$clan_json{$clan_tag}{'type'},
					$clan_json{$clan_tag}{'requiredTrophies'},
					$clan_json{$clan_tag}{'isWarLogPublic'} )
	 			or die "Unable to execute stmt_clan_info_insert.  " . $stmt_clan_info_insert->errstr;
		if ( $debug ) { print "DEBUG:   Clan info for $clan_json{$clan_tag}{'name'} inserted successfully.\n"; }
		$counter{'Clan info data inserted'}++;
	}
	# clean up sql statements
	$stmt_clan_warLosses_search->finish();
	$stmt_clan_search->finish();
	$stmt_clan_insert->finish();
	$stmt_clan_update->finish();
	$stmt_clan_info_insert->finish();

}

sub clan_warlog_sub {
	if ( $debug ) { print "DEBUG:   War Log is public.\n"; }
	
	# setup sql statements
	my $stmt_warlog_search = $dbh->prepare ( get_warlog_search() )
							or die "Unable to prepare stmt_warlog_search.  " . $dbh->errstr;
	my $stmt_warlog_insert = $dbh->prepare ( get_warlog_insert() )
							or die "Unable to prepare stmt_warlog_insert.  " . $dbh->errstr;
	
	# get newest war from sql
	$stmt_warlog_search->execute ( '#' . $clan_tag )	
		or die "Unable to execute stmt_warlog_search.  " . $stmt_warlog_search->errstr;
	if ( $debug ) { print "DEBUG:   Warlog for $clan_json{$clan_tag}{'name'} select successfully.\n"; }
	
	# check if there was results
	my $temp_opp_tag;
	while ( my $row = $stmt_warlog_search->fetchrow_hashref ( ) ) {
		$temp_opp_tag = $row->{'warlog_opp_tag'};
	}

	# iterate threw warlog
	for ( my $i = 0; $i < @{$clan_json{$clan_tag}{'warlog'}{'items'}}; $i++ ) {
		# check if this is different then the newest war
		my $test_opp_tag = $clan_json{$clan_tag}{'warlog'}{'items'}[$i]{'opponent'}{'tag'};
		
		if ( $temp_opp_tag !~ m/$test_opp_tag/ ) {

			# convert date
			if ( $clan_json{$clan_tag}{'warlog'}{'items'}[$i]{'endTime'} =~ 
					m/(\d\d\d\d)(\d\d)(\d\d)T(\d\d)(\d\d)(\d\d)/ ) {
				my $warlog_time = $1 . '-' . $2 . '-' . $3 . ' ' . $4 . ':' . $5 . ':' . $6;
				
				# add warlog
				if ( $test == 0 ) {
					$stmt_warlog_insert->execute ( $clan_json{$clan_tag}{'warlog'}{'items'}[$i]{'result'},
								$warlog_time,
								$clan_json{$clan_tag}{'warlog'}{'items'}[$i]{'teamSize'},
								$clan_json{$clan_tag}{'warlog'}{'items'}[$i]{'clan'}{'tag'},
								$clan_json{$clan_tag}{'warlog'}{'items'}[$i]{'clan'}{'clanLevel'},
								$clan_json{$clan_tag}{'warlog'}{'items'}[$i]{'clan'}{'stars'},
								$clan_json{$clan_tag}{'warlog'}{'items'}[$i]{'clan'}{'destructionPercentage'},
								$clan_json{$clan_tag}{'warlog'}{'items'}[$i]{'clan'}{'attacks'},
								$clan_json{$clan_tag}{'warlog'}{'items'}[$i]{'clan'}{'expEarned'},
								$clan_json{$clan_tag}{'warlog'}{'items'}[$i]{'opponent'}{'tag'},
								$clan_json{$clan_tag}{'warlog'}{'items'}[$i]{'opponent'}{'name'},
								$clan_json{$clan_tag}{'warlog'}{'items'}[$i]{'opponent'}{'clanLevel'},
								$clan_json{$clan_tag}{'warlog'}{'items'}[$i]{'opponent'}{'stars'},
								$clan_json{$clan_tag}{'warlog'}{'items'}[$i]{'opponent'}{'destructionPercentage'},
								$clan_json{$clan_tag}{'warlog'}{'items'}[$i]{'opponent'}{'badgeUrls'}{'small'},
								$clan_json{$clan_tag}{'warlog'}{'items'}[$i]{'opponent'}{'badgeUrls'}{'medium'},
								$clan_json{$clan_tag}{'warlog'}{'items'}[$i]{'opponent'}{'badgeUrls'}{'large'} )
							or die "Unable to execute stmt_warlog_insert.  " . $stmt_warlog_insert->errstr;
					if ( $debug ) { print "DEBUG:   WarLog for $clan_json{$clan_tag}{'name'} inserted successfully.\n"; }
					$counter{'War Log data inserted'}++;
				}
			} else {
				print "ERROR: Unable to match warlog time\n";
			}
		} else {
			#skip the rest of the warlog
			last;
		}
	}
	$counter{'Clan war log is public'}++;
	
	# clean up sql
	$stmt_warlog_search->finish();
	$stmt_warlog_insert->finish();
}

sub member_past_sub {
	# set up sql statements
	my $stmt_troops_search = $dbh->prepare ( get_troops_search() )
							or die "Unable to prepare stmt_troops_search.  " . $dbh->errstr;
	my $stmt_hero_search = $dbh->prepare ( get_hero_search() )
							or die "Unable to prepare stmt_hero_search.  " . $dbh->errstr;
	my $stmt_spell_search = $dbh->prepare ( get_spell_search() )
							or die "Unable to prepare stmt_spell_search.  " . $dbh->errstr;
	
	# Pull past troops from sql
	if ( $debug ) { print "DEBUG:   Getting all troops from sql.\n"; }
	$stmt_troops_search->execute ()
	 		or die "Unable to execute stmt_troops_search.  " . $stmt_troops_search->errstr;

	# get the results
	my $j = 0;
	while ( my $row = $stmt_troops_search->fetchrow_hashref ( ) ) {
		$troops_past[$j]{'name'} = $row->{'troops_name'};
		$troops_past[$j]{'maxLevel'} = $row->{'troops_maxLevel'};
		$j++;
	}

	# Pull past heros from sql
	if ( $debug ) { print "DEBUG:   Getting all heros from sql.\n"; }
	$stmt_hero_search->execute ()
	 		or die "Unable to execute stmt_hero_search.  " . $stmt_hero_search->errstr;

	# get the results
	$j = 0;
	while ( my $row = $stmt_hero_search->fetchrow_hashref ( ) ) {
		$heros_past[$j]{'name'} = $row->{'hero_name'};
		$heros_past[$j]{'maxLevel'} = $row->{'hero_maxLevel'};
		$j++;
	}

	# Pull past spells from sql
	if ( $debug ) { print "DEBUG:   Getting all spells from sql.\n"; }
	$stmt_spell_search->execute ()
	 		or die "Unable to execute stmt_hero_search.  " . $stmt_hero_search->errstr;

	# get the results
	$j = 0;
	while ( my $row = $stmt_spell_search->fetchrow_hashref ( ) ) {
		$spells_past[$j]{'name'} = $row->{'spells_name'};
		$spells_past[$j]{'maxLevel'} = $row->{'spells_maxLevel'};
		$j++;
	}

	# release sql statements
	$stmt_troops_search->finish();
	$stmt_hero_search->finish();
	$stmt_spell_search->finish();
}

sub member_general_sub {
	# set up sql statements
	my $stmt_league_search = $dbh->prepare ( get_league_search() )
							or die "Unable to prepare stmt_league_search.  " . $dbh->errstr;
	my $stmt_league_insert = $dbh->prepare ( get_league_insert() )
							or die "Unable to prepare stmt_league_insert.  " . $dbh->errstr;
	my $stmt_member_search = $dbh->prepare ( get_member_search() )
							or die "Unable to prepare stmt_member_search.  " . $dbh->errstr;
	my $stmt_member_insert = $dbh->prepare ( get_member_insert() )
							or die "Unable to prepare stmt_member_insert.  " . $dbh->errstr;

	# Update league info if needed?
	$stmt_league_search->execute ( $clan_json{$clan_tag}{'memberList'}[$member_no]{'league'}{'id'} )
							or die "Unable to execute stmt_league_search.  " . $stmt_league_search->errstr;
	
	# check if there was results
	my $temp_league_id = 0;
	while ( my $row = $stmt_league_search->fetchrow_hashref ( ) ) {
		$temp_league_id = $row->{'league_id'};
	}

	# if no results insert league info
	if ( $temp_league_id == 0 && $test == 0 ) {
		# check that iconUrls-medium does not exists
		if ( ! $clan_json{$clan_tag}{'memberList'}[$member_no]{'league'}{'iconUrls'}{'medium'} ) {
			# set to nothing
			$clan_json{$clan_tag}{'memberList'}[$member_no]{'league'}{'iconUrls'}{'medium'} = ' ';
		}
		
		$stmt_league_insert->execute ( $clan_json{$clan_tag}{'memberList'}[$member_no]{'league'}{'id'},
					$clan_json{$clan_tag}{'memberList'}[$member_no]{'league'}{'name'},
					$clan_json{$clan_tag}{'memberList'}[$member_no]{'league'}{'iconUrls'}{'tiny'},
					$clan_json{$clan_tag}{'memberList'}[$member_no]{'league'}{'iconUrls'}{'small'},
					$clan_json{$clan_tag}{'memberList'}[$member_no]{'league'}{'iconUrls'}{'medium'} )
				or die "Unable to execute stmt_league_insert.  " . $stmt_league_insert->errstr;
		if ( $debug ) { print "DEBUG:   League $clan_json{$clan_tag}{'memberList'}[$member_no]{'league'}{'name'} inserted successfully.\n"; }
		$counter{'League inserted'}++;
	}
	
	# grab last donations and donationsReceived entries from sql
	$stmt_member_search->execute ( $clan_json{$clan_tag}{'memberList'}[$member_no]{'tag'} )
				or die "Unable to execute stmt_member_search.  " . $stmt_member_search->errstr;
	
	while ( my $row = $stmt_member_search->fetchrow_hashref ( ) ) {
		$clan_json{$clan_tag}{'memberList'}[$member_no]{'last_donations'} = $row->{'member_donations'};
		$clan_json{$clan_tag}{'memberList'}[$member_no]{'last_total_donations'} = $row->{'member_total_donations'};
		$clan_json{$clan_tag}{'memberList'}[$member_no]{'last_donationsReceived'} = $row->{'member_donationsReceived'};
		$clan_json{$clan_tag}{'memberList'}[$member_no]{'last_total_donationsReceived'} = $row->{'member_total_donationsReceived'};
	}

	# check if member was in table
	if ( defined  ( $clan_json{$clan_tag}{'memberList'}[$member_no]{'last_donations'} ) ) {
		# check if donations are lower, higher, or equal and update accordingly
		if ( $clan_json{$clan_tag}{'memberList'}[$member_no]{'donations'} < 
				$clan_json{$clan_tag}{'memberList'}[$member_no]{'last_donations'} ) {
			$clan_json{$clan_tag}{'memberList'}[$member_no]{'total_donations'} = 
					$clan_json{$clan_tag}{'memberList'}[$member_no]{'last_total_donations'} + 
					$clan_json{$clan_tag}{'memberList'}[$member_no]{'donations'};
		} elsif ( $clan_json{$clan_tag}{'memberList'}[$member_no]{'donations'} > 
				$clan_json{$clan_tag}{'memberList'}[$member_no]{'last_donations'} ) {
			$clan_json{$clan_tag}{'memberList'}[$member_no]{'total_donations'} = 
					$clan_json{$clan_tag}{'memberList'}[$member_no]{'last_total_donations'} + 
					$clan_json{$clan_tag}{'memberList'}[$member_no]{'donations'} - 
					$clan_json{$clan_tag}{'memberList'}[$member_no]{'last_donations'};
		} else {
			$clan_json{$clan_tag}{'memberList'}[$member_no]{'total_donations'} = 
					$clan_json{$clan_tag}{'memberList'}[$member_no]{'last_total_donations'};
		}
	
		# check if donationsReceived are lower, higher, or equal and update accordingly
		if ( $clan_json{$clan_tag}{'memberList'}[$member_no]{'donationsReceived'} < 
				$clan_json{$clan_tag}{'memberList'}[$member_no]{'last_donationsReceived'} ) {
			$clan_json{$clan_tag}{'memberList'}[$member_no]{'total_donationsReceived'} = 
					$clan_json{$clan_tag}{'memberList'}[$member_no]{'last_total_donationsReceived'} + 
					$clan_json{$clan_tag}{'memberList'}[$member_no]{'donationsReceived'};
		} elsif ( $clan_json{$clan_tag}{'memberList'}[$member_no]{'donationsReceived'} > 
				$clan_json{$clan_tag}{'memberList'}[$member_no]{'last_donationsReceived'} ) {
			$clan_json{$clan_tag}{'memberList'}[$member_no]{'total_donationsReceived'} = 
					$clan_json{$clan_tag}{'memberList'}[$member_no]{'last_total_donationsReceived'} + 
					$clan_json{$clan_tag}{'memberList'}[$member_no]{'donationsReceived'} - 
					$clan_json{$clan_tag}{'memberList'}[$member_no]{'last_donationsReceived'};
		} else {
			$clan_json{$clan_tag}{'memberList'}[$member_no]{'total_donationsReceived'} = 
					$clan_json{$clan_tag}{'memberList'}[$member_no]{'last_total_donationsReceived'};
		}
	} else {
		$clan_json{$clan_tag}{'memberList'}[$member_no]{'total_donations'} = 
				$clan_json{$clan_tag}{'memberList'}[$member_no]{'donations'};
		$clan_json{$clan_tag}{'memberList'}[$member_no]{'total_donationsReceived'} = 
				$clan_json{$clan_tag}{'memberList'}[$member_no]{'donationsReceived'};			
	} 

	# pull member info from api
	if ( $debug ) { print "DEBUG:    Getting json for member $clan_json{$clan_tag}{'memberList'}[$member_no]{'name'}.\n"; }

	# set up url
	my $member_tag_temp = $clan_json{$clan_tag}{'memberList'}[$member_no]{'tag'};
	$member_tag_temp =~ s/#/%23/;
	my $member_url = $api_membertag_url . $member_tag_temp;
	# get the member in json
	my $http_response = qx{curl -si --header \'$json_header\' --header \"$api_member_header\" $member_url};
	my ( $http_head, $http_body ) = split ( m{\r?\n\r?\n}, $http_response );

	# check if we got error
	if ( $http_head !~ m#HTTP/1.1 200 OK# ) {
		print "ERROR: Did not get back member web page. Check response code.\n";
		print "================\n";
		print "= HEADER START =\n";
		print "================\n\n";
		print "$http_head\n\n";
		print "================\n";
		print "== HEADER END ==\n";
		print "================\n";
		exit ();
	}
	
	# parse json to hash
	$member_json = parse_json ( $http_body );
	
	# check if we parsed reply correctly
	if ( !$member_json ) {
		print "ERROR: Unable to member parse from API data.";
		exit ();
	}

	$counter{'Member data pulled from API'}++;

	# insert into members table
	if ( $test == 0 ) {
		$stmt_member_insert->execute ( $clan_json{$clan_tag}{'memberList'}[$member_no]{'tag'},
					'#' . $clan_tag,
					$clan_json{$clan_tag}{'memberList'}[$member_no]{'league'}{'id'},
					$clan_json{$clan_tag}{'memberList'}[$member_no]{'name'},
					$time,
					$clan_json{$clan_tag}{'memberList'}[$member_no]{'clanRank'},
					$clan_json{$clan_tag}{'memberList'}[$member_no]{'previousClanRank'},
					$clan_json{$clan_tag}{'memberList'}[$member_no]{'expLevel'},
					$clan_json{$clan_tag}{'memberList'}[$member_no]{'role'},
					$member_json->{'townHallLevel'},
					$clan_json{$clan_tag}{'memberList'}[$member_no]{'trophies'}, 
					$member_json->{'bestTrophies'},
					$member_json->{'warStars'},
					$member_json->{'attackWins'},
					$member_json->{'defenseWins'},
					$clan_json{$clan_tag}{'memberList'}[$member_no]{'donations'}, 
					$clan_json{$clan_tag}{'memberList'}[$member_no]{'total_donations'}, 
					$clan_json{$clan_tag}{'memberList'}[$member_no]{'donationsReceived'}, 
					$clan_json{$clan_tag}{'memberList'}[$member_no]{'total_donationsReceived'} )
				or die "Unable to execute stmt_member_insert.  " . $stmt_member_insert->errstr;
		if ( $debug ) { print "DEBUG:    Member $clan_json{$clan_tag}{'memberList'}[$member_no]{'name'} inserted successfully.\n"; }
		$counter{'Member data inserted'}++;
	}
	# clear sql statements
	$stmt_league_search->finish();
	$stmt_league_insert->finish();
	$stmt_member_search->finish();
	$stmt_member_insert->finish();
}

sub member_achiev_sub {
	# set up sql statements
	my $stmt_achievToMem_search = $dbh->prepare ( get_achievToMem_search() )
							or die "Unable to prepare stmt_achievToMem_search.  " . $dbh->errstr;
	my $stmt_achievToMem_insert = $dbh->prepare ( get_achievToMem_insert() )
							or die "Unable to prepare stmt_achievToMem_insert.  " . $dbh->errstr;
	my $stmt_achiev_insert = $dbh->prepare ( get_achiev_insert() )
							or die "Unable to prepare stmt_achiev_insert.  " . $dbh->errstr;
	
	# Pull current member achievements from sql
	if ( $debug ) { print "DEBUG:    Getting achievement from sql for member $clan_json{$clan_tag}{'memberList'}[$member_no]{'name'}.\n"; }
	$stmt_achievToMem_search->execute ( $clan_json{$clan_tag}{'memberList'}[$member_no]{'tag'} )
				or die "Unable to execute stmt_achievToMem_search.  " . $stmt_achievToMem_search->errstr;
	
	# get the results
	my $j = 0;
	while ( my $row = $stmt_achievToMem_search->fetchrow_hashref ( ) ) {
		$achiev_sql[$j]{'name'} = $row->{'achievToMem_achiev_name'};
		$achiev_sql[$j]{'value'} = $row->{'achievToMem_value'};
		$j++;
	}
			
	# iterate threw members current achievements
	foreach my $achiev_current ( @{ $member_json->{'achievements'} } ) {
		# check if completionInfo is blank and fill if so
		if ( ! $achiev_current->{'completionInfo'} ) {
			$achiev_current->{'completionInfo'} = '';
		}
		
		# check for past achiev
		if ( !@achiev_sql ) {
			if ( $test == 0 ) {
					# insert member achiev into sql
					$stmt_achievToMem_insert->execute ( $clan_json{$clan_tag}{'memberList'}[$member_no]{'tag'},
									 $achiev_current->{'name'},
									 $time,
									 $achiev_current->{'stars'},
									 $achiev_current->{'value'},
									 $achiev_current->{'target'},
									 $achiev_current->{'completionInfo'} )
							or die "Unable to execute stmt_achievToMem_insert.  " . $stmt_achievToMem_insert->errstr;
					if ( $debug ) { print "DEBUG:     Achiev $achiev_current->{'name'} for member $clan_json{$clan_tag}{'memberList'}[$member_no]{'name'} inserted successfully.\n"; }
					$counter{'Member achievToMem inserted'}++;
			}				
			next;
		}
		
		# iterate threw members past achievements
		my $achiev_flag = 0;
		foreach my $achiev_past ( @achiev_sql ) {

			# check if current achiev is in achiev sql
			if ( $achiev_current->{'name'} eq $achiev_past->{'name'} ) { 
				# check if current achiev is different then past achiev
				if ( $achiev_current->{'value'} != $achiev_past->{'value'} && $test == 0 ) {
					# insert member achiev into sql
					$stmt_achievToMem_insert->execute ( $clan_json{$clan_tag}{'memberList'}[$member_no]{'tag'},
							 $achiev_current->{'name'},
							 $time,
							 $achiev_current->{'stars'},
							 $achiev_current->{'value'},
							 $achiev_current->{'target'},
							 $achiev_current->{'completionInfo'} )
						or die "Unable to execute stmt_achievToMem_insert.  " . $stmt_achievToMem_insert->errstr;
					if ( $debug ) { print "DEBUG:     Achiev $achiev_current->{'name'} for member $clan_json{$clan_tag}{'memberList'}[$member_no]{'name'} inserted successfully.\n"; }
					$counter{'Member achievToMem inserted'}++;
				}
				$achiev_flag = 1;
				last;
			}
		}
		if ( $achiev_flag == 0 && $test == 0 ) {
			# insert achiev into sql
			$stmt_achiev_insert->execute ( $achiev_current->{'name'},
										   $achiev_current->{'info'} )
						or die "Unable to execute stmt_achiev_insert.  " . $stmt_achiev_insert->errstr;
			if ( $debug ) { print "DEBUG:     Achiev $achiev_current->{'name'} inserted successfully.\n"; }
			$counter{'New achiev inserted'}++;
			
			# insert member achiev into sql
			$stmt_achievToMem_insert->execute ( $clan_json{$clan_tag}{'memberList'}[$member_no]{'tag'},
							 $achiev_current->{'name'},
							 $time,
							 $achiev_current->{'stars'},
							 $achiev_current->{'value'},
							 $achiev_current->{'target'},
							 $achiev_current->{'completionInfo'} )
						or die "Unable to execute stmt_achievToMem_insert.  " . $stmt_achievToMem_insert->errstr;
			if ( $debug ) { print "DEBUG:     Achiev $achiev_current->{'name'} for member $clan_json{$clan_tag}{'memberList'}[$member_no]{'name'} inserted successfully.\n"; }
			$counter{'Member achievToMem inserted'}++;
		}
	}
	# release sql statements
	$stmt_achievToMem_search->finish();
	$stmt_achievToMem_insert->finish();
	$stmt_achiev_insert->finish();
}

sub member_troop_sub {
	# set up sql statements
	my $stmt_troopsToMem_search = $dbh->prepare ( get_troopsToMem_search() )
							or die "Unable to prepare stmt_troopsToMem_search.  " . $dbh->errstr;
	my $stmt_troopsToMem_insert = $dbh->prepare ( get_troopsToMem_insert() )
							or die "Unable to prepare stmt_troopsToMem_insert.  " . $dbh->errstr;
	my $stmt_troops_insert = $dbh->prepare ( get_troops_insert() )
							or die "Unable to prepare stmt_troops_insert.  " . $dbh->errstr;
	my $stmt_troops_update = $dbh->prepare ( get_troops_update() )
							or die "Unable to prepare stmt_troops_update.  " . $dbh->errstr;

	# Pull current member troops from sql
	if ( $debug ) { print "DEBUG:    Getting troops from sql for member $clan_json{$clan_tag}{'memberList'}[$member_no]{'name'}.\n"; }
	$stmt_troopsToMem_search->execute ( $clan_json{$clan_tag}{'memberList'}[$member_no]{'tag'} )
							or die "Unable to execute stmt_troopsToMem_search.  " . $stmt_troopsToMem_search->errstr;
	
	# get the results
	my $j = 0;
	while ( my $row = $stmt_troopsToMem_search->fetchrow_hashref ( ) ) {
		$troops_sql[$j]{'name'} = $row->{'troopsToMem_troops_name'};
		$troops_sql[$j]{'level'} = $row->{'troopsToMem_level'};
		$j++;
	}
	
	# iterate threw members current troops
	foreach my $troop_current ( @{ $member_json->{'troops'} } ) {
		my $troop_max_flag = 0;
		# check troops flag, so we only go threw this once
		if ( $troops_flag == 0 ) {
			# iterate threw past troops
			foreach my $troop_past ( @troops_past ) {
				# find matching troop
				if ( $troop_current->{'name'} eq $troop_past->{'name'} ) {
					#See if current troop max level equals past troop max level
					if ( $troop_current->{'maxLevel'} != $troop_past->{'maxLevel'} && $test == 0 ) { 
						$stmt_troops_update->execute ( $troop_current->{'maxLevel'},
														$troop_current->{'name'} )
							or die "Unable to execute stmt_troops_update.  " . $stmt_troops_update->errstr;
						if ( $debug ) { print "DEBUG:     Troops $troop_current->{'name'} update successfully.\n"; }
						$counter{'Troop Max Level Updated'}++;
					}
					$troop_max_flag = 1;
					last;
				}
			}
			# check if current troop was found in sql
			if ( $troop_max_flag == 0 && $test == 0  ) {
				# insert troop into sql, new toop
				$stmt_troops_insert->execute ( $troop_current->{'name'},
												   $troop_current->{'maxLevel'} )
							or die "Unable to execute stmt_troops_insert.  " . $stmt_troops_insert->errstr;
				if ( $debug ) { print "DEBUG:     Troop $troop_current->{'name'} inserted successfully.\n"; }
				$counter{'New troop inserted'}++;
			}
		}
		
		# check for past member troops, new member
		if ( !@troops_sql ) {
			if ( $test == 0 ) {
				# insert member troop into sql
				$stmt_troopsToMem_insert->execute ( $clan_json{$clan_tag}{'memberList'}[$member_no]{'tag'},
							 $troop_current->{'name'},
							 $time,
							 $troop_current->{'level'} )
						or die "Unable to execute stmt_troopsToMem_insert.  " . $stmt_troopsToMem_insert->errstr;
				if ( $debug ) { print "DEBUG:     Troop $troop_current->{'name'} for member $clan_json{$clan_tag}{'memberList'}[$member_no]{'name'} inserted successfully.\n"; }
				$counter{'Member troopToMem inserted'}++;
			}				
			next;
		}

		# iterate threw members past troops
		my $troop_flag = 0;
		foreach my $troop_past ( @troops_sql ) {
			# check if current troop is in troop sql
			if ( $troop_current->{'name'} eq $troop_past->{'name'} ) { 
				# check if current troop is different then past troop, troops changed
				if ( $troop_current->{'level'} != $troop_past->{'level'} && $test == 0 ) {
					# insert member troop into sql
					$stmt_troopsToMem_insert->execute ( $clan_json{$clan_tag}{'memberList'}[$member_no]{'tag'},
								 $troop_current->{'name'},
								 $time,
								 $troop_current->{'level'} )
							or die "Unable to execute stmt_troopsToMem_insert.  " . $stmt_troopsToMem_insert->errstr;
					if ( $debug ) { print "DEBUG:     Troop $troop_current->{'name'} for member $clan_json{$clan_tag}{'memberList'}[$member_no]{'name'} inserted successfully.\n"; }
					$counter{'Member troopToMem inserted'}++;
				}
				$troop_flag = 1;
				last;
			}
		}
		if ( $troop_flag == 0 && $test == 0 ) {
			# insert member troop into sql, no current troop (DE barracks)
			$stmt_troopsToMem_insert->execute ( $clan_json{$clan_tag}{'memberList'}[$member_no]{'tag'},
							 $troop_current->{'name'},
							 $time,
							 $troop_current->{'level'} )
						or die "Unable to execute stmt_troopsToMem_insert.  " . $stmt_troopsToMem_insert->errstr;
			if ( $debug ) { print "DEBUG:     Troop $troop_current->{'name'} for member $clan_json{$clan_tag}{'memberList'}[$member_no]{'name'} inserted successfully.\n"; }
			$counter{'Member troopToMem inserted'}++;
		}
	}
	# set troops flag so troops do not get updated again
	$troops_flag = 1;
	
	# release sql statements
	$stmt_troopsToMem_search->finish();
	$stmt_troopsToMem_insert->finish();
	$stmt_troops_insert->finish();
	$stmt_troops_update->finish();	
}

sub member_hero_sub {

	# set up sql statements
	my $stmt_heroToMem_search = $dbh->prepare ( get_heroToMem_search() )
							or die "Unable to prepare stmt_heroToMem_search.  " . $dbh->errstr;
	my $stmt_heroToMem_insert = $dbh->prepare ( get_heroToMem_insert() )
							or die "Unable to prepare stmt_heroToMem_insert.  " . $dbh->errstr;
	my $stmt_hero_insert = $dbh->prepare ( get_hero_insert() )
							or die "Unable to prepare stmt_hero_insert.  " . $dbh->errstr;
	my $stmt_hero_update = $dbh->prepare ( get_hero_update() )
							or die "Unable to prepare stmt_hero_update.  " . $dbh->errstr;

	# Pull current member heros from sql
	if ( $debug ) { print "DEBUG:    Getting heros from sql for member $clan_json{$clan_tag}{'memberList'}[$member_no]{'name'}.\n"; }
	$stmt_heroToMem_search->execute ( $clan_json{$clan_tag}{'memberList'}[$member_no]{'tag'} )
							or die "Unable to execute stmt_heroToMem_search.  " . $stmt_heroToMem_search->errstr;

	# get the results
	my $j = 0;
	while ( my $row = $stmt_heroToMem_search->fetchrow_hashref ( ) ) {
		$heros_sql[$j]{'name'} = $row->{'heroToMem_heros_name'};
		$heros_sql[$j]{'level'} = $row->{'heroToMem_level'};
		$j++;
	}

	# iterate threw members current heros
	foreach my $hero_current ( @{ $member_json->{'heroes'} } ) {
		my $hero_max_flag = 0;
		# check heros flag, so we only go threw this once
		if ( $heros_flag == 0 ) {
			# iterate threw past heros
			foreach my $hero_past ( @heros_past ) {
				# find matching hero
				if ( $hero_current->{'name'} eq $hero_past->{'name'} ) {
					#See if current hero max level equals past hero max level
					if ( $hero_current->{'maxLevel'} != $hero_past->{'maxLevel'} && $test == 0 ) { 
						$stmt_hero_update->execute ( $hero_current->{'maxLevel'},
														$hero_current->{'name'} )
							or die "Unable to execute stmt_hero_update.  " . $stmt_hero_update->errstr;
						if ( $debug ) { print "DEBUG:     Heros $hero_current->{'name'} update successfully.\n"; }
						$counter{'Hero Max Level Updated'}++;
					}
					$hero_max_flag = 1;
					last;
				}
			}
			# check if current hero was found in sql
			if ( $hero_max_flag == 0 && $test == 0  ) {
				# insert hero into sql, new toop
				$stmt_hero_insert->execute ( $hero_current->{'name'},
												   $hero_current->{'maxLevel'} )
							or die "Unable to execute stmt_hero_insert.  " . $stmt_hero_insert->errstr;
				if ( $debug ) { print "DEBUG:     Hero $hero_current->{'name'} inserted successfully.\n"; }
				$counter{'New hero inserted'}++;
			}
		}
		
		# check for past member heros, new member
		if ( !@heros_sql ) {
			if ( $test == 0 ) {
				# insert member hero into sql
				$stmt_heroToMem_insert->execute ( $clan_json{$clan_tag}{'memberList'}[$member_no]{'tag'},
							 $hero_current->{'name'},
							 $time,
							 $hero_current->{'level'} )
						or die "Unable to execute stmt_heroToMem_insert.  " . $stmt_heroToMem_insert->errstr;
				if ( $debug ) { print "DEBUG:     Hero $hero_current->{'name'} for member $clan_json{$clan_tag}{'memberList'}[$member_no]{'name'} inserted successfully.\n"; }
				$counter{'Member heroToMem inserted'}++;
			}				
			next;
		}

		# iterate threw members past heros
		my $hero_flag = 0;
		foreach my $hero_past ( @heros_sql ) {
			# check if current hero is in hero sql
			if ( $hero_current->{'name'} eq $hero_past->{'name'} ) { 
				# check if current hero is different then past hero, heros changed
				if ( $hero_current->{'level'} != $hero_past->{'level'} && $test == 0 ) {
					# insert member hero into sql
					$stmt_heroToMem_insert->execute ( $clan_json{$clan_tag}{'memberList'}[$member_no]{'tag'},
								 $hero_current->{'name'},
								 $time,
								 $hero_current->{'level'} )
							or die "Unable to execute stmt_heroToMem_insert.  " . $stmt_heroToMem_insert->errstr;
					if ( $debug ) { print "DEBUG:     Hero $hero_current->{'name'} for member $clan_json{$clan_tag}{'memberList'}[$member_no]{'name'} inserted successfully.\n"; }
					$counter{'Member heroToMem inserted'}++;
				}
				$hero_flag = 1;
				last;
			}
		}
		if ( $hero_flag == 0 && $test == 0 ) {
			# insert member hero into sql, no current hero (DE barracks)
			$stmt_heroToMem_insert->execute ( $clan_json{$clan_tag}{'memberList'}[$member_no]{'tag'},
							 $hero_current->{'name'},
							 $time,
							 $hero_current->{'level'} )
						or die "Unable to execute stmt_heroToMem_insert.  " . $stmt_heroToMem_insert->errstr;
			if ( $debug ) { print "DEBUG:     Hero $hero_current->{'name'} for member $clan_json{$clan_tag}{'memberList'}[$member_no]{'name'} inserted successfully.\n"; }
			$counter{'Member heroToMem inserted'}++;
		}
	}
	# set heros flag so heros do not get updated again
	$heros_flag = 1;
	
	# release sql statements
	$stmt_heroToMem_search->finish();
	$stmt_heroToMem_insert->finish();
	$stmt_hero_insert->finish();
	$stmt_hero_update->finish();	
}

sub member_spell_sub {

 	# set up sql statements
	my $stmt_spellToMem_search = $dbh->prepare ( get_spellToMem_search() )
							or die "Unable to prepare stmt_spellToMem_search.  " . $dbh->errstr;
	my $stmt_spellToMem_insert = $dbh->prepare ( get_spellToMem_insert() )
							or die "Unable to prepare stmt_spellToMem_insert.  " . $dbh->errstr;
	my $stmt_spell_insert = $dbh->prepare ( get_spell_insert() )
							or die "Unable to prepare stmt_spell_insert.  " . $dbh->errstr;
	my $stmt_spell_update = $dbh->prepare ( get_spell_update() )
							or die "Unable to prepare stmt_spell_update.  " . $dbh->errstr;

	# Pull current member spells from sql
	if ( $debug ) { print "DEBUG:    Getting spells from sql for member $clan_json{$clan_tag}{'memberList'}[$member_no]{'name'}.\n"; }
	$stmt_spellToMem_search->execute ( $clan_json{$clan_tag}{'memberList'}[$member_no]{'tag'} )
							or die "Unable to execute stmt_spellToMem_search.  " . $stmt_spellToMem_search->errstr;

	# get the results
	my $j = 0;
	while ( my $row = $stmt_spellToMem_search->fetchrow_hashref ( ) ) {
		$spells_sql[$j]{'name'} = $row->{'spellsToMem_spells_name'};
		$spells_sql[$j]{'level'} = $row->{'spellsToMem_level'};
		$j++;
	}

	# iterate threw members current spells
	foreach my $spell_current ( @{ $member_json->{'spells'} } ) {
		my $spell_max_flag = 0;
		#check spells flag, so we only go threw this once
		if ( $spells_flag == 0 ) {
			# iterate threw past spells
			foreach my $spell_past ( @spells_past ) {
				#find matching spell
				if ( $spell_current->{'name'} eq $spell_past->{'name'} ) {
					# See if current spell max level equals past spell max level
					if ( $spell_current->{'maxLevel'} != $spell_past->{'maxLevel'} && $test == 0 ) { 
						$stmt_spell_update->execute ( $spell_current->{'maxLevel'},
														$spell_current->{'name'} )
							or die "Unable to execute stmt_spell_update.  " . $stmt_spell_update->errstr;
						if ( $debug ) { print "DEBUG:     Spells $spell_current->{'name'} update successfully.\n"; }
						$counter{'Spell Max Level Updated'}++;
					}
					$spell_max_flag = 1;
					last;
				}
			}
			# check if current spell was found in sql
			if ( $spell_max_flag == 0 && $test == 0  ) {
				# insert spell into sql, new toop
				$stmt_spell_insert->execute ( $spell_current->{'name'},
												   $spell_current->{'maxLevel'} )
							or die "Unable to execute stmt_spell_insert.  " . $stmt_spell_insert->errstr;
				if ( $debug ) { print "DEBUG:     Spell $spell_current->{'name'} inserted successfully.\n"; }
				$counter{'New spell inserted'}++;
			}
		}
		
		# check for past member spells, new member
		if ( !@spells_sql ) {
			if ( $test == 0 ) {
				# insert member spell into sql
				$stmt_spellToMem_insert->execute ( $clan_json{$clan_tag}{'memberList'}[$member_no]{'tag'},
							 $spell_current->{'name'},
							 $time,
							 $spell_current->{'level'} )
						or die "Unable to execute stmt_spellToMem_insert.  " . $stmt_spellToMem_insert->errstr;
				if ( $debug ) { print "DEBUG:     Spell $spell_current->{'name'} for member $clan_json{$clan_tag}{'memberList'}[$member_no]{'name'} inserted successfully.\n"; }
				$counter{'Member spellToMem inserted'}++;
			}				
			next;
		}

		# iterate threw members past spells
		my $spell_flag = 0;
		foreach my $spell_past ( @spells_sql ) {
			# check if current spell is in spell sql
			if ( $spell_current->{'name'} eq $spell_past->{'name'} ) { 
				# check if current spell is different then past spell, spells changed
				if ( $spell_current->{'level'} != $spell_past->{'level'} && $test == 0 ) {
					# insert member spell into sql
					$stmt_spellToMem_insert->execute ( $clan_json{$clan_tag}{'memberList'}[$member_no]{'tag'},
								 $spell_current->{'name'},
								 $time,
								 $spell_current->{'level'} )
							or die "Unable to execute stmt_spellToMem_insert.  " . $stmt_spellToMem_insert->errstr;
					if ( $debug ) { print "DEBUG:     Spell $spell_current->{'name'} for member $clan_json{$clan_tag}{'memberList'}[$member_no]{'name'} inserted successfully.\n"; }
					$counter{'Member spellToMem inserted'}++;
				}
				$spell_flag = 1;
				last;
			}
		}
		if ( $spell_flag == 0 && $test == 0 ) {
			# insert member spell into sql, no current spell (Spell Factory Upgrade)
			$stmt_spellToMem_insert->execute ( $clan_json{$clan_tag}{'memberList'}[$member_no]{'tag'},
							 $spell_current->{'name'},
							 $time,
							 $spell_current->{'level'} )
						or die "Unable to execute stmt_spellToMem_insert.  " . $stmt_spellToMem_insert->errstr;
			if ( $debug ) { print "DEBUG:     Spell $spell_current->{'name'} for member $clan_json{$clan_tag}{'memberList'}[$member_no]{'name'} inserted successfully.\n"; }
			$counter{'Member spellToMem inserted'}++;
		}
	}
	# set spells flag so spells do not get updated again
	$spells_flag = 1;
	
	# release sql statements
	$stmt_spellToMem_search->finish();
	$stmt_spellToMem_insert->finish();
	$stmt_spell_insert->finish();
	$stmt_spell_update->finish();	
}
