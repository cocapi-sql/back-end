#! /usr/bin/perl

use Data::Dumper;
use Time::Piece;
use JSON::Parse 'parse_json';
use DBI;
use Config::Simple;

# SQL Variables
my $sql_platform = "mysql";			# change if not using mysql
my $dbname = "thebl962_jumi";		# set to database name
my $servername = "localhost";		# change if not local database
my $username = "thebl962_jumi";		# set to database username
my $password = "=4%Q}PJTeV8y";		# set to database password
my $dsn = "DBI:" . $sql_platform . ":" . $dbname . ":" . $localhost;

# Set up api token and url
my $api_token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiIsImtpZCI6IjI4YTMxOGY3LTAwMDAtYTFlYi03ZmExLTJjNzQzM2M2Y2NhNSJ9.eyJpc3MiOiJzdXBlcmNlbGwiLCJhdWQiOiJzdXBlcmNlbGw6Z2FtZWFwaSIsImp0aSI6IjZlMDRiOGM2LWY1NmEtNGJmZC1iZTE2LTc3MDI3NDU3ZTUyZSIsImlhdCI6MTQ3MjQwNTMxMywic3ViIjoiZGV2ZWxvcGVyL2MzNjMxODk2LWNiMzItM2IxYi1lMWNjLTNlMzY1NTdhMzA2OCIsInNjb3BlcyI6WyJjbGFzaCJdLCJsaW1pdHMiOlt7InRpZXIiOiJkZXZlbG9wZXIvc2lsdmVyIiwidHlwZSI6InRocm90dGxpbmcifSx7ImNpZHJzIjpbIjY1LjM5LjE5My4xMCJdLCJ0eXBlIjoiY2xpZW50In1dfQ.iY02G39z7xfZaLtgEoApDwtCBMCaYp-u_fCh1-8PDdqxCpX2GEmFVaUo7TKUTVD15eIG1UtwGYcZ6AmzWLjaWw";		#set to COC API key
my $api_clantag_url = "https://api.clashofclans.com/v1/clans/";

# load clan tags into array, Note leave the # off
my @clan_tags = ();
push @clan_tags, 'P9GYVGCP'; # Black List
push @clan_tags, 'LQVUPRR0'; # White List
push @clan_tags, 'QLP22JYG'; # Gold List

# set up headers
my $json_header = "Accept: application/json";
my $api_header = "authorization: Bearer " . $api_token;

# set up variables
my %clan_json = ();
my %warlog_json = ();
my %counter = ();
my $debug = 0;

# check for cli arguments
while ( @ARGV ) {
	if ( $Parameter eq '-debug' ) {
		$debug = 1;
		print "Running in DEBUG mode.\n";
	} elsif ( $Parameter eq '-clan' ) {
		my $temp_clan_tag = shift();
		push @clan_tags, $temp_clan_tag;
		print "Adding $temp_clan_tag to list of clans.\n";
	} elsif ( $Parameter eq '-usage' ) {
		print "usage: $0 [-debug] [-clan 'Clan Tag']\n";
		exit ();
	}
}

# loop threw clan tag array
for my $clan_tag ( @clan_tags ) {  
	if ( $debug ) { print "DEBUG: Getting json for clan $clan_tag.\n"; }
	
	# set up url
	my $clan_url = $api_clantag_url . '%23' . $clan_tag;
	my $warlog_url = $api_clantag_url . '%23' . $clan_tag . "/warlog";
	# get the clan in json
	my $http_response = qx{curl -si --header \'$json_header\' --header \"$api_header\" $clan_url};
	my ( $http_head, $http_body ) = split ( m{\r?\n\r?\n}, $http_response );
	
	# check if we got error
	if ( $http_head !~ m#HTTP/1.1 200 OK# ) {
		print "ERROR: Did not get back clan web page. Check response code.\n";
		print "================\n";
		print "= HEADER START =\n";
		print "================\n\n";
		print "$http_head\n\n";
		print "================\n";
		print "== HEADER END ==\n";
		print "================\n";
		exit ();
	}
		
	# parse json to hash
	$clan_json->{$clan_tag} = parse_json ( $http_body );
	
	# check if we parsed reply correctly
	if ( !$clan_json->{$clan_tag} ) {
		print "ERROR: Unable to clan parse from API data.";
		exit ();
	}

	if ( $debug ) { print "DEBUG: Successful got clan data for $clan_tag.\n"; }
	$counter->{'Clan data pulled from API'}++;
	
	# check if war log is public
	if ( $clan_json->{$clan_tag}->{'isWarLogPublic'} == 1 ) {
		# get the war log in json
		my $http_response = qx{curl -si --header \'$json_header\' --header \"$api_header\" $warlog_url};
		my ( $http_head, $http_body ) = split ( m{\r?\n\r?\n}, $http_response );
	
		# check if we got error
		if ( $http_head !~ m#HTTP/1.1 200 OK# ) {
			print "ERROR: Did not get back clan web page. Check response code.\n";
			print "================\n";
			print "= HEADER START =\n";
			print "================\n\n";
			print "$http_head\n\n";
			print "================\n";
			print "== HEADER END ==\n";
			print "================\n";
			exit ();
		}
		
		# parse json to hash
		$clan_json->{$clan_tag}->{'warlog'} = parse_json ( $http_body );
	
		# check if we parsed reply correctly
		if ( !$clan_json->{$clan_tag}->{'warlog'} ) {
			print "ERROR: Unable to warlog parse from API data.";
			exit ();
		}

		if ( $debug ) { print "DEBUG: Successful got warlog for $clan_tag.\n"; }
		$counter->{'Clan warlog pulled from API'}++;
	}
}

# connect to MySQL database
my %attr = ( PrintError=>0,RaiseError=>1 );
my $dbh = DBI->connect ( $dsn, $username, $password, \%attr );

# Get Greenwich time zone and convert to sql date-time stamp
my ($S, $M, $H, $d, $m, $Y) = gmtime(time);
$m += 1;
$Y += 1900;
my $time = sprintf("%04d-%02d-%02d %02d:%02d:%02d", $Y, $m, $d, $H, $M, $S);

# set up warlog search sql statement
my $sql_warlog_search = "SELECT warlog_opp_tag  
  FROM `API_WarLog` 
  WHERE `warlog_clan_tag` LIKE ? 
  ORDER BY `warlog_endTime` DESC 
  LIMIT 1";
   
my $stmt_warlog_search = $dbh->prepare ( $sql_warlog_search );

# set up warlog insert sql statement
my $sql_warlog_insert = "INSERT INTO `thebl962_jumi`.`API_WarLog` (`warlog_id`, `warlog_result`, 
  `warlog_endTime`, `warlog_teamSize`, `warlog_clan_tag`, `warlog_clan_clanLevel`, `warlog_clan_stars`, 
  `warlog_clan_destructionPercentage`, `warlog_clan_attacks`, `warlog_clan_expEarned`, `warlog_opp_tag`, 
  `warlog_opp_name`, `warlog_opp_clanLevel`, `warlog_opp_stars`, `warlog_opp_destructionPercentage`, 
  `warlog_opp_badgeUrls_small`, `warlog_opp_badgeUrls_medium`, `warlog_opp_badgeUrls_large`) 
  VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
   
my $stmt_warlog_insert = $dbh->prepare ( $sql_warlog_insert );

# set up clan info war lose search sql statement
my $sql_clan_warLosses_search = "SELECT `clan_info_time`, `clan_info_warTies`, `clan_info_warLosses` 
   FROM `API_Clan_Info` 
   WHERE `clan_info_tag` LIKE ? 
     AND `clan_info_warLosses` > 0 
   ORDER BY `clan_info_time` 
   DESC LIMIT 1";
   
my $stmt_clan_warLosses_search = $dbh->prepare ( $sql_clan_warLosses_search );

# set up clan search sql statement
my $sql_clan_search = "SELECT *
   FROM `API_Clan`
   WHERE `clan_tag` LIKE ?";
   
my $stmt_clan_search = $dbh->prepare ( $sql_clan_search );

# set up clan insert sql statement
my $sql_clan_insert = "INSERT INTO `thebl962_jumi`.`API_Clan` (`clan_tag`, `clan_name`, `clan_description`, 
   `clan_badgeUrls_small`, `clan_badgeUrls_medium`, `clan_badgeUrls_large`, `clan_location_id`, 
   `clan_location_name`, `clan_location_countryCode`, `clan_warFrequency`) 
   VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

my $stmt_clan_insert = $dbh->prepare ( $sql_clan_insert );

# set up clan update sql statement
my $sql_clan_update = "UPDATE `thebl962_jumi`.`API_Clan` 
  SET `clan_description` = ?,
     `clan_badgeUrls_small` = ?,
     `clan_badgeUrls_medium` = ?,
     `clan_badgeUrls_large` = ?,
     `clan_location_id` = ?,
     `clan_location_name` = ?,
     `clan_location_countryCode` = ?,
     `clan_warFrequency` = ?
  WHERE `API_Clan`.`clan_tag` = ?";
  
my $stmt_clan_update = $dbh->prepare ( $sql_clan_update );

# set up clan info insert sql statement
my $sql_clan_info_insert = "INSERT INTO `thebl962_jumi`.`API_Clan_Info` (`clan_info_id`, `clan_info_time`, 
   `clan_info_tag`, `clan_info_clanLevel`, `clan_info_clanPoints`, `clan_info_warWins`, 
   `clan_info_warWinStreak`,`clan_info_warTies`, `clan_info_warLosses`, `clan_info_members`, 
   `clan_info_type`,`clan_info_requiredTrophies`, `clan_info_isWarLogPublic`) 
   VALUES ( NULL,?,?,?,?,?,?,?,?,?,?,?,? )";
 
my $stmt_clan_info_insert = $dbh->prepare ( $sql_clan_info_insert );

# set up member search sql statement
my $sql_member_search = "SELECT `member_donations`, `member_total_donations`, `member_donationsReceived`, 
   `member_total_donationsReceived` 
   FROM `API_Member` 
   WHERE `member_tag` LIKE ? 
   ORDER BY `member_timestamp` DESC 
   LIMIT 1";

my $stmt_member_search = $dbh->prepare ( $sql_member_search );

# set up member insert sql statement
my $sql_member_insert = "INSERT INTO `thebl962_jumi`.`API_Member` (`member_id`, `member_tag`, 
   `member_clan_tag`, `member_league_id`, `member_name`, `member_timestamp`, `member_clanRank`, 
   `member_previousClanRank`, `member_expLevel`, `member_role`, `member_trophies`, `member_donations`, 
    `member_total_donations`, `member_donationsReceived`, `member_total_donationsReceived`) 
   VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
   
my $stmt_member_insert = $dbh->prepare ( $sql_member_insert );

# set up league search sql statement
my $sql_league_search = "SELECT *
   FROM `API_League`
   WHERE `league_id` =?";

my $stmt_league_search = $dbh->prepare ( $sql_league_search );

# set up league insert sql statement
my $sql_league_insert = "INSERT INTO `thebl962_jumi`.`API_League` (`league_id`, `league_name`, 
   `league_iconUrls_tiny`, `league_iconUrls_small`, `league_iconUrls_medium`) 
   VALUES (?, ?, ?, ?, ?)";
   
my $stmt_league_insert = $dbh->prepare ( $sql_league_insert );


# iterate threw clans
foreach my $tag ( @clan_tags ){
	if ( $debug ) { print "DEBUG: Start working on $clan_json->{$tag}->{'name'} clan's data.\n"; }

	# check if war log is private
	if ( $clan_json->{$tag}->{'isWarLogPublic'} != 1 ) {
		if ( $debug ) { print "DEBUG:   War Log is private.\n"; }
		
		# set war flag to false
		delete $clan_json->{$tag}->{'isWarLogPublic'};
		$clan_json->{$tag}->{'isWarLogPublic'} = 0;
		
		# get most recent warTies and warLosses from db
		$stmt_clan_warLosses_search->execute( '#' . $tag );
		while (@row = $stmt_clan_warLosses_search->fetchrow_array) { 
			# update warLosses and warTies
			$clan_json->{$tag}->{'warLosses'} = pop @row;
			$clan_json->{$tag}->{'warTies'} = pop @row;
			if ( $debug ) { print "DEBUG:   Updated war ties and losses from last entry.\n"; }
			
			# get time of last event and see if it is over a week old
			my $time_update = pop @row;
			my $time_formate = '%Y-%m-%d %H:%M:%S';
			my $time_diff = Time::Piece->strptime ( $time, $time_formate )
					 - Time::Piece->strptime ( $time_update, $time_formate );
			if ( $time_diff > 604800 ) {
				print "WARNING: The clans war log has not been updated in over a week\n";
			}
		}
		
		# Check if war losses are empty, set to 0
		if ( ! $clan_json->{$tag}->{'warLosses'} ) {
			$clan_json->{$tag}->{'warLosses'} = 0;
			if ( $debug ) { print "DEBUG:   War losses set to 0.\n"; }
		}
		
		# Check if war ties are empty, set to 0
		if ( ! $clan_json->{$tag}->{'warTies'} ) {
			$clan_json->{$tag}->{'warTies'} = 0;
			if ( $debug ) { print "DEBUG:   War ties set to 0.\n"; }
		}
		$counter->{'Clan war log is private'}++;
	} 
	
	# check if clan is in clan table
	if ( $stmt_clan_search->execute ( '#' . $tag ) ) {	
		if ( $debug ) { print "DEBUG:   Clan $clan_json->{$tag}->{'name'} select successfully.\n"; }
	}
		
	# check if there was results
	my $clan_count = 0;
	while ( my $row = $stmt_clan_search->fetchrow_arrayref ( ) ) {
		# Update clan table
		if ( $stmt_clan_update->execute ( $clan_json->{$tag}->{'description'},
										  $clan_json->{$tag}->{'badgeUrls'}->{'small'},
										  $clan_json->{$tag}->{'badgeUrls'}->{'medium'},
										  $clan_json->{$tag}->{'badgeUrls'}->{'large'},
										  $clan_json->{$tag}->{'location'}{'id'},
										  $clan_json->{$tag}->{'location'}{'name'},
										  $clan_json->{$tag}->{'location'}{'countryCode'},
										  $clan_json->{$tag}->{'warFrequency'},
										  '#' . $tag ) ) {
			if ( $debug ) { print "DEBUG:   Clan $clan_json->{$tag}->{'name'} update successfully.\n"; }
			$counter->{'Clan data updated'}++;
			$clan_count++;
		}
	}

	if ( $clan_count == 0 ) {
		# Insert clan table
		if ( $stmt_clan_insert->execute ( '#' . $tag,
										  $clan_json->{$tag}->{'name'},
										  $clan_json->{$tag}->{'description'},
										  $clan_json->{$tag}->{'badgeUrls'}->{'small'},
										  $clan_json->{$tag}->{'badgeUrls'}->{'medium'},
										  $clan_json->{$tag}->{'badgeUrls'}->{'large'},
										  $clan_json->{$tag}->{'location'}{'id'},
										  $clan_json->{$tag}->{'location'}{'name'},
										  $clan_json->{$tag}->{'location'}{'countryCode'},
										  $clan_json->{$tag}->{'warFrequency'} ) ) {
			if ( $debug ) { print "DEBUG:   Clan $clan_json->{$tag}->{'name'} inserted successfully.\n"; }
			$counter->{'Clan data inserted'}++;
		}
	}

	# insert clan info into clan_info table
	if ( $stmt_clan_info_insert->execute ( $time,
										   '#' . $tag,
										   $clan_json->{$tag}->{'clanLevel'},
										   $clan_json->{$tag}->{'clanPoints'},
										   $clan_json->{$tag}->{'warWins'},
										   $clan_json->{$tag}->{'warWinStreak'},
										   $clan_json->{$tag}->{'warTies'},
										   $clan_json->{$tag}->{'warLosses'},
										   $clan_json->{$tag}->{'members'},
										   $clan_json->{$tag}->{'type'},
										   $clan_json->{$tag}->{'requiredTrophies'},
										   $clan_json->{$tag}->{'isWarLogPublic'} ) ) {
		if ( $debug ) { print "DEBUG:   Clan info for $clan_json->{$tag}->{'name'} inserted successfully.\n"; }
		$counter->{'Clan info data inserted'}++;
 	}
 	
 	# check if war log is public
 	if ( $clan_json->{$tag}->{'isWarLogPublic'} == 1 ) {
		if ( $debug ) { print "DEBUG:   War Log is public.\n"; }
		
		# get newest war from sql
		if ( $stmt_warlog_search->execute ( '#' . $tag ) ) {	
			if ( $debug ) { print "DEBUG:   Warlog for $clan_json->{$tag}->{'name'} select successfully.\n"; }
		}
		
		# check if there was results
		my $temp_opp_tag;
		while ( my $row = $stmt_warlog_search->fetchrow_hashref ( ) ) {
			$temp_opp_tag = $row->{'warlog_opp_tag'};
		}

		# iterate threw warlog
		for ( my $i = 0; $i < @{$clan_json->{$tag}->{'warlog'}->{'items'}}; $i++ ) {
			# check if this is different then the newest war
			my $test_opp_tag = $clan_json->{$tag}->{'warlog'}->{'items'}->[$i]->{'opponent'}->{'tag'};
			
			if ( $temp_opp_tag !~ m/$test_opp_tag/ ) {

				# convert date
				if ( $clan_json->{$tag}->{'warlog'}->{'items'}->[$i]->{'endTime'} =~ 
						m/(\d\d\d\d)(\d\d)(\d\d)T(\d\d)(\d\d)(\d\d)/ ) {
					$warlog_time = $1 . '-' . $2 . '-' . $3 . ' ' . $4 . ':' . $5 . ':' . $6;
					
					# add warlog
					if ( $stmt_warlog_insert->execute ( $clan_json->{$tag}->{'warlog'}->{'items'}->[$i]->{'result'},
								$warlog_time,
								$clan_json->{$tag}->{'warlog'}->{'items'}->[$i]->{'teamSize'},
								$clan_json->{$tag}->{'warlog'}->{'items'}->[$i]->{'clan'}->{'tag'},
								$clan_json->{$tag}->{'warlog'}->{'items'}->[$i]->{'clan'}->{'clanLevel'},
								$clan_json->{$tag}->{'warlog'}->{'items'}->[$i]->{'clan'}->{'stars'},
								$clan_json->{$tag}->{'warlog'}->{'items'}->[$i]->{'clan'}->{'destructionPercentage'},
								$clan_json->{$tag}->{'warlog'}->{'items'}->[$i]->{'clan'}->{'attacks'},
								$clan_json->{$tag}->{'warlog'}->{'items'}->[$i]->{'clan'}->{'expEarned'},
								$clan_json->{$tag}->{'warlog'}->{'items'}->[$i]->{'opponent'}->{'tag'},
								$clan_json->{$tag}->{'warlog'}->{'items'}->[$i]->{'opponent'}->{'name'},
								$clan_json->{$tag}->{'warlog'}->{'items'}->[$i]->{'opponent'}->{'clanLevel'},
								$clan_json->{$tag}->{'warlog'}->{'items'}->[$i]->{'opponent'}->{'stars'},
								$clan_json->{$tag}->{'warlog'}->{'items'}->[$i]->{'opponent'}->{'destructionPercentage'},
								$clan_json->{$tag}->{'warlog'}->{'items'}->[$i]->{'opponent'}->{'badgeUrls'}->{'small'},
								$clan_json->{$tag}->{'warlog'}->{'items'}->[$i]->{'opponent'}->{'badgeUrls'}->{'medium'},
								$clan_json->{$tag}->{'warlog'}->{'items'}->[$i]->{'opponent'}->{'badgeUrls'}->{'large'} ) ) {
						if ( $debug ) { print "DEBUG:   WarLog for $clan_json->{$tag}->{'name'} inserted successfully.\n"; }
						$counter->{'War Log data inserted'}++;
					}
				} else {
					print "ERROR: Unable to match warlog time\n";
				}
			} else {
				#skip the rest of the warlog
				last;
			}
		}
		$counter->{'Clan war log is public'}++;
	}

 	# iterate threw members of clans
 	for ( my $i = 0; $i < $clan_json->{$tag}->{'members'}; $i++ ) {
 		# Update league info if needed?
 		$stmt_league_search->execute ( $clan_json->{$tag}->{'memberList'}[$i]{'league'}{'id'} );
 		
 		# check if there was results
		my $temp_league_id = 0;
		while ( my $row = $stmt_league_search->fetchrow_hashref ( ) ) {
			$temp_league_id = $row->{'league_id'};
		}


 		# if no results insert league info
 		if ( $temp_league_id == 0 ) {
 			# check that iconUrls-medium does not exists
 			if ( ! $clan_json->{$tag}->{'memberList'}[$i]{'league'}{'iconUrls'}{'medium'} ) {
 				# set to nothing
 				$clan_json->{$tag}->{'memberList'}[$i]{'league'}{'iconUrls'}{'medium'} = ' ';
 			}
 			
 		 	if ( $stmt_league_insert->execute ( $clan_json->{$tag}->{'memberList'}[$i]{'league'}{'id'},
 												$clan_json->{$tag}->{'memberList'}[$i]{'league'}{'name'},
 												$clan_json->{$tag}->{'memberList'}[$i]{'league'}{'iconUrls'}{'tiny'},
 												$clan_json->{$tag}->{'memberList'}[$i]{'league'}{'iconUrls'}{'small'},
 												$clan_json->{$tag}->{'memberList'}[$i]{'league'}{'iconUrls'}{'medium'} ) ) {
 				if ( $debug ) { print "DEBUG:   League $clan_json->{$tag}->{'memberList'}[$i]{'league'}{'name'} inserted successfully.\n"; }
 				$counter->{'League inserted'}++;
 			}
 		}
 		
 		# grab last donations and donationsReceived entries from sql
 		$stmt_member_search->execute ( $clan_json->{$tag}->{'memberList'}[$i]{'tag'} );
 		
		while ( my $row = $stmt_member_search->fetchrow_hashref ( ) ) {
			$clan_json->{$tag}->{'memberList'}[$i]{'last_donations'} = $row->{'member_donations'};
			$clan_json->{$tag}->{'memberList'}[$i]{'last_total_donations'} = $row->{'member_total_donations'};
			$clan_json->{$tag}->{'memberList'}[$i]{'last_donationsReceived'} = $row->{'member_donationsReceived'};
			$clan_json->{$tag}->{'memberList'}[$i]{'last_total_donationsReceived'} = $row->{'member_total_donationsReceived'};
		}

 		# check if member was in table
 		if ( defined  ( $clan_json->{$tag}->{'memberList'}[$i]{'last_donations'} ) ) {
			# check if donations are lower, higher, or equal and update accordingly
			if ( $clan_json->{$tag}->{'memberList'}[$i]{'donations'} < 
					$clan_json->{$tag}->{'memberList'}[$i]{'last_donations'} ) {
				$clan_json->{$tag}->{'memberList'}[$i]{'total_donations'} = 
						$clan_json->{$tag}->{'memberList'}[$i]{'last_total_donations'} + 
						$clan_json->{$tag}->{'memberList'}[$i]{'donations'};
			} elsif ( $clan_json->{$tag}->{'memberList'}[$i]{'donations'} > 
					$clan_json->{$tag}->{'memberList'}[$i]{'last_donations'} ) {
				$clan_json->{$tag}->{'memberList'}[$i]{'total_donations'} = 
						$clan_json->{$tag}->{'memberList'}[$i]{'last_total_donations'} + 
						$clan_json->{$tag}->{'memberList'}[$i]{'donations'} - 
						$clan_json->{$tag}->{'memberList'}[$i]{'last_donations'};
			} else {
				$clan_json->{$tag}->{'memberList'}[$i]{'total_donations'} = 
						$clan_json->{$tag}->{'memberList'}[$i]{'last_total_donations'};
			}
		
			# check if donationsReceived are lower, higher, or equal and update accordingly
			if ( $clan_json->{$tag}->{'memberList'}[$i]{'donationsReceived'} < 
					$clan_json->{$tag}->{'memberList'}[$i]{'last_donationsReceived'} ) {
				$clan_json->{$tag}->{'memberList'}[$i]{'total_donationsReceived'} = 
						$clan_json->{$tag}->{'memberList'}[$i]{'last_total_donationsReceived'} + 
						$clan_json->{$tag}->{'memberList'}[$i]{'donationsReceived'};
			} elsif ( $clan_json->{$tag}->{'memberList'}[$i]{'donationsReceived'} > 
					$clan_json->{$tag}->{'memberList'}[$i]{'last_donationsReceived'} ) {
				$clan_json->{$tag}->{'memberList'}[$i]{'total_donationsReceived'} = 
						$clan_json->{$tag}->{'memberList'}[$i]{'last_total_donationsReceived'} + 
						$clan_json->{$tag}->{'memberList'}[$i]{'donationsReceived'} - 
						$clan_json->{$tag}->{'memberList'}[$i]{'last_donationsReceived'};
			} else {
				$clan_json->{$tag}->{'memberList'}[$i]{'total_donationsReceived'} = 
						$clan_json->{$tag}->{'memberList'}[$i]{'last_total_donationsReceived'};
			}
 		} else {
  			$clan_json->{$tag}->{'memberList'}[$i]{'total_donations'} = 
 					$clan_json->{$tag}->{'memberList'}[$i]{'donations'};
 			$clan_json->{$tag}->{'memberList'}[$i]{'total_donationsReceived'} = 
 					$clan_json->{$tag}->{'memberList'}[$i]{'donationsReceived'};			
 		} 

 		# insert into members table
 	  	if ( $stmt_member_insert->execute ( $clan_json->{$tag}->{'memberList'}[$i]{'tag'},
 	  										'#' . $tag,
 	  										$clan_json->{$tag}->{'memberList'}[$i]{'league'}{'id'},
 	  										$clan_json->{$tag}->{'memberList'}[$i]{'name'},
 	  										$time,
 	  										$clan_json->{$tag}->{'memberList'}[$i]{'clanRank'},
 	  										$clan_json->{$tag}->{'memberList'}[$i]{'previousClanRank'},
 	  										$clan_json->{$tag}->{'memberList'}[$i]{'expLevel'},
 	  										$clan_json->{$tag}->{'memberList'}[$i]{'role'},
 	  										$clan_json->{$tag}->{'memberList'}[$i]{'trophies'}, 
 	  										$clan_json->{$tag}->{'memberList'}[$i]{'donations'}, 
 	  										$clan_json->{$tag}->{'memberList'}[$i]{'total_donations'}, 
 	  										$clan_json->{$tag}->{'memberList'}[$i]{'donationsReceived'}, 
 	  										$clan_json->{$tag}->{'memberList'}[$i]{'total_donationsReceived'} ) ) {
			if ( $debug ) { print "DEBUG:     Member $clan_json->{$tag}->{'memberList'}[$i]{'name'} inserted successfully.\n"; }
			$counter->{'Member data inserted'}++;
 		}
 	}
 	
 	if ( $debug ) { print "DEBUG: Finished working on $clan_json->{$tag}->{'name'} clan's data.\n"; }
}

# clean up and disconnect from the MySQL database
$stmt_clan_warLosses_search->finish();
$stmt_clan_search->finish();
$stmt_clan_insert->finish();
$stmt_clan_update->finish();
$stmt_clan_info_insert->finish();
$stmt_league_search->finish();
$stmt_league_insert->finish();
$stmt_member_search->finish();
$stmt_member_insert->finish();
$dbh->disconnect();
 
# display counter
print "\n";
print "Summary\n";
print "-" x 50 . " " . "-" x 5 . "\n";
foreach my $key ( sort keys %{ $counter } ) {
	printf( "%-50s %5d\n", $key, $counter->{$key} );
}



#print Dumper( $clan_json->{QLP22JYG}->{'warlog'}->{'items'}->[0] );

