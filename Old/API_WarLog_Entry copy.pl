#! /usr/bin/perl

#############################################
#                  COC API                  #
#                                           #
#     Used to update clan_info records      #
#         from the warLog database.         #
#                                           #
#              by Gary Douglas              #
#               Sep. 24, 2016               #
#        http://www.the-blacklist.ca        #
#############################################


use Data::Dumper;
use Time::Piece;
use DBI;

# SQL Variables
my $sql_platform = "mysql";			# change if not using mysql
my $dbname = "thebl962_jumi";		# set to database name
my $servername = "localhost";		# change if not local database
my $username = "thebl962_jumi";		# set to database username
my $password = "=4%Q}PJTeV8y";		# set to database password
my $dsn = "DBI:" . $sql_platform . ":" . $dbname . ":" . $localhost;

# load clan tags into array, Note leave the # off
my @clan_tags = ();
push @clan_tags, 'P9GYVGCP'; # Black List
push @clan_tags, 'LQVUPRR0'; # White List
push @clan_tags, 'QLP22JYG'; # Gold List

# set up variables
my %counter = ();
my $debug = 1;

# check for cli arguments
while ( @ARGV ) {
	if ( $Parameter eq '-debug' ) {
		$debug = 1;
		print "Running in DEBUG mode.\n";
	} elsif ( $Parameter eq '-clan' ) {
		my $temp_clan_tag = shift();
		push @clan_tags, $temp_clan_tag;
		print "Adding $temp_clan_tag to list of clans.\n";
	} elsif ( $Parameter eq '-usage' ) {
		print "usage: $0 [-debug] [-clan 'Clan Tag']\n";
		exit ();
	}
}

# connect to MySQL database
my %attr = ( PrintError=>0,RaiseError=>1 );
my $dbh = DBI->connect ( $dsn, $username, $password, \%attr );

# set up warlog search sql statement
my $sql_warlog_search = "SELECT `warlog_endTime`, `warlog_result`, `warlog_clan_expEarned`, `warlog_clan_clanLevel`  
  FROM `API_WarLog` 
  WHERE `warlog_clan_tag` LIKE ? 
  ORDER BY `warlog_endTime` DESC";
   
my $stmt_warlog_search = $dbh->prepare ( $sql_warlog_search );

# set up clan_info search sql statement
my $sql_clanInfo_search = "SELECT * 
FROM `API_Clan_Info` 
WHERE `clan_info_tag` LIKE ? 
ORDER BY `clan_info_time` ASC 
LIMIT 1";

my $stmt_clanInfo_search = $dbh->prepare ( $sql_clanInfo_search );

# set up clan info insert sql statement
my $sql_clan_info_insert = "INSERT INTO `thebl962_jumi`.`API_Clan_Info` (`clan_info_id`, `clan_info_time`, 
   `clan_info_tag`, `clan_info_clanLevel`, `clan_info_clanPoints`, `clan_info_warWins`, 
   `clan_info_warWinStreak`,`clan_info_warTies`, `clan_info_warLosses`, `clan_info_members`, 
   `clan_info_type`,`clan_info_requiredTrophies`, `clan_info_isWarLogPublic`) 
   VALUES ( NULL,?,?,?,?,?,?,?,?, '0', '0', '0', '0' )";
 
my $stmt_clan_info_insert = $dbh->prepare ( $sql_clan_info_insert );


# loop threw clan tag array
for my $clan_tag ( @clan_tags ) {  
	if ( $debug ) { print "DEBUG: Start working on $clan_tag clan's data.\n"; }
	
	# setup clean counters for clan
	my @warlog = ();
	my %clanInfo = ();
	
	# pull warlog data for clan
	if ( $stmt_warlog_search->execute ( '#' . $clan_tag ) ) {	
		if ( $debug ) { print "DEBUG:   Clan warLog execute successfully.\n"; }
	}
		
	# get warlog results
	while ( my $row = $stmt_warlog_search->fetchrow_hashref ( ) ) {
		push ( @warlog, $row );
		$counter->{'Searched and grabbed: WarLog'}++;
	}
	
	# check if we got data
	if ( @warlog ) {
		if ( $debug ) { print "DEBUG:   Clan warlog retrieved successfully.\n"; }
	} else {
		# no data skip to next clan
		print "WARNING: No WarLog Data for Clan $clan_tag";
		$counter->{'No data found: WarLog'}++;
		next;
	}

	# pull clanInfo data for clan
	if ( $stmt_clanInfo_search->execute ( '#' . $clan_tag ) ) {	
		if ( $debug ) { print "DEBUG:   Clan clanInfo execute successfully.\n"; }
	}
		
	# get warlog results
	$clanInfo = $stmt_clanInfo_search->fetchrow_hashref ( );
	
	# check if we got data
	if ( $clanInfo ) {
		$counter->{'Searched and grabbed: ClanInfo'}++;
	} else {
		print "WARNING: No ClanInfo Data for Clan $clan_tag";
		$counter->{'No data found: ClanInfo'}++;
		next;
	}
	
	# Convert clanInfo time 
	my $time_formate = '%Y-%m-%d %H:%M:%S';
	my $clanInfo_time = Time::Piece->strptime ( $clanInfo->{'clan_info_time'}, $time_formate );
	
	# Setup up clanInfo array
	my $clanInfo_counter = 0;
	my @clanInfo_insert = ();
	$clanInfo_insert[0]->{'clan_info_time'} = @warlog[0]->{'warlog_endTime'};
	$clanInfo_insert[0]->{'clan_info_clanLevel'} = @warlog[0]->{'warlog_clan_clanLevel'};
	$clanInfo_insert[0]->{'clan_info_clanPoints'} = $clanInfo->{'clan_info_clanPoints'};
	$clanInfo_insert[0]->{'clan_info_warWins'} = $clanInfo->{'clan_info_warWins'};
	$clanInfo_insert[0]->{'clan_info_warWinStreak'} = $clanInfo->{'clan_info_warWinStreak'};
	$clanInfo_insert[0]->{'clan_info_warLosses'} = $clanInfo->{'clan_info_warLosses'};
	$clanInfo_insert[0]->{'clan_info_warTies'} = $clanInfo->{'clan_info_warTies'};

	# iterate threw warLog array
	foreach my $i ( 0 .. $#warlog ) {
		# Convert warlog time
		my $warLog_time = Time::Piece->strptime ( @warlog[$i]->{'warlog_endTime'}, $time_formate );
		
		# check if warLog time is before clanInfo time
		if ( $warLog_time < $clanInfo_time ) {
			# increase clanInfo counter
			$clanInfo_counter++;
			
			# Check if win, lose, or tie
			if ( @warlog[$i]->{'warlog_result'} eq 'win' ) {
				# WIN, adjust counters
				$clanInfo_insert[$clanInfo_counter]->{'clan_info_warWins'} = 
					$clanInfo_insert[$clanInfo_counter - 1]->{'clan_info_warWins'} - 1;
				$clanInfo_insert[$clanInfo_counter]->{'clan_info_warLosses'} = 
					$clanInfo_insert[$clanInfo_counter - 1]->{'clan_info_warLosses'};
				$clanInfo_insert[$clanInfo_counter]->{'clan_info_warTies'} = 
					$clanInfo_insert[$clanInfo_counter - 1]->{'clan_info_warTies'};
				$clanInfo_insert[$clanInfo_counter]->{'clan_info_warWinStreak'} = 1;
				# iterate threw previous clanInfo_insert and update clan tie if needed
				for ( my $j = 1; $j <= 100 ; $j++ ) {
					# check if previous war was a win
					if ( $clanInfo_insert[$clanInfo_counter - $j]->{'war_results'} eq 'win' ) {
						# update win streak
						$clanInfo_insert[$clanInfo_counter - $j]->{'clan_info_warWinStreak'}++;
					} else {
						# break out of loop
						last;
					}
				}
				$clanInfo_insert[$clanInfo_counter]->{'war_results'} = 'win';
				if ( $debug ) { print "DEBUG:   ClanInfo adjust: Win.\n"; }
			} elsif ( @warlog[$i]->{'warlog_result'} eq 'lose' ) {
				# LOSE, adjust counters
				$clanInfo_insert[$clanInfo_counter]->{'clan_info_warWins'} = 
					$clanInfo_insert[$clanInfo_counter - 1]->{'clan_info_warWins'};
				$clanInfo_insert[$clanInfo_counter]->{'clan_info_warLosses'} = 
					$clanInfo_insert[$clanInfo_counter - 1]->{'clan_info_warLosses'} - 1;
				$clanInfo_insert[$clanInfo_counter]->{'clan_info_warTies'} = 
					$clanInfo_insert[$clanInfo_counter - 1]->{'clan_info_warTies'};
				$clanInfo_insert[$clanInfo_counter]->{'clan_info_warWinStreak'} = 0;
				$clanInfo_insert[$clanInfo_counter]->{'war_results'} = 'lose';
				if ( $debug ) { print "DEBUG:   ClanInfo adjust: Lose.\n"; }
			} elsif ( @warlog[$i]->{'warlog_result'} eq 'tie' ) {
				# TIE, adjust counters
				$clanInfo_insert[$clanInfo_counter]->{'clan_info_warWins'} = 
					$clanInfo_insert[$clanInfo_counter - 1]->{'clan_info_warWins'};
				$clanInfo_insert[$clanInfo_counter]->{'clan_info_warLosses'} = 
					$clanInfo_insert[$clanInfo_counter - 1]->{'clan_info_warLosses'};
				$clanInfo_insert[$clanInfo_counter]->{'clan_info_warTies'} = 
					$clanInfo_insert[$clanInfo_counter - 1]->{'clan_info_warTies'} - 1;
				$clanInfo_insert[$clanInfo_counter]->{'clan_info_warWinStreak'} = 0;
				$clanInfo_insert[$clanInfo_counter]->{'war_results'} = 'tie';
				if ( $debug ) { print "DEBUG:   ClanInfo adjust: Tie.\n"; }
			}
			# grab warLog time and level
			$clanInfo_insert[$clanInfo_counter]->{'clan_info_time'} = @warlog[$i]->{'warlog_endTime'};
			$clanInfo_insert[$clanInfo_counter]->{'clan_info_clanLevel'} = @warlog[$i]->{'warlog_clan_clanLevel'};
			
			# adjust clan points
			$clanInfo_insert[$clanInfo_counter]->{'clan_info_clanPoints'} = 
				$clanInfo_insert[$clanInfo_counter - 1]->{'clan_info_clanPoints'} - 
				@warlog[$i]->{'warlog_clan_expEarned'};
			
		} else {
			if ( $debug ) { print "DEBUG:   ClanInfo skipped.\n"; }
			$counter->{'Insert skipped: ClanInfo'}++;
		}
	}
	
	#iterate threw clanInfo_insert array
	foreach my $i ( 0 .. $#clanInfo_insert ) {
		# skip if 0
		if ( $i == 0 ) {
			next;
		}

		# insert clanInfo_insert into clan_info table
		if ( $stmt_clan_info_insert->execute ( @clanInfo_insert[$i]->{'clan_info_time'},
											   '#' . $clan_tag,
											   @clanInfo_insert[$i]->{'clan_info_clanLevel'},
											   @clanInfo_insert[$i]->{'clan_info_clanPoints'},
											   @clanInfo_insert[$i]->{'clan_info_warWins'},
											   @clanInfo_insert[$i]->{'clan_info_warWinStreak'},
											   @clanInfo_insert[$i]->{'clan_info_warTies'},
											   @clanInfo_insert[$i]->{'clan_info_warLosses'} ) ) {
			if ( $debug ) { print "DEBUG:   ClanInfo inserted successfully.\n"; }
			$counter->{'Insert successful: ClanInfo'}++;
		}
	}
	if ( $debug ) { print "DEBUG: Finished working on $clan_tag clan's data.\n"; }
	$counter->{'Clan Processed'}++;
}
	
# clean up and disconnect from the MySQL database
$stmt_warlog_search->finish();
$stmt_clanInfo_search->finish();
$stmt_clan_info_insert->finish();
$dbh->disconnect();
	
	
# display counter
print "\n";
print "Summary\n";
print "-" x 50 . " " . "-" x 5 . "\n";
foreach my $key ( sort keys %{ $counter } ) {
	printf( "%-50s %5d\n", $key, $counter->{$key} );
}

	
