#! /usr/bin/perl

use strict;
use warnings;

##########
# SQL Variables
##########
# set to sql platform
sub get_sql_platform {
 	return "mysql";
};
# set to database name
sub get_dbname {
 	return "thebl962_jumi"
};
# set to database location
sub get_servername {
	return "localhost";
};
# set to database username
sub get_username {
	return "thebl962_jumi";
};
# set to database password
sub get_password {
	return '=4%Q}PJTeV8y';
};

##########
# API Token
##########
# set to Clan API Token
sub get_api_clan_token {
	return "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiIsImtpZCI6IjI4YTMxOGY3LTAwMDAtYTFlYi03ZmExLTJjNzQzM2M2Y2NhNSJ9.eyJpc3MiOiJzdXBlcmNlbGwiLCJhdWQiOiJzdXBlcmNlbGw6Z2FtZWFwaSIsImp0aSI6IjZlMDRiOGM2LWY1NmEtNGJmZC1iZTE2LTc3MDI3NDU3ZTUyZSIsImlhdCI6MTQ3MjQwNTMxMywic3ViIjoiZGV2ZWxvcGVyL2MzNjMxODk2LWNiMzItM2IxYi1lMWNjLTNlMzY1NTdhMzA2OCIsInNjb3BlcyI6WyJjbGFzaCJdLCJsaW1pdHMiOlt7InRpZXIiOiJkZXZlbG9wZXIvc2lsdmVyIiwidHlwZSI6InRocm90dGxpbmcifSx7ImNpZHJzIjpbIjY1LjM5LjE5My4xMCJdLCJ0eXBlIjoiY2xpZW50In1dfQ.iY02G39z7xfZaLtgEoApDwtCBMCaYp-u_fCh1-8PDdqxCpX2GEmFVaUo7TKUTVD15eIG1UtwGYcZ6AmzWLjaWw";		#set to COC API key
};
# set to Member API Token
sub get_api_member_token {
	return "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiIsImtpZCI6IjI4YTMxOGY3LTAwMDAtYTFlYi03ZmExLTJjNzQzM2M2Y2NhNSJ9.eyJpc3MiOiJzdXBlcmNlbGwiLCJhdWQiOiJzdXBlcmNlbGw6Z2FtZWFwaSIsImp0aSI6Ijk1YzE2NmQ5LWQ2MDUtNDk2MC1iZjVjLWMxMmY1YTA4ODI1NCIsImlhdCI6MTQ3Njc1NDY2OSwic3ViIjoiZGV2ZWxvcGVyL2MzNjMxODk2LWNiMzItM2IxYi1lMWNjLTNlMzY1NTdhMzA2OCIsInNjb3BlcyI6WyJjbGFzaCJdLCJsaW1pdHMiOlt7InRpZXIiOiJkZXZlbG9wZXIvc2lsdmVyIiwidHlwZSI6InRocm90dGxpbmcifSx7ImNpZHJzIjpbIjY1LjM5LjE5My4xMCJdLCJ0eXBlIjoiY2xpZW50In1dfQ.7PrW682Oi1CR0qfEl-Mqr83MXod1I5T-Wls6qiMgORn8ZvxLeQBHLUS28g9veakzkpEZg58xZIB_Yoc-eMswMg";
};

##########
# Clan List
##########
# set to Clan Tag List
sub get_clan_list {
	my @clan_tags = ();
	push @clan_tags, 'P9GYVGCP'; # Black List
	push @clan_tags, 'RC9CL0G2'; # Red List
#	push @clan_tags, 'LQVUPRR0'; # White List
# 	push @clan_tags, 'QLP22JYG'; # Gold List
	return @clan_tags;
};

##########
# API Token
##########
# set to Debug (verbose logging)
#	0 = debug off
#	1 = debug on
sub get_debug {
	return 0
};
# set to Test (SQL Insert)
#	0 = SQL Insert on
#	1 = SQL Insert off (test mode)
sub get_test {
	return 0
};

##########
# Mail Settings
##########
# set to Mail (will only send mail in war and right when war ends)
#	0 = Mail off
#	1 = Mail on
sub get_mail_flag {
	return 1;
};
# to address for mail
sub get_mail_to {
	return 'douglas.gary@gmail.com';
};
sub get_mail_ban_to {
	return 'douglas.gary@gmail.com';
};
# from address for mail
sub get_mail_from{
	return 'gary@the-blacklist.ca';
};

# cc address for mail
sub get_cc_from{
	return '';
};
sub get_cc_ban_from{
	return '';
};

return 1;

