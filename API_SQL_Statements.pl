#! /usr/bin/perl

use strict;
use warnings;

##############################
#
#  Subroutines
#
##############################
##############################
# API_Mem_Wars
##############################
# set up member_wars search sql statement
sub get_memberWars_search {
	return "SELECT * 
		FROM  `API_Mem_Wars` 
		WHERE `member_tag` LIKE ?";
};

# set up member_wars insert sql statement
sub get_memberWars_insert {
	return "INSERT INTO `thebl962_jumi`.`API_Mem_Wars` (`member_id`, `member_tag`, 
		`member_war`, `member_attack_used`, `member_attack_stars`, `member_attack_newStars`,
		`member_attack_destruction`, `member_attack_3Stars`, `member_attack_2Stars`, `member_attack_1Stars`, 
		`member_attack_0Stars`, `member_defense_used`, `member_defense_stars`, `member_defense_newStars`, 
		`member_defense_destruction`, `member_defense_3Stars`, `member_defense_2Stars`, `member_defense_1Stars`, 
		`member_defense_0Stars`, `member_clean_cnt`, `member_loot_cnt`, `member_earlyLoot_cnt`, `member_noCC_cnt`) 
		VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
};

# set up member_wars update sql statement
sub get_memberWars_update {
	return "UPDATE  `thebl962_jumi`.`API_Mem_Wars` 
		SET  `member_war` =  ?, 
			`member_attack_used` =  ?, 
			`member_attack_stars` =  ?, 
			`member_attack_newStars` =  ?, 
			`member_attack_destruction` =  ?, 
			`member_attack_3Stars` =  ?, 
			`member_attack_2Stars` =  ?, 
			`member_attack_1Stars` =  ?, 
			`member_attack_0Stars` =  ?, 
			`member_attack_3NewStars` =  ?, 
			`member_attack_2NewStars` =  ?, 
			`member_attack_1NewStars` =  ?, 
			`member_attack_0NewStars` =  ?, 
			`member_defense_used` =  ?, 
			`member_defense_stars` =  ?, 
			`member_defense_newStars` =  ?, 
			`member_defense_destruction` =  ?, 
			`member_defense_3Stars` =  ?, 
			`member_defense_2Stars` =  ?, 
			`member_defense_1Stars` =  ?, 
			`member_defense_0Stars` =  ?, 
			`member_defense_3NewStars` =  ?, 
			`member_defense_2NewStars` =  ?, 
			`member_defense_1NewStars` =  ?, 
			`member_defense_0NewStars` =  ?, 
			`member_clean_cnt` =  ?, 
			`member_loot_cnt` =  ?, 
			`member_earlyLoot_cnt` =  ?, 
			`member_noCC_cnt` =  ? 
		WHERE  `API_Mem_Wars`.`member_tag` = ?";
};


##############################
# API_WarLog_Battle
##############################
# set up warlog_battle insert sql statement
sub get_warlog_battle_insert {
	return "INSERT INTO  `thebl962_jumi`.`API_WarLog_Battle` (
		`battle_id`, `battle_warlog_id`, `battle_attackerTag`, `battle_defenderTag`,
		`battle_stars`, `battle_newStars`, `battle_destructionPercentage`, `battle_order`, 
		`battle_clean_flag`, `battle_loot_flag`, `battle_earlyLoot_flag`, `battle_noCC_flag`) 
		VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
};

# set up walog_battle complete search statement
sub get_walog_battle_comp_search {
	return "SELECT battle_warlog_id, battle_order, battle_attackerTag AS att_tag, att.member_id AS att_id, 
			att.member_clan_tag AS att_clan_tag, att.member_name AS att_name, 
			att.member_townhallLevel AS att_townhallLevel, att.member_mapPosition AS att_mapPosition, 
			battle_defenderTag AS def_tag, def.member_id as def_id, 
			def.member_clan_tag AS def_clan_tag, def.member_name AS def_name, 
			def.member_townhallLevel AS def_townhallLevel, def.member_mapPosition AS def_mapPosition, 
			battle_stars, battle_newStars, battle_destructionPercentage, battle_clean_flag, battle_loot_flag, 
			battle_earlyLoot_flag, battle_noCC_flag, war.warlog_startTime AS warlog_startTime 
		FROM `API_WarLog_Battle` AS bat
		INNER JOIN `API_WarLog_Member` AS att
			ON bat.battle_attackerTag LIKE att.member_tag
			AND bat.battle_warlog_id = att.member_warlog_id
		INNER JOIN `API_WarLog_Member` AS def
			ON bat.battle_defenderTag LIKE def.member_tag
			AND bat.battle_warlog_id = def.member_warlog_id
		INNER JOIN `API_WarLog` AS war
			ON bat.battle_warlog_id = war.warlog_id";
};


# set up warlog_battle search sql statement
sub get_warlog_battle_search {
	return "SELECT * 
		FROM  `API_WarLog_Battle` 
		WHERE  `battle_warlog_id` = ?";
};

# set up member_wars select that pulls home members attacks
sub get_warlog_battle_att_select {
	return "SELECT *  
		 FROM `API_WarLog_Battle` 
		 RIGHT JOIN `API_Mem_Wars` 
		  ON `battle_attackerTag` LIKE `member_tag` 
		 WHERE `battle_attackerTag` IS NOT NULL 
		 ORDER BY `battle_warlog_id` ASC, `battle_order` ASC;";
};

# set up member_wars select that pulls home members defense
sub get_warlog_battle_def_select {
	return "SELECT *  
		 FROM `API_WarLog_Battle` 
		 RIGHT JOIN `API_Mem_Wars` 
		  ON `battle_defenderTag` LIKE `member_tag` 
		 WHERE `battle_defenderTag` IS NOT NULL 
		 ORDER BY `battle_warlog_id` ASC, `battle_order` ASC;";
};
##############################
# API_WarLog
##############################
# set up warlog search sql statement
sub get_warlog_clan_search {
	return "SELECT *  
	  FROM `API_WarLog` 
	  WHERE `warlog_clan_tag` LIKE ? 
	  ORDER BY `warlog_endTime` DESC";
};

# set up warlog search on opponent sql statement
sub get_warlog_opp_search {
	return "SELECT *  
	  FROM `API_WarLog` 
	  WHERE `warlog_opp_tag` LIKE ? 
	  ORDER BY `warlog_endTime` DESC";
};

# set up warlog search sql statement
sub get_warlog_search {
	return "SELECT warlog_opp_tag  
	  FROM `API_WarLog` 
	  WHERE `warlog_clan_tag` LIKE ? 
	  	AND (`warlog_result` LIKE 'win'
	  		OR `warlog_result` LIKE 'lose'
	  		OR `warlog_result` LIKE 'tie')
	  ORDER BY `warlog_endTime` DESC 
	  LIMIT 1";
};

# set up warlog insert sql statement
sub get_warlog_withtimes_insert {
	return "INSERT INTO `thebl962_jumi`.`API_WarLog` (`warlog_id`, `warlog_result`, `warlog_details`, 
	  `warlog_preparationStartTime`, `warlog_startTime`, `warlog_endTime`, `warlog_teamSize`, 
	  `warlog_clan_tag`, `warlog_clan_clanLevel`, `warlog_clan_stars`, `warlog_clan_destructionPercentage`, 
	  `warlog_clan_attacks`, `warlog_clan_expEarned`, `warlog_opp_tag`, `warlog_opp_name`, 
	  `warlog_opp_clanLevel`, `warlog_opp_stars`, `warlog_opp_destructionPercentage`, 
	  `warlog_opp_attacks`, `warlog_opp_badgeUrls_small`, `warlog_opp_badgeUrls_medium`, 
	  `warlog_opp_badgeUrls_large`) 
	  VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
};

# set up warlog insert sql statement
sub get_warlog_insert {
	return "INSERT INTO `thebl962_jumi`.`API_WarLog` (`warlog_id`, `warlog_result`, `warlog_details`,
	  `warlog_endTime`, `warlog_teamSize`, `warlog_clan_tag`, `warlog_clan_clanLevel`, `warlog_clan_stars`, 
	  `warlog_clan_destructionPercentage`, `warlog_clan_attacks`, `warlog_clan_expEarned`, `warlog_opp_tag`, 
	  `warlog_opp_name`, `warlog_opp_clanLevel`, `warlog_opp_stars`, `warlog_opp_destructionPercentage`, 
	  `warlog_opp_badgeUrls_small`, `warlog_opp_badgeUrls_medium`, `warlog_opp_badgeUrls_large`) 
	  VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
};

# set up warlog update sql statement
sub get_warlog_update {
	return "UPDATE  `thebl962_jumi`.`API_WarLog` 
		SET  `warlog_clan_stars` =  ?, 
			`warlog_clan_destructionPercentage` =  ?, 
			`warlog_clan_attacks` =  ?, 
			`warlog_opp_stars` =  ?, 
			`warlog_opp_destructionPercentage` =  ?, 
			`warlog_opp_attacks` =  ? 
		WHERE  `API_WarLog`.`warlog_id` =?";
};

# set up warlog end update sql statement
sub get_warlog_end_update {
	return "UPDATE  `thebl962_jumi`.`API_WarLog` 
		SET  `warlog_result` =  ?, 
			`warlog_clan_expEarned` =  ? 
		WHERE  `API_WarLog`.`warlog_id` =?";
};

##############################
# API_WarLog_Member
##############################
# set up warlog_member insert sql statement
sub get_warlog_member_insert {
	return "INSERT INTO  `thebl962_jumi`.`API_WarLog_Member` ( 
		`member_id`, `member_warlog_id`, `member_clan_tag`, `member_tag`, `member_name`, 
		`member_townhallLevel`,	`member_off_warWeight`, `member_mapPosition`, `member_attacks_used`, 
		`member_attack_stars`, `member_attack_destruction`, `member_defense_used`, 
		`member_defense_stars`, `member_defense_destruction`, `member_clean_flag`, 
		`member_loot_flag`, `member_earlyLoot_flag`, `member_noCC_flag`) 
 		VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)";
};

# set up warlog_member update sql statement
sub get_warlog_member_update {
	return "UPDATE  `thebl962_jumi`.`API_WarLog_Member` 
		SET  `member_attacks_used` =  ?, 
			`member_attack_stars` =  ?, 
			`member_attack_newStars` = ?, 
			`member_attack_destruction` =  ?, 
			`member_defense_used` =  ?, 
			`member_defense_stars` =  ?, 
			`member_defense_newStars` = ?, 
			`member_defense_destruction` =  ?, 
			`member_clean_flag` =  ?, 
			`member_loot_flag` =  ?, 
			`member_earlyLoot_flag` =  ?, 
			`member_noCC_flag` =  ? 
		WHERE  `member_warlog_id` LIKE  ?
			AND `member_tag` LIKE  ?";
};

# set up warlog_member search sql statement
sub get_warlog_member_search {
	return "SELECT * 
		FROM  `API_WarLog_Member` 
		WHERE  `member_warlog_id` LIKE  ?
			AND `member_tag` LIKE  ?";
};

# set up warlog_member search with clan tag sql statement
sub get_warlog_member_clan_search {
	return "SELECT * 
		FROM  `API_WarLog_Member` 
		WHERE  `member_clan_tag` LIKE  ?";
};

# set up warlog_member search to return count of wars
sub get_warlog_member_count_search {
	return "SELECT COUNT(`member_id`) AS member_wars
  		FROM `API_WarLog_Member` 
  		WHERE `member_tag` 
  		LIKE ?";
};

##############################
# API_WarWeight_TH
##############################
# set up search sql statement
sub get_wwTH_search {
	return "SELECT * 
		FROM `API_WarWeight_TH`
		WHERE `TH` LIKE ?";
};

##############################
# API_Mem_WarWeight
##############################
# set up search sql statement
sub get_memWW_search {
	return "SELECT * 
		FROM `API_Mem_WarWeight`
		WHERE `memWW_member_tag` LIKE ?
	  	ORDER BY `memWW_id` DESC 
	  	LIMIT 1";
};

# set up search by time sql statement
sub get_memWW_time_search {
	return "SELECT * 
		FROM `API_Mem_WarWeight`
		WHERE `memWW_member_tag` LIKE ?
            AND `memWW_timestamp` <= ?
	  	ORDER BY `memWW_id` DESC 
	  	LIMIT 1";
};

# set up search sql statement
sub get_memWW_insert {
	return "INSERT INTO `thebl962_jumi`.`API_Mem_WarWeight` (`memWW_id`, `memWW_member_tag`, 
	   `memWW_timestamp`, `memWW_defBldg`, `memWW_wall`, `memWW_trap`, 
	   `memWW_miscBldg`,`memWW_hero_off`, `memWW_spell`, `memWW_total`, 
	   `memWW_unadjusted`,`memWW_penalty`, `memWW_adjusted`) 
	   VALUES ( NULL,?,?,?,?,?,?,?,?,?,?,?,? )";
};

# set up insert war weight without defense sql statement
sub get_memWW_woDef_insert {
	return "INSERT INTO `thebl962_jumi`.`API_Mem_WarWeight` (`memWW_id`, `memWW_member_tag`, 
	   `memWW_timestamp`, `memWW_hero_off`, `memWW_hero_def`, `memWW_troop`, 
	   `memWW_spell`,`memWW_defense`, `memWW_offense`, `memWW_total`) 
	   VALUES ( NULL,?,?,?,?,?,?,?,?,? )";
};

sub get_memWW_Def_insert {
	return "INSERT INTO `thebl962_jumi`.`API_Mem_WarWeight` (`memWW_id`, `memWW_member_tag`, 
	   `memWW_timestamp`, `memWW_defBldg`, `memWW_wall`, `memWW_trap`, `memWW_miscBldg`, 
	   `memWW_hero_off`, `memWW_hero_def`, `memWW_troop`, `memWW_spell`, 
	   `memWW_defense`, `memWW_offense`, `memWW_total`, `memWW_rounded`, `memWW_Step1`, 
	   `memWW_Step2`, `memWW_Step3_OffRatio`, `memWW_Step3_DefRatio`, 
	   `memWW_Step3_DiffRatio`, `memWW_Step3`, `memWW_Step4_Diff`, `memWW_Step4`) 
	   VALUES ( NULL,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,? )";
};

##############################
# API_Clan_Info
##############################
# set up clan info war lose search sql statement
sub get_clan_warLosses_search {
	return "SELECT `clan_info_time`, `clan_info_warTies`, `clan_info_warLosses` 
	   FROM `API_Clan_Info` 
	   WHERE `clan_info_tag` LIKE ? 
		 AND `clan_info_warLosses` > 0 
	   ORDER BY `clan_info_time` 
	   DESC LIMIT 1";
};

# set up clan info insert sql statement
sub get_clan_info_insert {
	return "INSERT INTO `thebl962_jumi`.`API_Clan_Info` (`clan_info_id`, `clan_info_time`, 
	   `clan_info_tag`, `clan_info_clanLevel`, `clan_info_clanPoints`, `clan_info_warWins`, 
	   `clan_info_warWinStreak`,`clan_info_warTies`, `clan_info_warLosses`, `clan_info_members`, 
	   `clan_info_type`,`clan_info_requiredTrophies`, `clan_info_isWarLogPublic`) 
	   VALUES ( NULL,?,?,?,?,?,?,?,?,?,?,?,? )";
};

##############################
# API_Clan
##############################
# set up clan search sql statement
sub get_clan_search {
	return "SELECT * 
	   FROM `API_Clan` 
	   WHERE `clan_tag` LIKE ?";
};

# set up clan all search sql statement
sub get_clan_all_search {
	return "Select *
		From `API_Clan`";
};

# set up clan insert sql statement
sub get_clan_insert {
	return "INSERT INTO `thebl962_jumi`.`API_Clan` (`clan_tag`, `clan_name`, `clan_description`, 
	   `clan_badgeUrls_small`, `clan_badgeUrls_medium`, `clan_badgeUrls_large`, `clan_location_id`, 
	   `clan_location_name`, `clan_location_countryCode`, `clan_warFrequency`) 
	   VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
};

# set up clan update sql statement
sub get_clan_update {
	return "UPDATE `thebl962_jumi`.`API_Clan` 
	  SET `clan_description` = ?,
		 `clan_badgeUrls_small` = ?,
		 `clan_badgeUrls_medium` = ?,
		 `clan_badgeUrls_large` = ?,
		 `clan_location_id` = ?,
		 `clan_location_name` = ?,
		 `clan_location_countryCode` = ?,
		 `clan_warFrequency` = ?
	  WHERE `API_Clan`.`clan_tag` = ?";
};

##############################
# API_Member
##############################
# set up member search sql statement
sub get_member_search {
	return "SELECT * 
	   FROM `API_Member` 
	   WHERE `member_tag` LIKE ? 
	   ORDER BY `member_current_timestamp` DESC 
	   LIMIT 1";
};

# set up timestamp search for last udpate sql statement
sub get_member_lastUpdate_search {
	return "SELECT `member_current_timestamp` 
 	 FROM  `API_Member` 
	 WHERE  `member_clan_tag` LIKE ? 
	 ORDER BY `member_current_timestamp` DESC 
	 LIMIT 1";
};

# set up time search sql statement
sub get_member_time_search {
	return "SELECT * 
 	 FROM  `API_Member` 
	 WHERE  `member_current_timestamp` LIKE ?";
};

# set up member insert sql statement
sub get_member_insert {
	return "INSERT INTO `thebl962_jumi`.`API_Member` (`member_id`, `member_tag`, 
	   `member_clan_tag`, `member_league_id`, `member_name`, `member_timestamp`, `member_current_timestamp`, `member_clanRank`, 
	   `member_previousClanRank`, `member_expLevel`, `member_role`, `member_townHallLevel`, `member_trophies`, 
	   `member_bestTrophies`, `member_warStars`, `member_attackWins`, `member_defenseWins`, 
	   `member_builderHallLevel`, `member_versusTrophies`, `member_bestVersusTrophies`, 
	   `member_versusBattleWinCount`, `member_versusBattleWins`, `member_donations`, 
	   `member_total_donations`, `member_donationsReceived`, `member_total_donationsReceived`) 
	   VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
};

# set up member insert sql statement
sub get_member_time_update {
	return "UPDATE `API_Member` 
		SET `member_current_timestamp` = ? 
		WHERE `API_Member`.`member_id` = ?";
};

##############################
# API_League
##############################
# set up league search sql statement
sub get_league_search {
	return "SELECT *
	   FROM `API_League`
	   WHERE `league_id` =?";
};

# set up league insert sql statement
sub get_league_insert {
	return "INSERT INTO `thebl962_jumi`.`API_League` (`league_id`, `league_name`, 
	   `league_iconUrls_tiny`, `league_iconUrls_small`, `league_iconUrls_medium`) 
	   VALUES (?, ?, ?, ?, ?)";
};

##############################
# API_Mem_AchievToMem
##############################
# set up member achievement search sql statement
sub get_achievToMem_group_search {
	return "SELECT `achievToMem_achiev_name`, `achievToMem_achiev_id`, `achiev_base`, 
	  	`achiev_order`, MAX(`achievToMem_value`) AS achievToMem_value 
	  	FROM `API_Mem_AchievToMem` AS ATM 
	  	INNER JOIN `API_Mem_Achiev`
	  		ON `achiev_id` LIKE `achievToMem_achiev_id` 
	  	WHERE `achievToMem_member_tag` LIKE ?
	  	GROUP BY `achievToMem_achiev_id`";
};

# set up member achievement insert sql statement
sub get_achievToMem_insert {
	return "INSERT INTO `thebl962_jumi`.`API_Mem_AchievToMem` (
	   	`achievToMem_id`, `achievToMem_member_tag`, `achievToMem_achiev_id`, `achievToMem_achiev_name`, 
		`achievToMem_timestamp`, `achievToMem_stars`, `achievToMem_value`, `achievToMem_target`, 
		`achievToMem_completionInfo`) 
	   	VALUES (NULL , ?, ?, ?, ?, ?, ?, ?, ?)";
};

##############################
# API_Mem_Achiev
##############################
# set up achievement insert sql statement
sub get_achiev_insert {
	return "INSERT INTO `thebl962_jumi`.`API_Mem_Achiev` (`achiev_name`, `achiev_info`, `achiev_base`) 
	   VALUES (?, ?, ?)";
};

# set up achievement search sql statement
sub get_achiev_search {
	return"SELECT * 
		FROM  `API_Mem_Achiev`
	   	ORDER BY `achiev_id` ASC";
};

##############################
# API_Mem_Banned
##############################
# set up banned search sql statement
sub get_banned_search {
	return "SELECT * 
		FROM `API_Mem_Banned`
		WHERE `banned_tag` LIKE ?";
};

sub get_banned_update {
	return "UPDATE `API_Mem_Banned`
		SET `banned_notify` = ?, 
			`banned_notified` = ?,
			`banned_notified_timestamp` = ?, 
			`banned_notes` = ? 
		WHERE `banned_tag` LIKE ?";
};

##############################
# API_Mem_TroopsToMem
##############################
# set up member troop search by member grouping on troop name sql statement
sub get_troopsToMem_group_search {
	return "SELECT `troopsToMem_troops_name`, `troopsToMem_troop_id`, `troops_base`, `troops_order`, 
	  MAX(`troopsToMem_level`) AS troopsToMem_level, MAX(`troopsToMem_warWeight`) AS troopsToMem_warWeight 
	  FROM `API_Mem_TroopsToMem` 
	  INNER JOIN `API_Mem_Troops`
	     ON `troops_id` LIKE `troopsToMem_troop_id` 
	  WHERE `troopsToMem_member_tag` LIKE ? 
	  GROUP BY `troopsToMem_troop_id`";
};

# set up member troop search
sub get_troopsToMem_search {
	return "SELECT * 
	  FROM `API_Mem_TroopsToMem`";
};

# set up member troop to get newest war weight for one member search
sub get_troopsToMem_mebWW_search {
	return "SELECT `troopsToMem_troops_name`, MAX(`troopsToMem_warWeight`) as troopsToMem_warWeight 
	 FROM  `API_Mem_TroopsToMem` 
	 WHERE  `troopsToMem_member_tag` LIKE  ? 
	 GROUP BY `troopsToMem_troops_name` 
	 ORDER BY  `troopsToMem_timestamp` DESC ";
};

# set up member troop insert sql statement
sub get_troopsToMem_insert {
	return "INSERT INTO `thebl962_jumi`.`API_Mem_TroopsToMem` (`troopsToMem_id`, 
	   `troopsToMem_member_tag`, `troopsToMem_troop_id`, `troopsToMem_troops_name`, 
	   `troopsToMem_timestamp`, `troopsToMem_level`, `troopsToMem_warWeight`) 
	   VALUES (NULL, ?, ?, ?, ?, ?, ?)";
};

# set up member troop warWeight update sql statement
sub get_troopsToMem_update {
	return "UPDATE  `thebl962_jumi`.`API_Mem_TroopsToMem`
	  SET `troopsToMem_warWeight` =  ?
	  WHERE `API_Mem_TroopsToMem`.`troopsToMem_id` = ?";
};

##############################
# API_Mem_Troops
##############################
# set up troop search sql statement
sub get_troops_search {
	return "SELECT * 
	   FROM `API_Mem_Troops`
	   ORDER BY `troops_id` ASC";
};

# set up troop insert sql statement
sub get_troops_insert {
	return "INSERT INTO `thebl962_jumi`.`API_Mem_Troops` (`troops_id`, `troops_name`, 
		`troops_base`, `troops_maxLevel`) 
	   VALUES (NULL, ?, ?, ?)";
};

# set up troop update sql statement
sub get_troops_update {
	return "UPDATE `thebl962_jumi`.`API_Mem_Troops` 
	  SET `troops_maxLevel` = ? 
	  WHERE `API_Mem_Troops`.`troops_name` = ?
	  	AND `API_Mem_Troops`.`troops_base` = ?";
};

##############################
# API_Mem_HerosToMem
##############################
# set up member hero search by member grouping on hero name sql statement
sub get_heroToMem_group_search {
	return "SELECT `heroToMem_heros_name`, `heroToMem_heros_id`, `hero_base`, `hero_order`, 
	  MAX(`heroToMem_level`) AS heroToMem_level, MAX(`heroToMem_warWeight`) AS heroToMem_warWeight
	  FROM `API_Mem_HerosToMem` 
	  INNER JOIN `API_Mem_Heros`
	     ON `hero_id` LIKE `heroToMem_heros_id` 
	  WHERE `heroToMem_member_tag` LIKE ? 
	  GROUP BY `heroToMem_heros_id`";
};

# set up member hero search
sub get_herosToMem_search {
	return "SELECT * 
	  FROM `API_Mem_HerosToMem`";
};

# set up member hero to get newest war weight for one member search
sub get_heroToMem_mebWW_search {
	return "SELECT `heroToMem_heros_name`, MAX(`heroToMem_warWeight`) as heroToMem_warWeight 
	 FROM  `API_Mem_HerosToMem` 
	 WHERE  `heroToMem_member_tag` LIKE  ? 
	 GROUP BY `heroToMem_heros_name` 
	 ORDER BY  `heroToMem_timestamp` DESC ";
};

# set up member hero insert sql statement
sub get_heroToMem_insert {
	return "INSERT INTO `thebl962_jumi`.`API_Mem_HerosToMem` (`heroToMem_id`, 
	   `heroToMem_member_tag`, `heroToMem_heros_id`, `heroToMem_heros_name`, 
	   `heroToMem_timestamp`, `heroToMem_level`, `heroToMem_warWeight_off`, 
	   `heroToMem_warWeight_def`) 
	   VALUES (NULL , ?, ?, ?, ?, ?, ?, ?)";
};

# set up member hero warWeight update sql statement
sub get_herosToMem_update {
	return "UPDATE  `thebl962_jumi`.`API_Mem_HerosToMem`
	  SET `heroToMem_warWeight` =  ?
	  WHERE `API_Mem_HerosToMem`.`heroToMem_id` = ?";
};

##############################
# API_Mem_Heros
##############################
# set up hero search sql statement
sub get_hero_search {
	return "SELECT * 
	   FROM `API_Mem_Heros`
	   ORDER BY `hero_id` ASC";
};

# set up hero insert sql statement
sub get_hero_insert {
	return "INSERT INTO `thebl962_jumi`.`API_Mem_Heros` (`hero_name`, `hero_maxLevel`) 
	   VALUES (?, ?)";
};

# set up heros update sql statement
sub get_hero_update {
	return "UPDATE `thebl962_jumi`.`API_Mem_Heros` 
	  SET `hero_maxLevel` = ? 
	  WHERE `API_Mem_Heros`.`hero_name` = ?";
};

##############################
# API_Mem_SpellsToMem
##############################
# set up member spell search sql statement
sub get_spellToMem_group_search {
	return "SELECT `spellsToMem_spells_name`,  
	  MAX(`spellsToMem_level`) AS spellsToMem_level 
	  FROM `API_Mem_SpellsToMem` 
	  WHERE `spellsToMem_member_tag` LIKE ? 
	  GROUP BY `spellsToMem_spells_name`";
};

# set up member spell search
sub get_spellsToMem_search {
	return "SELECT * 
	  FROM `API_Mem_SpellsToMem`";
};

# set up member spell to get newest war weight for one member search
sub get_spellToMem_mebWW_search {
	return "SELECT `spellsToMem_spells_name`, MAX(`spellsToMem_warWeight`) as spellsToMem_warWeight 
	 FROM  `API_Mem_SpellsToMem` 
	 WHERE  `spellsToMem_member_tag` LIKE  ? 
	 GROUP BY `spellsToMem_spells_name` 
	 ORDER BY  `spellsToMem_timestamp` DESC ";
};

# set up member spell insert sql statement
sub get_spellToMem_insert {
	return "INSERT INTO `thebl962_jumi`.`API_Mem_SpellsToMem` (`spellsToMem_id`, 
	   `spellsToMem_member_tag`, `spellsToMem_spells_id`, `spellsToMem_spells_name`, 
	   `spellsToMem_timestamp`, `spellsToMem_level`, `spellsToMem_warWeight` ) 
	   VALUES (NULL , ?, ?, ?, ?, ?, ?)";
};

# set up member spell warWeight update sql statement
sub get_spellsToMem_update {
	return "UPDATE  `thebl962_jumi`.`API_Mem_SpellsToMem`
	  SET `spellsToMem_warWeight` =  ?
	  WHERE `API_Mem_SpellsToMem`.`spellsToMem_id` = ?";
};


##############################
# API_Mem_Spells
##############################
# set up spell search sql statement
sub get_spell_search {
	return "SELECT * 
	   FROM `API_Mem_Spells`
	   ORDER BY `spells_id` ASC";
};

# set up spell insert sql statement
sub get_spell_insert {
	return "INSERT INTO `thebl962_jumi`.`API_Mem_Spells` (`spells_name`, `spells_maxLevel`) 
	   VALUES (?, ?)";
};

# set up spell update sql statement
sub get_spell_update {
	return "UPDATE `thebl962_jumi`.`API_Mem_Spells` 
	  SET `spells_maxLevel` = ? 
	  WHERE `API_Mem_Spells`.`spells_name` = ?";
};


##############################
# API_WarWeight
##############################
#

# set up war weight search by name and level sql statement
sub get_ww_specific_search {
	return "SELECT * 
		FROM  `API_WarWeight` 
		WHERE  `ww_name` LIKE  ? 
		 AND  `ww_level` = ?";
};

# set up for max level for th search sql for def bldg statement
sub get_th_defBldgs_search {
	return "SELECT `ww_name`, `ww_build_id`, MAX(`ww_level`) ww_level 
		FROM `API_WarWeight` 
		WHERE `ww_build_id` IS NOT NULL AND `ww_thLevel` <= ? 
		GROUP BY `ww_build_id` ";
};

# set up for max level for th search sql for trap statement
sub get_th_traps_search {
	return "SELECT `ww_name`, `ww_trap_id`, MAX(`ww_level`) ww_level 
		FROM `API_WarWeight` 
		WHERE `ww_trap_id` IS NOT NULL AND `ww_thLevel` <= ? 
		GROUP BY `ww_trap_id` ";
};

# set up for max level for th search sql for misc bldg statement
sub get_th_miscs_search {
	return "SELECT `ww_name`, `ww_misc_id`, MAX(`ww_level`) ww_level 
		FROM `API_WarWeight` 
		WHERE `ww_misc_id` IS NOT NULL AND `ww_thLevel` <= ? 
		GROUP BY `ww_misc_id` ";
};

# set up for max level for th search sql for nonWt bldg statement
sub get_th_nonWt_search {
	return "SELECT `ww_name`, `ww_nonWt_id`, MAX(`ww_level`) ww_level 
		FROM `API_WarWeight` 
		WHERE `ww_nonWt_id` IS NOT NULL AND `ww_thLevel` <= ? 
		GROUP BY `ww_nonWt_id` ";
};

# set up war weight search sql for troops statement
sub get_ww_troops_search {
	return "SELECT * 
		FROM  `API_WarWeight` 
		WHERE  `ww_troops_id` IS NOT NULL ";
};

# set up for max level for th search sql for troops statement
sub get_th_troops_search {
	return "SELECT `ww_name`, `ww_troops_id`, MAX(`ww_level`) ww_level 
		FROM `API_WarWeight` 
		WHERE `ww_troops_id` IS NOT NULL AND `ww_thLevel` <= ? 
		GROUP BY `ww_troops_id` ";
};

# set up war weight search sql for heros statement
sub get_ww_heros_search {
	return "SELECT * 
		FROM  `API_WarWeight` 
		WHERE  `ww_heros_id` IS NOT NULL ";
};

# set up for max level for th search sql for heros statement
sub get_th_heros_search {
	return "SELECT `ww_name`, `ww_heros_id`, MAX(`ww_level`) ww_level 
		FROM `API_WarWeight` 
		WHERE `ww_heros_id` IS NOT NULL AND `ww_thLevel` <= ? 
		GROUP BY `ww_heros_id` ";
};

# set up war weight search sql for spells statement
sub get_ww_spells_search {
	return "SELECT * 
		FROM  `API_WarWeight` 
		WHERE  `ww_spells_id` IS NOT NULL ";
};

# set up for max level for th search sql for spells statement
sub get_th_spells_search {
	return "SELECT `ww_name`, `ww_spells_id`, MAX(`ww_level`) ww_level 
		FROM `API_WarWeight` 
		WHERE `ww_spells_id` IS NOT NULL AND `ww_thLevel` <= ? 
		GROUP BY `ww_spells_id` ";
};

##############################
# API_Mem_Defense
##############################
# set up member def search
sub get_defense_search {
	return "SELECT * 
	  FROM `API_Mem_Defense`";
};

##############################
# API_Mem_DefToMem
##############################
# set up member def bldg search
sub get_defToMem_search {
	return "SELECT * 
	  FROM `API_Mem_DefToMem`";
};

# set up member def bldg to get newest war weight for one member search
sub get_defToMem_mebWW_search {
	return "SELECT `defToMem_defense_name`, MAX(`defToMem_warWeight`) as defToMem_warWeight 
	 FROM  `API_Mem_DefToMem` 
	 WHERE  `defToMem_member_tag` LIKE  ? 
	 GROUP BY `defToMem_defense_name` 
	 ORDER BY  `defToMem_timestamp` DESC ";
};

# set up member def bldg warWeight update sql statement
sub get_defToMem_update {
	return "UPDATE  `thebl962_jumi`.`API_Mem_DefToMem`
	  SET `defToMem_warWeight` =  ?
	  WHERE `API_Mem_DefToMem`.`defToMem_id` = ?";
};

# set up member newest def bldg for specific def bldg
sub get_defToMem_defBldg_search {
	return "SELECT * 
	  FROM `API_Mem_DefToMem`  
	  WHERE `defToMem_member_tag` LIKE ? AND `defToMem_defense_id` = ?  
	  ORDER BY `defToMem_id` DESC 
	  LIMIT 1";
};

##############################
# API_Mem_Traps
##############################
# set up member trap search
sub get_trap_search {
	return "SELECT * 
	  FROM `API_Mem_Traps`";
};

##############################
# API_Mem_TrapToMem
##############################
# set up member spell search
sub get_trapToMem_search {
	return "SELECT * 
	  FROM `API_Mem_TrapToMem`";
};

# set up member trap to get newest war weight for one member search
sub get_trapToMem_mebWW_search {
	return "SELECT `trapToMem_trap_name`, MAX(`trapToMem_warWeight`) as trapToMem_warWeight 
	 FROM  `API_Mem_TrapToMem` 
	 WHERE  `trapToMem_member_tag` LIKE  ? 
	 GROUP BY `trapToMem_trap_name` 
	 ORDER BY  `trapToMem_timestamp` DESC ";
};

# set up member spell warWeight update sql statement
sub get_trapToMem_update {
	return "UPDATE  `thebl962_jumi`.`API_Mem_TrapToMem`
	  SET `trapToMem_warWeight` =  ?
	  WHERE `API_Mem_TrapToMem`.`trapToMem_id` = ?";
};

# set up member newest trap for specific trap
sub get_trapToMem_trap_search {
	return "SELECT * 
	  FROM `API_Mem_TrapToMem`  
	  WHERE `trapToMem_member_tag` LIKE ? AND `trapToMem_trap_id` = ?  
	  ORDER BY `trapToMem_id` DESC 
	  LIMIT 1";
};

##############################
# API_Mem_Misc
##############################
# set up member misc bldg search
sub get_misc_search {
	return "SELECT * 
	  FROM `API_Mem_Misc`";
};

##############################
# API_Mem_MiscToMem
##############################
# set up member misc bldg search
sub get_miscToMem_search {
	return "SELECT * 
	  FROM `API_Mem_MiscToMem`";
};

# set up member misc bldg to get newest war weight for one member search
sub get_miscToMem_mebWW_search {
	return "SELECT `miscToMem_misc_name`, MAX(`miscToMem_warWeight`) as miscToMem_warWeight 
	 FROM  `API_Mem_MiscToMem` 
	 WHERE  `miscToMem_member_tag` LIKE  ? 
	 GROUP BY `miscToMem_misc_name` 
	 ORDER BY  `miscToMem_timestamp` DESC ";
};

# set up member spell warWeight update sql statement
sub get_miscToMem_update {
	return "UPDATE  `thebl962_jumi`.`API_Mem_MiscToMem`
	  SET `miscToMem_warWeight` =  ?
	  WHERE `API_Mem_MiscToMem`.`miscToMem_id` = ?";
};

# set up member newest misc bldg for specific trap
sub get_miscToMem_misc_search {
	return "SELECT * 
	  FROM `API_Mem_MiscToMem`  
	  WHERE `miscToMem_member_tag` LIKE ? AND `miscToMem_misc_id` = ?  
	  ORDER BY `miscToMem_id` DESC 
	  LIMIT 1";
};

##############################
# API_Mem_NonWtToMem
##############################
# set up member newest nonWt bldg for specific nonWt bldg
sub get_nonWtToMem_nonWt_search {
	return "SELECT * 
	  FROM `API_Mem_NonWtToMem`  
	  WHERE `nonWtToMem_member_tag` LIKE ? AND `nonWtToMem_nonWt_id` = ?  
	  ORDER BY `nonWtToMem_id` DESC 
	  LIMIT 1";
};

return 1;
