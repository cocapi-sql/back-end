#! /usr/bin/perl

#############################################
#           API_Mem_War_Update.pl           #
#                                           #
#           Used to to total up             #
#         all war attacks for users         #
#                                           #
#              by Gary Douglas              #
#               August 6, 2017              #
#        http://www.the-blacklist.ca        #
#############################################


use strict;
use warnings;
use Data::Dumper;
use Time::Piece;
use DBI;

# include files
use lib '/home/thebl962/public_html/bin';
require 'API_SQL_Statements.pl';
require 'config_02.pl';
# require 'config.pl';

# SQL Variables
my $sql_platform = get_sql_platform ();
my $dbname =  get_dbname ();
my $servername = get_servername ();
my $username = get_username();
my $password = get_password();
my $dsn = "DBI:" . get_sql_platform() . ":" . get_dbname() . ":" . get_servername();

# load clan tags into array, Note leave the # off
my @clan_tags = get_clan_list();

# set up global variables
my %counter = ();
my @homeClan_tag = ();
my %warlog_sql = ();
my %member_sql = ();

# get debug and test
my $debug = get_debug();
my $test = get_test();

# check for cli arguments
while ( @ARGV ) {
	if ( my $Parameter eq '-debug' ) {
		$debug = 1;
	} elsif ( $Parameter eq '-test' ) {
		$test = 1;
	}
} 

# display debug and test flags
if ( $debug == 1 ) { print "DEBUG: Debug (Verbose Logging) is ON\n"; }
if ( $test == 1 ) { print "DEBUG: Test (No SQL Insert) is ON\n"; }

# load clan tags into array, Note leave the # off
@clan_tags = get_clan_list();

# connect to MySQL database
my %attr = ( PrintError=>0,RaiseError=>1,PrintError=>0,ShowErrorStatement=>1 );
my $dbh = DBI->connect ( $dsn, $username, $password, \%attr );

# set up sql statements

my $stmt_clan_search= $dbh->prepare ( get_clan_all_search() )
					or die "Unable to prepare stmt_clan_search.  " . $dbh->errstr;
my $stmt_walog_battle_comp_search= $dbh->prepare ( get_walog_battle_comp_search() )
					or die "Unable to prepare stmt_walog_battle_comp_search.  " . $dbh->errstr;
my $stmt_memWW_time_search= $dbh->prepare ( get_memWW_time_search() )
					or die "Unable to prepare stmt_memWW_time_search.  " . $dbh->errstr;
my $stmt_warlog_member_update = $dbh->prepare ( get_warlog_member_update() )
					or die "Unable to prepare stmt_warlog_member_clan_search.  " . $dbh->errstr;
my $stmt_warlog_member_count_search = $dbh->prepare ( get_warlog_member_count_search() )
					or die "Unable to prepare stmt_warlog_member_count_search.  " . $dbh->errstr;
my $stmt_memberWars_update = $dbh->prepare ( get_memberWars_update() )
					or die "Unable to prepare stmt_memberWars_update.  " . $dbh->errstr;

# get all home clans from sql
if ( $debug ) { print "DEBUG: Started: Pulling home clans.\n"; }
$stmt_clan_search->execute ();

while ( my $clan_row = $stmt_clan_search->fetchrow_hashref ( ) ) {
	my $temp_tag = $clan_row->{'clan_tag'};
	push @homeClan_tag, $temp_tag;
	$counter{'SQL search successful: API_Clan'}++;	
}
if ( $debug ) { print "DEBUG: Finished: Pulling home clans.\n"; }

# get all battles from sql
if ( $debug ) { print "DEBUG: Started: Pulling battle data.\n"; }
$stmt_walog_battle_comp_search->execute ();	

while ( my $battle_row = $stmt_walog_battle_comp_search->fetchrow_hashref ( ) ) {
	my $battle_warlog_id = $battle_row->{'battle_warlog_id'};
	my $warlog_startTime = $battle_row->{'warlog_startTime'};
	my $battle_order = $battle_row->{'battle_order'};
	my $att_tag = $battle_row->{'att_tag'};
	my $att_id = $battle_row->{'att_id'};
	my $att_clan_tag = $battle_row->{'att_clan_tag'};
	my $att_name = $battle_row->{'att_name'};
	my $att_townhallLevel = $battle_row->{'att_townhallLevel'};
	my $att_mapPosition = $battle_row->{'att_mapPosition'};
	my $def_tag = $battle_row->{'def_tag'};
	my $def_id = $battle_row->{'def_id'};
	my $def_clan_tag = $battle_row->{'def_clan_tag'};
	my $def_name = $battle_row->{'def_name'};
	my $def_townhallLevel = $battle_row->{'def_townhallLevel'};
	my $def_mapPosition = $battle_row->{'def_mapPosition'};
	my $battle_stars = $battle_row->{'battle_stars'};
	my $battle_newStars = $battle_row->{'battle_newStars'};
	my $battle_destructionPercentage = $battle_row->{'battle_destructionPercentage'};
	my $battle_clean_flag = $battle_row->{'battle_clean_flag'};
	my $battle_loot_flag = $battle_row->{'battle_loot_flag'};
	my $battle_earlyLoot_flag = $battle_row->{'battle_earlyLoot_flag'};
	my $battle_noCC_flag = $battle_row->{'battle_noCC_flag'};
	$counter{'SQL search successful: API_WarLog_Battle'}++;
	
	# update attacker warlog data
	$warlog_sql{$att_id}{'member_warlog_id'} = $battle_warlog_id;
	$warlog_sql{$att_id}{'member_tag'} = $att_tag;
	$warlog_sql{$att_id}{'member_attacks_used'}++;
	$warlog_sql{$att_id}{'member_attack_stars'} += $battle_stars;
	$warlog_sql{$att_id}{'member_attack_newStars'} += $battle_newStars;
	$warlog_sql{$att_id}{'member_attack_destruction'} += $battle_destructionPercentage;
	$warlog_sql{$att_id}{'member_clean_cnt'} += $battle_clean_flag;
	$warlog_sql{$att_id}{'member_loot_cnt'} += $battle_loot_flag;
	$warlog_sql{$att_id}{'member_earlyloot_cnt'} += $battle_earlyLoot_flag;
	$warlog_sql{$att_id}{'member_noCC_cnt'} += $battle_noCC_flag;

	# update defender warlog data
	$warlog_sql{$def_id}{'member_defense_used'}++;
	$warlog_sql{$def_id}{'member_defense_stars'} += $battle_stars;
	$warlog_sql{$def_id}{'member_defense_newStars'} += $battle_newStars;
	if (( $battle_newStars > 0 )||(!defined( $warlog_sql{$def_id}{'member_defense_destruction'} ))||
			(( $battle_newStars == 0 )&&( $warlog_sql{$def_id}{'member_defense_destruction'} < $battle_destructionPercentage ))) {
		$warlog_sql{$def_id}{'member_defense_destruction'} = $battle_destructionPercentage;	
	}
	# Check if home clan was attacker
	my $homeClan_tag = 0;
	foreach my $temp_tag (@homeClan_tag) {
  		if ( $temp_tag eq $att_clan_tag ) {
			$homeClan_tag = 1;
		}
	}

	if ( $homeClan_tag == 1 ) {
		# attacker was in home clan, update members attack data
		$member_sql{$att_tag}{'member_attack_used'}++;
		$member_sql{$att_tag}{'member_attack_stars'} += $battle_stars;
		if ( $battle_stars == 3 ) {
			$member_sql{$att_tag}{'member_attack_3Stars'}++;
		} elsif ( $battle_stars == 2 ) {
			$member_sql{$att_tag}{'member_attack_2Stars'}++;
		} elsif ( $battle_stars == 1 ) {
			$member_sql{$att_tag}{'member_attack_1Stars'}++;
		} else {
			$member_sql{$att_tag}{'member_attack_1Stars'}++;
		}
		$member_sql{$att_tag}{'member_attack_newStars'} += $battle_newStars;
		if ( $battle_newStars == 3 ) {
			$member_sql{$att_tag}{'member_attack_3NewStars'}++;
		} elsif ( $battle_newStars == 2 ) {
			$member_sql{$att_tag}{'member_attack_2NewStars'}++;
		} elsif ( $battle_newStars == 1 ) {
			$member_sql{$att_tag}{'member_attack_1NewStars'}++;
		} else {
			$member_sql{$att_tag}{'member_attack_destruction'}++;
		}
		$member_sql{$att_tag}{'member_attack_destruction'} += $battle_destructionPercentage;
		$member_sql{$att_tag}{'member_clean_cnt'} += $battle_clean_flag;
		$member_sql{$att_tag}{'member_loot_cnt'} += $battle_loot_flag;
		$member_sql{$att_tag}{'member_earlyloot_cnt'} += $battle_earlyLoot_flag;
		$member_sql{$att_tag}{'member_noCC_cnt'} += $battle_noCC_flag;

		# check if we already have war weight for member
		# if ( !defined ( $warlog_sql{$att_id}{'memWW_off'} )) {
		# 	# get members war weight at start of war
		# 	my $memWW_off = 0;
		# 	$stmt_memWW_time_search->execute ( $att_tag, $warlog_startTime );

		# 	while ( my $weight_row = $stmt_memWW_time_search->fetchrow_hashref ( ) ) {
		# 		my $temp_memWW_hero = $weight_row->{'memWW_hero'};
		# 		my $temp_memWW_troop = $weight_row->{'memWW_troop'};
		# 		my $temp_memWW_spell = $weight_row->{'memWW_spell'};
		# 		$memWW_off = $temp_memWW_hero + $temp_memWW_troop + $temp_memWW_spell;
		# 		$counter{'SQL search successful: API_Mem_WarWeight'}++;	
		# 	}
		# 	$warlog_sql{$att_id}{'memWW_off'} = $memWW_off;
		# }

	} else {
		# defender was in home clan, update members data
		$member_sql{$def_tag}{'member_defense_used'}++;
		$member_sql{$def_tag}{'member_defense_stars'} += $battle_stars;
		if ( $battle_stars == 3 ) {
			$member_sql{$def_tag}{'member_defense_3Stars'}++;
		} elsif ( $battle_stars == 2 ) {
			$member_sql{$def_tag}{'member_defense_2Stars'}++;
		} elsif ( $battle_stars == 1 ) {
			$member_sql{$def_tag}{'member_defense_1Stars'}++;
		} else {
			$member_sql{$def_tag}{'member_defense_0Stars'}++;
		}
		$member_sql{$def_tag}{'member_defense_newStars'} += $battle_newStars;
		if ( $battle_newStars == 3 ) {
			$member_sql{$def_tag}{'member_defense_3NewStars'}++;
		} elsif ( $battle_newStars == 2 ) {
			$member_sql{$def_tag}{'member_defense_1NewStars'}++;
		} elsif ( $battle_newStars == 1 ) {
			$member_sql{$def_tag}{'member_defense_2NewStars'}++;
		} else {
			$member_sql{$def_tag}{'member_defense_0NewStars'}++;
		}
		$member_sql{$def_tag}{'member_defense_destruction'} += $battle_destructionPercentage;
		# $warlog_sql{$att_id}{'memWW_off'} = 0;
	}
}
if ( $debug ) { print "DEBUG: Finished: Pulling battle data.\n"; }

# Iterate threw warlog_sql
if ( $debug ) { print "DEBUG: Started: Updating warlog for members.\n"; }
foreach my $warlog_key ( keys %warlog_sql ) {
	if ( $test == 0 ) {
		$stmt_warlog_member_update->execute ( $warlog_sql{$warlog_key}{'member_attacks_used'},
												$warlog_sql{$warlog_key}{'member_attack_stars'},
												$warlog_sql{$warlog_key}{'member_attack_newStars'},
												$warlog_sql{$warlog_key}{'member_attack_destruction'},
												$warlog_sql{$warlog_key}{'member_defense_used'},
												$warlog_sql{$warlog_key}{'member_defense_stars'},
												$warlog_sql{$warlog_key}{'member_defense_newStars'},
												$warlog_sql{$warlog_key}{'member_defense_destruction'},
												$warlog_sql{$warlog_key}{'member_clean_cnt'},
												$warlog_sql{$warlog_key}{'member_loot_cnt'},
												$warlog_sql{$warlog_key}{'member_earlyloot_cnt'},
												$warlog_sql{$warlog_key}{'member_noCC_cnt'},
												$warlog_sql{$warlog_key}{'member_warlog_id'},
												$warlog_sql{$warlog_key}{'member_tag'} )
				or die "Unable to execute stmt_warlog_member_update.  " . $stmt_warlog_member_update->errstr;
		# if ( $debug ) { print "DEBUG:   WarLog for $clan_json{$clan_tag}{'name'} updated successfully.\n"; }
		$counter{'SQL update successful: API_WarLog_Member'}++;
	}
}
if ( $debug ) { print "DEBUG: Finished: Updating warlog for members.\n"; }

# Iterate threw member_sql
if ( $debug ) { print "DEBUG: Started: Updating wars for members.\n"; }
foreach my $member_key ( keys %member_sql ) {
	# get count of war's for member
	my $member_wars;
	$stmt_warlog_member_count_search->execute ( $member_key ) 
				or die "Unable to execute stmt_warlog_member_count_search.  " . $stmt_warlog_member_count_search->errstr;

	while ( my $warcnt_row = $stmt_warlog_member_count_search->fetchrow_hashref ( ) ) {
		$member_wars = $warcnt_row->{'member_wars'};
	}

	if ( $test == 0 ) {
		$stmt_memberWars_update->execute ( $member_wars,
												$member_sql{$member_key}{'member_attack_used'},
												$member_sql{$member_key}{'member_attack_stars'},
												$member_sql{$member_key}{'member_attack_newStars'},
												$member_sql{$member_key}{'member_attack_destruction'},
												$member_sql{$member_key}{'member_attack_3Stars'},
												$member_sql{$member_key}{'member_attack_2Stars'},
												$member_sql{$member_key}{'member_attack_1Stars'},
												$member_sql{$member_key}{'member_attack_0Stars'},
												$member_sql{$member_key}{'member_attack_3NewStars'},
												$member_sql{$member_key}{'member_attack_2NewStars'},
												$member_sql{$member_key}{'member_attack_1NewStars'},
												$member_sql{$member_key}{'member_attack_0NewStars'},
												$member_sql{$member_key}{'member_defense_used'},
												$member_sql{$member_key}{'member_defense_stars'},
												$member_sql{$member_key}{'member_defense_newStars'},
												$member_sql{$member_key}{'member_defense_destruction'},
												$member_sql{$member_key}{'member_defense_3Stars'},
												$member_sql{$member_key}{'member_defense_2Stars'},
												$member_sql{$member_key}{'member_defense_1Stars'},
												$member_sql{$member_key}{'member_defense_0Stars'},
												$member_sql{$member_key}{'member_defense_3NewStars'},
												$member_sql{$member_key}{'member_defense_2NewStars'},
												$member_sql{$member_key}{'member_defense_1NewStars'},
												$member_sql{$member_key}{'member_defense_0NewStars'},
												$member_sql{$member_key}{'member_clean_cnt'},
												$member_sql{$member_key}{'member_loot_cnt'},
												$member_sql{$member_key}{'member_earlyloot_cnt'},
												$member_sql{$member_key}{'member_noCC_cnt'},
												$member_key )
				or die "Unable to execute stmt_memberWars_update.  " . $stmt_memberWars_update->errstr;

		$counter{'SQL update successful: API_Mem_Wars'}++;
	}
}
if ( $debug ) { print "DEBUG: Started: Updating wars for members.\n"; }

#  print Dumper ( %member_sql );


# clean up sql statements
$stmt_clan_search->finish();
$stmt_walog_battle_comp_search->finish();
$stmt_memWW_time_search->finish();
$stmt_warlog_member_update->finish();
$stmt_warlog_member_count_search->finish();
$stmt_memberWars_update->finish();

# print Dumper(%warlog_sql) . "\n";
	
# clean up and disconnect from the MySQL database
$dbh->disconnect();
	
# display counter
print "\n";
print "Summary\n";
print "-" x 50 . " " . "-" x 5 . "\n";
foreach my $key ( sort keys %counter ) {
	printf( "%-50s %5d\n", $key, $counter{$key} );
}
