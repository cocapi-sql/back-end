#! /usr/bin/perl

#############################################
#                  COC API                  #
#                                           #
#         Used to pull update war           #
#           weight information              #
#                                           #
#              by Gary Douglas              #
#                May 19, 2017               #
#        http://www.the-blacklist.ca        #
#############################################


use strict;
use warnings;
use Data::Dumper;
use Time::Piece;
use DBI;

# include files
use lib '/home/thebl962/public_html/bin';
require 'API_SQL_Statements.pl';
require 'config_02.pl';
# require 'config.pl';

# SQL Variables
my $sql_platform = get_sql_platform ();
my $dbname =  get_dbname ();
my $servername = get_servername ();
my $username = get_username();
my $password = get_password();
my $dsn = "DBI:" . get_sql_platform() . ":" . get_dbname() . ":" . get_servername();

# load clan tags into array, Note leave the # off
my @clan_tags = get_clan_list();

# set up global variables
my %counter = ();
our $clan_tag;
my %warWeight = ();
my $lastUpdate;

# get debug and test
my $debug = get_debug();
my $test = get_test();

# check for cli arguments
while ( @ARGV ) {
	if ( my $Parameter eq '-debug' ) {
		$debug = 1;
	} elsif ( $Parameter eq '-test' ) {
		$test = 1;
	}
} 

# display debug and test flags
if ( $debug == 1 ) { print "DEBUG: Debug (Verbose Logging) is ON\n"; }
if ( $test == 1 ) { print "DEBUG: Test (No SQL Insert) is ON\n"; }

# load clan tags into array, Note leave the # off
@clan_tags = get_clan_list();

# Get Greenwich time zone and convert to sql date-time stamp
my ($S, $M, $H, $d, $m, $Y) = gmtime(time);
$m += 1;
$Y += 1900;
my $time = sprintf("%04d-%02d-%02d %02d:%02d:%02d", $Y, $m, $d, $H, $M, $S);

# connect to MySQL database
my %attr = ( PrintError=>0,RaiseError=>1,PrintError=>0,ShowErrorStatement=>1 );
my $dbh = DBI->connect ( $dsn, $username, $password, \%attr );

# set up sql statements
my $stmt_member_lastUpdate_search = $dbh->prepare ( get_member_lastUpdate_search() )
					or die "Unable to prepare stmt_member_lastUpdate_search.  " . $dbh->errstr;
my $stmt_member_time_search = $dbh->prepare ( get_member_time_search() )
					or die "Unable to prepare stmt_member_time_search.  " . $dbh->errstr;

# update war weight subgroups
update_defBldg_data();
update_trap_data();
update_miscBldg_data();
update_heros_data();
update_troops_data();						
update_spells_data();						

# iterate threw clans
foreach $clan_tag ( @clan_tags ){
	if ( $debug ) { print "DEBUG: Start working on $clan_tag clan's data.\n"; }

	# pull last update for clan from sql
	$stmt_member_lastUpdate_search->execute ( '#' . $clan_tag )
							or die "Unable to execute stmt_member_lastUpdate_search.  " . $stmt_member_lastUpdate_search->errstr;
	
	# get the results
	while ( my $row = $stmt_member_lastUpdate_search->fetchrow_hashref ( ) ) {
		$lastUpdate = $row->{'member_timestamp'};
	}

	# pull all members from sql at last update
	$stmt_member_time_search->execute ( $lastUpdate )
							or die "Unable to execute stmt_member_time_search.  " . $stmt_member_time_search->errstr;
	
	if ( $debug ) { print "DEBUG:    Start getting and updating war weights for all members.\n"; }
	# get the results
	while ( my $row = $stmt_member_time_search->fetchrow_hashref ( ) ) {
		my $member_tag = $row->{'member_tag'};
		my $member_th = $row->{'member_townHallLevel'};
		
		# update war weight for member
		update_ww( $member_tag, $member_th );
	}
	if ( $debug ) { print "DEBUG:    Finish getting and updating war weights for all members.\n"; }

	if ( $debug ) { print "DEBUG: Finish working on $clan_tag clan's data.\n"; }
}


# clean up sql statements
$stmt_member_lastUpdate_search->finish();
$stmt_member_time_search->finish();
	
# clean up and disconnect from the MySQL database
$dbh->disconnect();
	
# display counter
print "\n";
print "Summary\n";
print "-" x 50 . " " . "-" x 5 . "\n";
foreach my $key ( sort keys %counter ) {
	printf( "%-50s %5d\n", $key, $counter{$key} );
}

# print Dumper(%warWeight) . "\n";

##############################
# Get update war weight
##############################
sub update_ww {
	# get input variables
	my $member_tag = $_[0];
	my $member_th = $_[1];

	# local variables
	my %warWeight = ();

	# set up sql statements
	my $stmt_wwTH_search = $dbh->prepare ( get_wwTH_search() )
						or die "Unable to prepare stmt_wwTH_search.  " . $dbh->errstr;
	my $stmt_memWW_search = $dbh->prepare ( get_memWW_search() )
						or die "Unable to prepare stmt_memWW_search.  " . $dbh->errstr;
	my $stmt_memWW_insert = $dbh->prepare ( get_memWW_insert() )
						or die "Unable to prepare stmt_memWW_insert.  " . $dbh->errstr;

	# get war weight's for members
	$warWeight{'defBldg'} = get_defBldg_ww( $member_tag );
	$warWeight{'traps'} = get_trap_ww( $member_tag );
	$warWeight{'miscBldg'} = get_miscBldg_ww( $member_tag );
	$warWeight{'heros'} = get_heros_ww( $member_tag );
	$warWeight{'troops'} = get_troops_ww( $member_tag );
	$warWeight{'spells'} = get_spells_ww( $member_tag );
	
	# set incomplete flag
	if (( $warWeight{'defBldg'} == 0 )&&
		( $warWeight{'traps'} == 0 )&&
		( $warWeight{'miscBldg'} == 0 )) {
		$warWeight{'incFlag'} = 0;	
	} else {
		$warWeight{'incFlag'} = 1;	
	}
	
	# total war weight
	$warWeight{'total'} = $warWeight{'defBldg'} + $warWeight{'traps'} +
		$warWeight{'miscBldg'} + $warWeight{'heros'} +
		$warWeight{'troops'} + $warWeight{'spells'};
	
	# get war weight values for TH level
	# pull all members from sql at last update
	$stmt_wwTH_search->execute ( $member_th )
							or die "Unable to execute stmt_wwTH_search.  " . $stmt_wwTH_search->errstr;
	
	# get the results
	while ( my $row = $stmt_wwTH_search->fetchrow_hashref ( ) ) {
		$warWeight{'median'} += $row->{'WarWeight_Median'};	
		$warWeight{'max'} += $row->{'WarWeight_Rounded'};	
		$warWeight{'min'} += $row->{'WarWeight_Min'};	
		$counter{'SQL searched successful: API_WarWeight_TH'}++;	
	}
	
	# check for incomplete flag
	if ( $warWeight{'incFlag'} ) {
		$warWeight{'unadjusted'} = ( int ( $warWeight{'total'} / 2000 )) * 2000;
		if (( $warWeight{'unadjusted'} + 10000 < $warWeight{'median'} )&&
					( $warWeight{'total'} - 6 < $warWeight{'min'} ))  {
			$warWeight{'penalty'} = $warWeight{'median'} - $warWeight{'unadjusted'};
			if ( $warWeight{'penalty'} > 29999 ) {
				$warWeight{'penalty'} = 29999;
			}		
			$warWeight{'adjusted'} = $warWeight{'unadjusted'} + $warWeight{'penalty'};		
		} else {
			$warWeight{'adjusted'} = $warWeight{'unadjusted'};		
			$warWeight{'penalty'} = 0;		
		}
	} else {
		$warWeight{'unadjusted'} = 0;
		$warWeight{'penalty'} = 0;
		$warWeight{'adjusted'} = $warWeight{'median'};
	}
	
	# pull members current war weight from sql
	$stmt_memWW_search->execute ( $member_tag )
							or die "Unable to execute stmt_memWW_search.  " . $stmt_memWW_search->errstr;
	
	# get the results
	while ( my $row = $stmt_memWW_search->fetchrow_hashref ( ) ) {
		$warWeight{'adjusted_sql'} = $row->{'memWW_adjusted'};	
		$counter{'SQL searched successful: API_Mem_WarWeight'}++;	
	} 
	
	# update war weights totals
	if (( $test == 0 )&&(( !defined ( $warWeight{'adjusted_sql'} ))||($warWeight{'adjusted_sql'} != $warWeight{'adjusted'} ))) {
		$stmt_memWW_insert->execute ( $member_tag,
					$time,
					$warWeight{'defBldg'},
					$warWeight{'traps'},
					$warWeight{'miscBldg'},
					$warWeight{'heros'},
					$warWeight{'troops'},
					$warWeight{'spells'},
					$warWeight{'total'},
					$warWeight{'unadjusted'},
					$warWeight{'penalty'},
					$warWeight{'adjusted'} )
				or die "Unable to execute stmt_memWW_insert.  " . $stmt_memWW_insert->errstr;
		if ( $debug ) { print "DEBUG:		War weight for $member_tag inserted successfully.\n"; }
		$counter{'SQL inserted successful: API_Mem_WarWeight'}++;	
	}
	
	# clean up sql statements
	$stmt_wwTH_search->finish();
	$stmt_memWW_search->finish();
	$stmt_memWW_insert->finish();
	
# 	print "time = $time\n";
# 	print Dumper(%warWeight) . "\n";
}

##############################
# Update War Weight for Defense Buildings
##############################
sub update_defBldg_data {
	if ( $debug ) { print "DEBUG:  Start updating defensive buildings war weight data.\n"; }
	
	# set up local variables
	my %warWeight_sql = ();

	# set up sql statements
	my $stmt_defToMem_search = $dbh->prepare ( get_defToMem_search() )
						or die "Unable to prepare stmt_defToMem_search.  " . $dbh->errstr;
	my $stmt_defToMem_update = $dbh->prepare ( get_defToMem_update() )
						or die "Unable to prepare stmt_defToMem_update.  " . $dbh->errstr;
	my $stmt_ww_search = $dbh->prepare ( get_ww_specific_search() )
						or die "Unable to prepare stmt_ww_search.  " . $dbh->errstr;
	
	# pull data for all members
	$stmt_defToMem_search->execute ()
							or die "Unable to execute stmt_defToMem_search.  " . $stmt_defToMem_search->errstr;
	
	# get the results
	while ( my $row = $stmt_defToMem_search->fetchrow_hashref ( ) ) {
		my @level = ();
		
		my $id = $row->{'defToMem_id'};
		my $tag = $row->{'defToMem_member_tag'};
		my $name = $row->{'defToMem_defense_name'};
		my $timestamp = $row->{'defToMem_timestamp'};
		$level[1] = $row->{'defToMem_pos1'};
		$level[2] = $row->{'defToMem_pos2'};
		$level[3] = $row->{'defToMem_pos3'};
		$level[4] = $row->{'defToMem_pos4'};
		$level[5] = $row->{'defToMem_pos5'};
		$level[6] = $row->{'defToMem_pos6'};
		$level[7] = $row->{'defToMem_pos7'};
		$level[8] = $row->{'defToMem_pos8'};
		$level[9] = $row->{'defToMem_pos9'};
		$level[10] = $row->{'defToMem_pos10'};
		$level[11] = $row->{'defToMem_pos11'};
		$level[12] = $row->{'defToMem_pos12'};
		my $warWeight = $row->{'defToMem_warWeight'};
		$counter{'SQL searched successful: API_Mem_DefToMem'}++;	
			
		# check if warWeight needs updating
		if (( !defined ( $warWeight ))||( $warWeight == 0 )) {
			# check if building or wall
			if ( $name ne 'Wall' ) {
				#iterate threw level array
				for ( my $i = 1; $i <= 12; $i++ ) {
					# check if position is set
					if (( defined ( $level[$i] ))&&( $level[$i] != 0 )) {
						# check if we already have found warweight for buildings
						if ( !defined ( $warWeight_sql{$name}{$level[$i]} )) {
							# pull war weight for toop and level
							$stmt_ww_search->execute ( $name, $level[$i] )
												or die "Unable to execute stmt_ww_search.  " . $stmt_ww_search->errstr;
								
							while ( my $ww_row = $stmt_ww_search->fetchrow_hashref ( ) ) {
								$warWeight_sql{$name}{$level[$i]} = $ww_row->{'ww_weight'};
								$counter{'SQL searched successful: API_WarWeight'}++;	
							}
						}
					
						# total war weight
						$warWeight += $warWeight_sql{$name}{$level[$i]};
					}
				}	
			} else {
				#iterate threw level array
				for ( my $i = 1; $i <= 12; $i++ ) {
					# check if position is set
					if (( defined ( $level[$i] ))&&( $level[$i] != 0 )) {
						# check if we already have found warweight for troop
						if ( !defined ( $warWeight_sql{$name}{$i} )) {
							# pull war weight for toop and level
							$stmt_ww_search->execute ( $name, $i )
												or die "Unable to execute stmt_ww_search.  " . $stmt_ww_search->errstr;
								
							while ( my $ww_row = $stmt_ww_search->fetchrow_hashref ( ) ) {
								$warWeight_sql{$name}{$i} = $ww_row->{'ww_weight'};
								$counter{'SQL searched successful: API_WarWeight'}++;	
							}
						}
					
						# total war weight
						$warWeight += $warWeight_sql{$name}{$i} * $level[$i];
					}
				}					
			}
			# update war weight in member table
			if (( $test == 0 )&&( defined ( $warWeight ))&&( $warWeight != 0 )) {
				$stmt_defToMem_update->execute ( $warWeight, $id )
									or die "Unable to execute stmt_defToMem_update.  " . $stmt_defToMem_update->errstr;
				$counter{'SQL update successful: API_Mem_DefToMem'}++;	
			}
		} 
	}	
	
	# clean up sql statements
	$stmt_defToMem_search->finish();
	$stmt_defToMem_update->finish();
	$stmt_ww_search->finish();

	if ( $debug ) { print "DEBUG:  Finish updating defensive buildings war weight data.\n"; }
	
}

##############################
# Get war weight for member Defense Buildings
##############################
sub get_defBldg_ww {
	# get input variables
	my ( $member_tag ) = @_;

	# local variables
	my $warWeight = 0;

	# set up sql statements
	my $stmt_defToMem_mebWW_search = $dbh->prepare ( get_defToMem_mebWW_search() )
						or die "Unable to prepare stmt_defToMem_mebWW_search.  " . $dbh->errstr;

	# pull all members from sql at last update
	$stmt_defToMem_mebWW_search->execute ( $member_tag )
							or die "Unable to execute stmt_defToMem_mebWW_search.  " . $stmt_defToMem_mebWW_search->errstr;
	
	# get the results
	while ( my $row = $stmt_defToMem_mebWW_search->fetchrow_hashref ( ) ) {
		$warWeight += $row->{'defToMem_warWeight'};	
		$counter{'SQL searched successful: API_Mem_DefToMem'}++;	
	}
	
	# clean up sql statements
	$stmt_defToMem_mebWW_search->finish();
	
	return $warWeight;
}

##############################
# Update War Weight for Traps
##############################
sub update_trap_data {
	if ( $debug ) { print "DEBUG:  Start updating traps war weight data.\n"; }
	
	# set up local variables
	my %warWeight_sql = ();

	# set up sql statements
	my $stmt_trapToMem_search = $dbh->prepare ( get_trapToMem_search() )
						or die "Unable to prepare stmt_trapToMem_search.  " . $dbh->errstr;
	my $stmt_trapToMem_update = $dbh->prepare ( get_trapToMem_update() )
						or die "Unable to prepare stmt_trapToMem_update.  " . $dbh->errstr;
	my $stmt_ww_search = $dbh->prepare ( get_ww_specific_search() )
						or die "Unable to prepare stmt_ww_search.  " . $dbh->errstr;
	
	# pull data for all members
	$stmt_trapToMem_search->execute ()
							or die "Unable to execute stmt_trapToMem_search.  " . $stmt_trapToMem_search->errstr;
	
	# get the results
	while ( my $row = $stmt_trapToMem_search->fetchrow_hashref ( ) ) {
		my @level = ();
		
		my $id = $row->{'trapToMem_id'};
		my $tag = $row->{'trapToMem_member_tag'};
		my $name = $row->{'trapToMem_trap_name'};
		my $timestamp = $row->{'trapToMem_timestamp'};
		$level[1] = $row->{'trapToMem_pos1'};
		$level[2] = $row->{'trapToMem_pos2'};
		$level[3] = $row->{'trapToMem_pos3'};
		$level[4] = $row->{'trapToMem_pos4'};
		$level[5] = $row->{'trapToMem_pos5'};
		$level[6] = $row->{'trapToMem_pos6'};
		my $warWeight = $row->{'trapToMem_warWeight'};
		$counter{'SQL searched successful: API_Mem_TrapToMem'}++;	

		# check if warWeight needs updating
		if (( !defined ( $warWeight ))||( $warWeight == 0 )) {
			#iterate threw level array
			for ( my $i = 1; $i <= 6; $i++ ) {
				# check if position is set
				if (( defined ( $level[$i] ))&&( $level[$i] != 0 )) {
					# check if we already have found warweight for troop
					if ( !defined ( $warWeight_sql{$name}{$level[$i]} )) {
						# pull war weight for toop and level
						$stmt_ww_search->execute ( $name, $level[$i] )
											or die "Unable to execute stmt_ww_search.  " . $stmt_ww_search->errstr;
							
						while ( my $ww_row = $stmt_ww_search->fetchrow_hashref ( ) ) {
							$warWeight_sql{$name}{$level[$i]} = $ww_row->{'ww_weight'};
							$counter{'SQL searched successful: API_WarWeight'}++;	
						}
					}
				
					# total war weight
					$warWeight += $warWeight_sql{$name}{$level[$i]};
				}
			}	
			# update war weight in member table
			if (( $test == 0 )&&( defined ( $warWeight ))&&( $warWeight != 0 )) {
				$stmt_trapToMem_update->execute ( $warWeight, $id )
									or die "Unable to execute stmt_trapToMem_update.  " . $stmt_trapToMem_update->errstr;
				$counter{'SQL update successful: API_Mem_TrapToMem'}++;	
			}
		} 
	}	
	
	# clean up sql statements
	$stmt_trapToMem_search->finish();
	$stmt_trapToMem_update->finish();
	$stmt_ww_search->finish();

	if ( $debug ) { print "DEBUG:  Finish updating traps war weight data.\n"; }
	
}

##############################
# Get war weight for member Traps
##############################
sub get_trap_ww {
	# get input variables
	my ( $member_tag ) = @_;

	# local variables
	my $warWeight = 0;

	# set up sql statements
	my $stmt_trapToMem_mebWW_search = $dbh->prepare ( get_trapToMem_mebWW_search() )
						or die "Unable to prepare stmt_trapToMem_mebWW_search.  " . $dbh->errstr;

	# pull all members from sql at last update
	$stmt_trapToMem_mebWW_search->execute ( $member_tag )
							or die "Unable to execute stmt_trapToMem_mebWW_search.  " . $stmt_trapToMem_mebWW_search->errstr;
	
	# get the results
	while ( my $row = $stmt_trapToMem_mebWW_search->fetchrow_hashref ( ) ) {
		$warWeight += $row->{'trapToMem_warWeight'};	
		$counter{'SQL searched successful: API_Mem_TrapToMem'}++;	
	}
	
	# clean up sql statements
	$stmt_trapToMem_mebWW_search->finish();
	
	return $warWeight;
}

##############################
# Update War Weight for Misc Buildings
##############################
sub update_miscBldg_data {
	if ( $debug ) { print "DEBUG:  Start updating misc buildings war weight data.\n"; }
	
	# set up local variables
	my %warWeight_sql = ();

	# set up sql statements
	my $stmt_miscToMem_search = $dbh->prepare ( get_miscToMem_search() )
						or die "Unable to prepare stmt_miscToMem_search.  " . $dbh->errstr;
	my $stmt_miscToMem_update = $dbh->prepare ( get_miscToMem_update() )
						or die "Unable to prepare stmt_miscToMem_update.  " . $dbh->errstr;
	my $stmt_ww_search = $dbh->prepare ( get_ww_specific_search() )
						or die "Unable to prepare stmt_ww_search.  " . $dbh->errstr;
	
	# pull data for all members
	$stmt_miscToMem_search->execute ()
							or die "Unable to execute stmt_miscToMem_search.  " . $stmt_miscToMem_search->errstr;
	
	# get the results
	while ( my $row = $stmt_miscToMem_search->fetchrow_hashref ( ) ) {
		my @level = ();
		
		my $id = $row->{'miscToMem_id'};
		my $tag = $row->{'miscToMem_member_tag'};
		my $name = $row->{'miscToMem_misc_name'};
		my $timestamp = $row->{'miscToMem_timestamp'};
		$level[1] = $row->{'miscToMem_pos1'};
		$level[2] = $row->{'miscToMem_pos2'};
		$level[3] = $row->{'miscToMem_pos3'};
		$level[4] = $row->{'miscToMem_pos4'};
		$level[5] = $row->{'miscToMem_pos5'};
		$level[6] = $row->{'miscToMem_pos6'};
		$level[7] = $row->{'miscToMem_pos7'};
		my $warWeight = $row->{'miscToMem_warWeight'};
		$counter{'SQL searched successful: API_Mem_MiscToMem'}++;	

		# check if warWeight needs updating
		if (( !defined ( $warWeight ))||( $warWeight == 0 )) {
			#iterate threw level array
			for ( my $i = 1; $i <= 7; $i++ ) {
				# check if position is set
				if (( defined ( $level[$i] ))&&( $level[$i] != 0 )) {
					# check if we already have found warweight for troop
					if ( !defined ( $warWeight_sql{$name}{$level[$i]} )) {
						# pull war weight for toop and level
						$stmt_ww_search->execute ( $name, $level[$i] )
											or die "Unable to execute stmt_ww_search.  " . $stmt_ww_search->errstr;
							
						while ( my $ww_row = $stmt_ww_search->fetchrow_hashref ( ) ) {
							$warWeight_sql{$name}{$level[$i]} = $ww_row->{'ww_weight'};
							$counter{'SQL searched successful: API_WarWeight'}++;	
						}
					}
				
					# total war weight
					$warWeight += $warWeight_sql{$name}{$level[$i]};
				}
			}	
			# update war weight in member table
			if (( $test == 0 )&&( defined ( $warWeight ))&&( $warWeight != 0 )) {
				$stmt_miscToMem_update->execute ( $warWeight, $id )
									or die "Unable to execute stmt_miscToMem_update.  " . $stmt_miscToMem_update->errstr;
				$counter{'SQL update successful: API_Mem_MiscToMem'}++;	
			}
		} 
	}	
	
	# clean up sql statements
	$stmt_miscToMem_search->finish();
	$stmt_miscToMem_update->finish();
	$stmt_ww_search->finish();

	if ( $debug ) { print "DEBUG:  Finish updating misc buildings war weight data.\n"; }
	
}

##############################
# Get war weight for member Misc Buildings
##############################
sub get_miscBldg_ww {
	# get input variables
	my ( $member_tag ) = @_;

	# local variables
	my $warWeight = 0;

	# set up sql statements
	my $stmt_miscToMem_mebWW_search = $dbh->prepare ( get_miscToMem_mebWW_search() )
						or die "Unable to prepare stmt_miscToMem_mebWW_search.  " . $dbh->errstr;

	# pull all members from sql at last update
	$stmt_miscToMem_mebWW_search->execute ( $member_tag )
							or die "Unable to execute stmt_miscToMem_mebWW_search.  " . $stmt_miscToMem_mebWW_search->errstr;
	
	# get the results
	while ( my $row = $stmt_miscToMem_mebWW_search->fetchrow_hashref ( ) ) {
		$warWeight += $row->{'miscToMem_warWeight'};	
		$counter{'SQL searched successful: API_Mem_MiscToMem'}++;	
	}
	
	# clean up sql statements
	$stmt_miscToMem_mebWW_search->finish();
	
	return $warWeight;
}

##############################
# Update War Weight for Heros
##############################
sub update_heros_data {
	if ( $debug ) { print "DEBUG:  Start updating hero war weight data.\n"; }
	
	# set up local variables
	my %warWeight_sql = ();

	# set up sql statements
	my $stmt_herosToMem_search = $dbh->prepare ( get_herosToMem_search() )
						or die "Unable to prepare stmt_herosToMem_search.  " . $dbh->errstr;
	my $stmt_herosToMem_update = $dbh->prepare ( get_herosToMem_update() )
						or die "Unable to prepare stmt_herosToMem_update.  " . $dbh->errstr;
	my $stmt_ww_search = $dbh->prepare ( get_ww_specific_search() )
						or die "Unable to prepare stmt_ww_search.  " . $dbh->errstr;
	
	# pull heros for all members
	$stmt_herosToMem_search->execute ()
							or die "Unable to execute stmt_herosToMem_search.  " . $stmt_herosToMem_search->errstr;
	
	# get the results
	while ( my $row = $stmt_herosToMem_search->fetchrow_hashref ( ) ) {
		
		my $id = $row->{'heroToMem_id'};
		my $tag = $row->{'heroToMem_member_tag'};
		my $name = $row->{'heroToMem_heros_name'};
		my $timestamp = $row->{'heroToMem_timestamp'};
		my $level = $row->{'heroToMem_level'};
		my $warWeight = $row->{'heroToMem_warWeight'};
		$counter{'SQL searched successful: API_Mem_HerosToMem'}++;	
			
		# check if warWeight needs updating
		if ( $warWeight == 0 ) {
			# check if we already have found warweight for troop
			if ( !defined ( $warWeight_sql{$name}{$level} )) {
				# pull war weight for toop and level
				$stmt_ww_search->execute ( $name, $level )
									or die "Unable to execute stmt_ww_search.  " . $stmt_ww_search->errstr;
								
				while ( my $ww_row = $stmt_ww_search->fetchrow_hashref ( ) ) {
					$warWeight_sql{$name}{$level} = $ww_row->{'ww_weight'};
					$counter{'SQL searched successful: API_WarWeight'}++;	
				}
			}
			# update war weight in member table
			if ( $test == 0 ) {
				$stmt_herosToMem_update->execute ( $warWeight_sql{$name}{$level}, $id )
									or die "Unable to execute stmt_herosToMem_update.  " . $stmt_herosToMem_update->errstr;
				$counter{'SQL update successful: API_Mem_HerosToMem'}++;	
			}	
		} 
	}	
	
	# clean up sql statements
	$stmt_herosToMem_search->finish();
	$stmt_herosToMem_update->finish();
	$stmt_ww_search->finish();

	if ( $debug ) { print "DEBUG:  Finish updating hero war weight data.\n"; }
	
}

##############################
# Get war weight for member hero
##############################
sub get_heros_ww {
	# get input variables
	my ( $member_tag ) = @_;

	# local variables
	my $warWeight = 0;

	# set up sql statements
	my $stmt_heroToMem_mebWW_search = $dbh->prepare ( get_heroToMem_mebWW_search() )
						or die "Unable to prepare stmt_heroToMem_mebWW_search.  " . $dbh->errstr;

	# pull all members from sql at last update
	$stmt_heroToMem_mebWW_search->execute ( $member_tag )
							or die "Unable to execute stmt_heroToMem_mebWW_search.  " . $stmt_heroToMem_mebWW_search->errstr;
	
	# get the results
	while ( my $row = $stmt_heroToMem_mebWW_search->fetchrow_hashref ( ) ) {
		$warWeight += $row->{'heroToMem_warWeight'};	
		$counter{'SQL searched successful: API_Mem_HerosToMem'}++;	
	}
	
	# clean up sql statements
	$stmt_heroToMem_mebWW_search->finish();
	
	return $warWeight;
}
##############################
# Update War Weight for Troops
##############################
sub update_troops_data {
	if ( $debug ) { print "DEBUG:  Start updating troop war weight data.\n"; }
	
	# set up local variables
	my %warWeight_sql = ();

	# set up sql statements
	my $stmt_troopsToMem_search = $dbh->prepare ( get_troopsToMem_search() )
						or die "Unable to prepare stmt_troopsToMem_search.  " . $dbh->errstr;
	my $stmt_troopsToMem_update = $dbh->prepare ( get_troopsToMem_update() )
						or die "Unable to prepare stmt_troopsToMem_update.  " . $dbh->errstr;
	my $stmt_ww_search = $dbh->prepare ( get_ww_specific_search() )
						or die "Unable to prepare stmt_ww_search.  " . $dbh->errstr;
	
	# pull troops for all members
	$stmt_troopsToMem_search->execute ()
							or die "Unable to execute stmt_troopsToMem_search.  " . $stmt_troopsToMem_search->errstr;
	
	# get the results
	while ( my $row = $stmt_troopsToMem_search->fetchrow_hashref ( ) ) {
		
		my $id = $row->{'troopsToMem_id'};
		my $tag = $row->{'troopsToMem_member_tag'};
		my $name = $row->{'troopsToMem_troops_name'};
		my $timestamp = $row->{'troopsToMem_timestamp'};
		my $level = $row->{'troopsToMem_level'};
		my $warWeight = $row->{'troopsToMem_warWeight'};
		$counter{'SQL searched successful: API_Mem_TroopsToMem'}++;	
			
		# check if warWeight needs updating
		if ( $warWeight == 0 ) {
			# check if we already have found warweight for troop
			if ( !defined ( $warWeight_sql{$name}{$level} )) {
				# pull war weight for toop and level
				$stmt_ww_search->execute ( $name, $level )
									or die "Unable to execute stmt_ww_search.  " . $stmt_ww_search->errstr;
								
				while ( my $ww_row = $stmt_ww_search->fetchrow_hashref ( ) ) {
					$warWeight_sql{$name}{$level} = $ww_row->{'ww_weight'};
					$counter{'SQL searched successful: API_WarWeight'}++;	
				}
			}
			# update war weight in member table
			if ( $test == 0 ) {
				$stmt_troopsToMem_update->execute ( $warWeight_sql{$name}{$level}, $id )
									or die "Unable to execute stmt_troopsToMem_update.  " . $stmt_troopsToMem_update->errstr;
				$counter{'SQL update successful: API_Mem_TroopsToMem'}++;	
			}	
		} 
	}	
	
	# clean up sql statements
	$stmt_troopsToMem_search->finish();
	$stmt_troopsToMem_update->finish();
	$stmt_ww_search->finish();

	if ( $debug ) { print "DEBUG:  Finish updating troop war weight data.\n"; }
	
}

##############################
# Get war weight for member troops
##############################
sub get_troops_ww {
	# get input variables
	my ( $member_tag ) = @_;

	# local variables
	my $warWeight = 0;

	# set up sql statements
	my $stmt_troopsToMem_mebWW_search = $dbh->prepare ( get_troopsToMem_mebWW_search() )
						or die "Unable to prepare stmt_troopsToMem_mebWW_search.  " . $dbh->errstr;

	# pull all members from sql at last update
	$stmt_troopsToMem_mebWW_search->execute ( $member_tag )
							or die "Unable to execute stmt_troopsToMem_mebWW_search.  " . $stmt_troopsToMem_mebWW_search->errstr;
	
	# get the results
	while ( my $row = $stmt_troopsToMem_mebWW_search->fetchrow_hashref ( ) ) {
		$warWeight += $row->{'troopsToMem_warWeight'};	
		$counter{'SQL searched successful: API_Mem_TroopsToMem'}++;	
	}
	
	# clean up sql statements
	$stmt_troopsToMem_mebWW_search->finish();
	
	return $warWeight;
}

##############################
# Update War Weight for Spells
##############################
sub update_spells_data {
	if ( $debug ) { print "DEBUG:  Start updating spell war weight data.\n"; }
	
	# set up local variables
	my %warWeight_sql = ();

	# set up sql statements
	my $stmt_spellsToMem_search = $dbh->prepare ( get_spellsToMem_search() )
						or die "Unable to prepare stmt_spellsToMem_search.  " . $dbh->errstr;
	my $stmt_spellsToMem_update = $dbh->prepare ( get_spellsToMem_update() )
						or die "Unable to prepare stmt_spellsToMem_update.  " . $dbh->errstr;
	my $stmt_ww_search = $dbh->prepare ( get_ww_specific_search() )
						or die "Unable to prepare stmt_ww_search.  " . $dbh->errstr;
	
	# pull spells for all members
	$stmt_spellsToMem_search->execute ()
							or die "Unable to execute stmt_spellsToMem_search.  " . $stmt_spellsToMem_search->errstr;
	
	# get the results
	while ( my $row = $stmt_spellsToMem_search->fetchrow_hashref ( ) ) {
		
		my $id = $row->{'spellsToMem_id'};
		my $tag = $row->{'spellsToMem_member_tag'};
		my $name = $row->{'spellsToMem_spells_name'};
		my $timestamp = $row->{'spellsToMem_timestamp'};
		my $level = $row->{'spellsToMem_level'};
		my $warWeight = $row->{'spellsToMem_warWeight'};
		$counter{'SQL searched successful: API_Mem_SpellsToMem'}++;	
		
		# remove Spell from name
		$name =~ s/ Spell//;
		
		# check if warWeight needs updating
		if ( $warWeight == 0 ) {
			# check if we already have found warweight for troop
			if ( !defined ( $warWeight_sql{$name}{$level} )) {
				# pull war weight for toop and level
				$stmt_ww_search->execute ( $name, $level )
									or die "Unable to execute stmt_ww_search.  " . $stmt_ww_search->errstr;
								
				while ( my $ww_row = $stmt_ww_search->fetchrow_hashref ( ) ) {
					$warWeight_sql{$name}{$level} = $ww_row->{'ww_weight'};
					$counter{'SQL searched successful: API_WarWeight'}++;	
				}
			}
			# update war weight in member table
			if ( $test == 0 ) {
			
			
				$stmt_spellsToMem_update->execute ( $warWeight_sql{$name}{$level}, $id )
									or die "Unable to execute stmt_spellsToMem_update.  " . $stmt_spellsToMem_update->errstr;
				$counter{'SQL update successful: API_Mem_SpellsToMem'}++;	
			}	
		} 
	}	
	
	# clean up sql statements
	$stmt_spellsToMem_search->finish();
	$stmt_spellsToMem_update->finish();
	$stmt_ww_search->finish();

	if ( $debug ) { print "DEBUG:  Finish updating spell war weight data.\n"; }
	
}

##############################
# Get war weight for member Spells
##############################
sub get_spells_ww {
	# get input variables
	my ( $member_tag ) = @_;

	# local variables
	my $warWeight = 0;

	# set up sql statements
	my $stmt_spellToMem_mebWW_search = $dbh->prepare ( get_spellToMem_mebWW_search() )
						or die "Unable to prepare stmt_spellToMem_mebWW_search.  " . $dbh->errstr;

	# pull all members from sql at last update
	$stmt_spellToMem_mebWW_search->execute ( $member_tag )
							or die "Unable to execute stmt_spellToMem_mebWW_search.  " . $stmt_spellToMem_mebWW_search->errstr;
	
	# get the results
	while ( my $row = $stmt_spellToMem_mebWW_search->fetchrow_hashref ( ) ) {
		$warWeight += $row->{'spellsToMem_warWeight'};	
		$counter{'SQL searched successful: API_Mem_SpellsToMem'}++;	
	}
	
	# clean up sql statements
	$stmt_spellToMem_mebWW_search->finish();
	
	return $warWeight;
}

