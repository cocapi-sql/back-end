#! /usr/bin/perl

#############################################
#                  COC API                  #
#                                           #
#       Used to update member records       #
#           for total donations.            #
#                                           #
#              by Gary Douglas              #
#               Oct. 2, 2016                #
#        http://www.the-blacklist.ca        #
#############################################


use Data::Dumper;
use Time::Piece;
use DBI;
#use Config::Simple;

# SQL Variables
my $sql_platform = "mysql";			# change if not using mysql
my $dbname = "thebl962_jumi";		# set to database name
my $servername = "localhost";		# change if not local database
my $username = "thebl962_jumi";		# set to database username
my $password = "=4%Q}PJTeV8y";		# set to database password
my $dsn = "DBI:" . $sql_platform . ":" . $dbname . ":" . $localhost;

# set up variables
my $members = ();
my %counter = ();
my $debug = 1;

# connect to MySQL database
my %attr = ( PrintError=>0,RaiseError=>1 );
my $dbh = DBI->connect ( $dsn, $username, $password, \%attr );

# set up members search sql statement
my $sql_members_search = "SELECT DISTINCT `member_tag` 
   FROM `API_Member`";
   
my $stmt_members_search = $dbh->prepare ( $sql_members_search );

# set up member search sql statement
my $sql_member_search = "SELECT `member_id`, `member_donations`, `member_donationsReceived` 
   FROM `API_Member` 
   WHERE `member_tag` LIKE ? 
   ORDER BY `member_timestamp` ASC";

my $stmt_member_search = $dbh->prepare ( $sql_member_search );

# set up member update sql statement
my $sql_member_update = "UPDATE `thebl962_jumi`.`API_Member` 
   SET `member_total_donations` = ?, 
     `member_total_donationsReceived` = ? 
   WHERE `API_Member`.`member_id` = ?";

my $stmt_member_update = $dbh->prepare ( $sql_member_update );

# pull all members tag numbers from sql
if ( $stmt_members_search->execute () ) {	
	if ( $debug ) { print "DEBUG: Members pulled successfully.\n"; }
}

while ( my $row = $stmt_members_search->fetchrow_hashref ( ) ) {
	push @members, $row->{'member_tag'};
}

# loop threw members
foreach my $member ( @members ) {
	if ( $debug ) { print "DEBUG: Start processing $member.\n"; }
	
	# setup variables
	my %member_donations = ();
	my $new_donations = 0;
	my $old_donations = 0;
	my $new_total_donations = 0;
	my $old_total_donations = 0;
	my $new_donationsReceived = 0;
	my $old_donationsReceived = 0;
	my $new_total_donationsReceived = 0;
	my $old_total_donationsReceived = 0;

	# pull member donations
	if ( $stmt_member_search->execute ( $member ) ) {	
		if ( $debug ) { print "DEBUG:    Member donations pulled successfully.\n"; }
	}
	
	while ( my $row = $stmt_member_search->fetchrow_hashref ( ) ) {
		$member_id = $row->{'member_id'};
		$new_donations = $row->{'member_donations'};
		$new_donationsReceived = $row->{'member_donationsReceived'};
		
		# check if donations are lower, higher, or equal and update accordingly
		if ( $new_donations < $old_donations ) {
			$new_total_donations = $old_total_donations + $new_donations;
		} elsif ( $new_donations > $old_donations ) {
			$new_total_donations = $old_total_donations + $new_donations - $old_donations;
		} else {
			$new_total_donations = $old_total_donations;
		}
		
		# check if donationsReceived are lower, higher, or equal and update accordingly
		if ( $new_donationsReceived < $old_donationsReceived ) {
			$new_total_donationsReceived = $old_total_donationsReceived + $new_donationsReceived;
		} elsif ( $new_donationsReceived > $old_donationsReceived ) {
			$new_total_donationsReceived = $old_total_donationsReceived + $new_donationsReceived - $old_donationsReceived;
		} else {
			$new_total_donationsReceived = $old_total_donationsReceived;
		}
		
		# update sql
		if ( $stmt_member_update->execute ( $new_total_donations, 
											$new_total_donationsReceived, 
											$member_id ) ) {	
			#if ( $debug ) { print "DEBUG:    Member donations updated successfully.\n"; }
			$counter->{'Records\'s donations updated'}++;	
		}		
		
		# set old donations from new donations
		$old_donations = $new_donations;
		$old_total_donations = $new_total_donations;
		$old_donationsReceived = $new_donationsReceived;
		$old_total_donationsReceived = $new_total_donationsReceived;
	}
	
	if ( $debug ) { print "DEBUG: Finished processing $member.\n"; }
	$counter->{'Member\'s processed'}++;	
}

# clean up and disconnect from the MySQL database
$stmt_members_search->finish();
$stmt_member_search->finish();
$stmt_member_update->finish();
$dbh->disconnect();
	
	
# display counter
print "\n";
print "Summary\n";
print "-" x 50 . " " . "-" x 5 . "\n";
foreach my $key ( sort keys %{ $counter } ) {
	printf( "%-50s %5d\n", $key, $counter->{$key} );
}

